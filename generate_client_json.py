import os
import prance
import json
from typing import Any, Dict
from pathlib import Path


def bundle_specs(spec: Path) -> Dict[str, Any]:
    parser = prance.ResolvingParser(str(spec.absolute()), lazy=True, strict=True,
                                    backend='openapi-spec-validator')
    parser.parse()
    return parser.specification


def bundle_all_specs(specPath, dest):
    print(f"Boundle all specks from {specPath}")
    os.makedirs(dest, exist_ok=True)
    specs = list(Path(specPath).glob('*.yml'))
    for spec in specs:
        destFile = Path(f"{dest}/{spec.stem}.json")
        print(f"{spec.name} ---> {destFile.name}")
        jsonSpec = bundle_specs(spec)
        f = open(destFile, "w+")
        f.write(json.dumps(jsonSpec))
        f.close()

bundle_all_specs('../imbio-core/swagger', 'client_jsons/core')
bundle_all_specs('../imbio-auth/swagger', 'client_jsons/auth')
