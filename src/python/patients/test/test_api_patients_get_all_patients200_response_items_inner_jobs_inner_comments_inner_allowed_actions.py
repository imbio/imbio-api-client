# coding: utf-8

"""
    IMBIO core api for patients

    Methods for managing patients 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_patients.models.api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_allowed_actions import ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions

class TestApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions(unittest.TestCase):
    """ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions:
        """Test ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions`
        """
        model = ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions()
        if include_optional:
            return ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions(
                edit = True,
                delete = True
            )
        else:
            return ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions(
        )
        """

    def testApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions(self):
        """Test ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
