# coding: utf-8

"""
    IMBIO core api for patients

    Methods for managing patients 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_patients.models.api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner import ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner

class TestApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner(unittest.TestCase):
    """ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner:
        """Test ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner`
        """
        model = ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner()
        if include_optional:
            return ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner(
                id = 56,
                created_at = '',
                modified_at = '',
                value = '',
                owner_id = 56,
                modified_by = core_patients.models.api_patients_get_all_patients_200_response_items_inner_jobs_inner_owner.api_patients_get_all_patients_200_response_items_inner_jobs_inner_owner(
                    id = 56, 
                    name = '', 
                    email = '', ),
                allowed_actions = core_patients.models.api_patients_get_all_patients_200_response_items_inner_jobs_inner_comments_inner_allowed_actions.api_patients_get_all_patients_200_response_items_inner_jobs_inner_comments_inner_allowed_actions(
                    edit = True, 
                    delete = True, )
            )
        else:
            return ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner(
        )
        """

    def testApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner(self):
        """Test ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
