# coding: utf-8

"""
    IMBIO core api for patients

    Methods for managing patients 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_patients.models.api_patients_get_all_patients200_response_items_inner_jobs_inner_results_inner import ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerResultsInner

class TestApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerResultsInner(unittest.TestCase):
    """ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerResultsInner unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerResultsInner:
        """Test ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerResultsInner
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerResultsInner`
        """
        model = ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerResultsInner()
        if include_optional:
            return ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerResultsInner(
                id = 56,
                created_at = '',
                modified_at = '',
                name = '',
                value = '',
                type = '',
                owner_id = 56,
                modified_by = core_patients.models.api_patients_get_all_patients_200_response_items_inner_jobs_inner_owner.api_patients_get_all_patients_200_response_items_inner_jobs_inner_owner(
                    id = 56, 
                    name = '', 
                    email = '', )
            )
        else:
            return ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerResultsInner(
        )
        """

    def testApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerResultsInner(self):
        """Test ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerResultsInner"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
