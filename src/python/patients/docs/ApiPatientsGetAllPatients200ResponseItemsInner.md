# ApiPatientsGetAllPatients200ResponseItemsInner


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**patient_birthdate** | **str** |  | [optional] 
**patient_id** | **str** |  | [optional] 
**patient_name** | **str** |  | [optional] 
**patient_age** | **int** |  | [optional] 
**patient_sex** | **str** |  | [optional] 
**jobs** | [**List[ApiPatientsGetAllPatients200ResponseItemsInnerJobsInner]**](ApiPatientsGetAllPatients200ResponseItemsInnerJobsInner.md) |  | [optional] 

## Example

```python
from core_patients.models.api_patients_get_all_patients200_response_items_inner import ApiPatientsGetAllPatients200ResponseItemsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInner from a JSON string
api_patients_get_all_patients200_response_items_inner_instance = ApiPatientsGetAllPatients200ResponseItemsInner.from_json(json)
# print the JSON string representation of the object
print ApiPatientsGetAllPatients200ResponseItemsInner.to_json()

# convert the object into a dict
api_patients_get_all_patients200_response_items_inner_dict = api_patients_get_all_patients200_response_items_inner_instance.to_dict()
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInner from a dict
api_patients_get_all_patients200_response_items_inner_form_dict = api_patients_get_all_patients200_response_items_inner.from_dict(api_patients_get_all_patients200_response_items_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


