# ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**edit** | **bool** | shows if user can edit the commment | [optional] 
**delete** | **bool** | shows if user can delete the commment | [optional] 

## Example

```python
from core_patients.models.api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_allowed_actions import ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions

# TODO update the JSON string below
json = "{}"
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions from a JSON string
api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_allowed_actions_instance = ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions.from_json(json)
# print the JSON string representation of the object
print ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions.to_json()

# convert the object into a dict
api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_allowed_actions_dict = api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_allowed_actions_instance.to_dict()
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions from a dict
api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_allowed_actions_form_dict = api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_allowed_actions.from_dict(api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_allowed_actions_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


