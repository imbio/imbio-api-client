# ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerStudy

Study properties parsed from the DICOM study that will be used by the UI/bridge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The id of the study  | [optional] 
**uid** | **str** | The UID of the study  | [optional] 
**study_hash** | **str** | Hash to identify the patient  | [optional] 
**study_date** | **str** | The time when the study was created  | [optional] 
**patient_id** | **str** | The patient&#39;s ID from orthanc  | [optional] 
**patient_name** | **str** | The patient&#39;s name  | [optional] 
**patient_birthdate** | **str** | The patient&#39;s birthdate  | [optional] 
**patient_age** | **int** | The patient&#39;s age in year  | [optional] 
**patient_sex** | **str** | The gender of the patient  | [optional] 
**accession_number** | **str** | The accession number  | [optional] 

## Example

```python
from core_patients.models.api_patients_get_all_patients200_response_items_inner_jobs_inner_study import ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerStudy

# TODO update the JSON string below
json = "{}"
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerStudy from a JSON string
api_patients_get_all_patients200_response_items_inner_jobs_inner_study_instance = ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerStudy.from_json(json)
# print the JSON string representation of the object
print ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerStudy.to_json()

# convert the object into a dict
api_patients_get_all_patients200_response_items_inner_jobs_inner_study_dict = api_patients_get_all_patients200_response_items_inner_jobs_inner_study_instance.to_dict()
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerStudy from a dict
api_patients_get_all_patients200_response_items_inner_jobs_inner_study_form_dict = api_patients_get_all_patients200_response_items_inner_jobs_inner_study.from_dict(api_patients_get_all_patients200_response_items_inner_jobs_inner_study_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


