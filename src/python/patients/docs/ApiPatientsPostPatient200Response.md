# ApiPatientsPostPatient200Response


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 

## Example

```python
from core_patients.models.api_patients_post_patient200_response import ApiPatientsPostPatient200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiPatientsPostPatient200Response from a JSON string
api_patients_post_patient200_response_instance = ApiPatientsPostPatient200Response.from_json(json)
# print the JSON string representation of the object
print ApiPatientsPostPatient200Response.to_json()

# convert the object into a dict
api_patients_post_patient200_response_dict = api_patients_post_patient200_response_instance.to_dict()
# create an instance of ApiPatientsPostPatient200Response from a dict
api_patients_post_patient200_response_form_dict = api_patients_post_patient200_response.from_dict(api_patients_post_patient200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


