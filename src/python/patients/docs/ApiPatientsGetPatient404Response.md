# ApiPatientsGetPatient404Response

The error object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **int** | The error code for this error | [optional] 
**message** | **str** | The error message for this error | [optional] 

## Example

```python
from core_patients.models.api_patients_get_patient404_response import ApiPatientsGetPatient404Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiPatientsGetPatient404Response from a JSON string
api_patients_get_patient404_response_instance = ApiPatientsGetPatient404Response.from_json(json)
# print the JSON string representation of the object
print ApiPatientsGetPatient404Response.to_json()

# convert the object into a dict
api_patients_get_patient404_response_dict = api_patients_get_patient404_response_instance.to_dict()
# create an instance of ApiPatientsGetPatient404Response from a dict
api_patients_get_patient404_response_form_dict = api_patients_get_patient404_response.from_dict(api_patients_get_patient404_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


