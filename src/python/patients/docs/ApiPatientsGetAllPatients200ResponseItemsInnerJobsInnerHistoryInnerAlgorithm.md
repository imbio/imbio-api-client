# ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInnerAlgorithm

Description of an algorithm minimal response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Algorithm Id | [optional] 
**name** | **str** | The name of the algorithm | [optional] 
**version** | **str** | The version of the algorithm | [optional] 
**logo_url** | **str** | The url for the algorithm | [optional] 
**body_part** | **str** | On which body_part of the algorithm runs | [optional] 
**description** | **str** | Description of the algorithm, what it does, etc... | [optional] 
**validator_orthanc** | **str** | Contains the validator template that will be run on the Study | [optional] 
**required_number_of_assets** | **int** |  | [optional] 

## Example

```python
from core_patients.models.api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner_algorithm import ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInnerAlgorithm

# TODO update the JSON string below
json = "{}"
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInnerAlgorithm from a JSON string
api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner_algorithm_instance = ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInnerAlgorithm.from_json(json)
# print the JSON string representation of the object
print ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInnerAlgorithm.to_json()

# convert the object into a dict
api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner_algorithm_dict = api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner_algorithm_instance.to_dict()
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInnerAlgorithm from a dict
api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner_algorithm_form_dict = api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner_algorithm.from_dict(api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner_algorithm_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


