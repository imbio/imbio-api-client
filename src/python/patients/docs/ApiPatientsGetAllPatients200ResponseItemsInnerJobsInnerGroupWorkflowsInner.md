# ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerGroupWorkflowsInner

Workflow object description

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from core_patients.models.api_patients_get_all_patients200_response_items_inner_jobs_inner_group_workflows_inner import ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerGroupWorkflowsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerGroupWorkflowsInner from a JSON string
api_patients_get_all_patients200_response_items_inner_jobs_inner_group_workflows_inner_instance = ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerGroupWorkflowsInner.from_json(json)
# print the JSON string representation of the object
print ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerGroupWorkflowsInner.to_json()

# convert the object into a dict
api_patients_get_all_patients200_response_items_inner_jobs_inner_group_workflows_inner_dict = api_patients_get_all_patients200_response_items_inner_jobs_inner_group_workflows_inner_instance.to_dict()
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerGroupWorkflowsInner from a dict
api_patients_get_all_patients200_response_items_inner_jobs_inner_group_workflows_inner_form_dict = api_patients_get_all_patients200_response_items_inner_jobs_inner_group_workflows_inner.from_dict(api_patients_get_all_patients200_response_items_inner_jobs_inner_group_workflows_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


