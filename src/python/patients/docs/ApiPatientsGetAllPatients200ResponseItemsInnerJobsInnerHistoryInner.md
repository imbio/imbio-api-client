# ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInner


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of a job | [optional] 
**name** | **str** | name of a job | [optional] 
**workflows** | [**List[ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerGroupWorkflowsInner]**](ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerGroupWorkflowsInner.md) |  | [optional] 
**upload_source** | **str** | Source where the job was created from (eg. api|ui|bridge|etc.) | [optional] 
**algorithm** | [**ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInnerAlgorithm**](ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInnerAlgorithm.md) |  | [optional] 
**status** | **str** | status of a job | [optional] 
**created_at** | **str** | The time when the job was created | [optional] 

## Example

```python
from core_patients.models.api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner import ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInner from a JSON string
api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner_instance = ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInner.from_json(json)
# print the JSON string representation of the object
print ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInner.to_json()

# convert the object into a dict
api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner_dict = api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner_instance.to_dict()
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerHistoryInner from a dict
api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner_form_dict = api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner.from_dict(api_patients_get_all_patients200_response_items_inner_jobs_inner_history_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


