# core_patients.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/patients*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_patients_delete_patient**](DefaultApi.md#api_patients_delete_patient) | **DELETE** /{patient_id} | 
[**api_patients_find_patient**](DefaultApi.md#api_patients_find_patient) | **POST** /actions/find | 
[**api_patients_get_all_patients**](DefaultApi.md#api_patients_get_all_patients) | **GET** / | 
[**api_patients_get_all_patients_without_job_restrictaion**](DefaultApi.md#api_patients_get_all_patients_without_job_restrictaion) | **GET** /all/ | 
[**api_patients_get_patient**](DefaultApi.md#api_patients_get_patient) | **GET** /{patient_id} | 
[**api_patients_post_patient**](DefaultApi.md#api_patients_post_patient) | **POST** / | 


# **api_patients_delete_patient**
> api_patients_delete_patient(patient_id)



Deletes a patient and it's all data, exceptt jobs 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_patients
from core_patients.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/patients
# See configuration.py for a list of all supported configuration parameters.
configuration = core_patients.Configuration(
    host = "https://launchpad.imbio.com/api/v1/patients"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_patients.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_patients.DefaultApi(api_client)
    patient_id = 'patient_id_example' # str | The unique identifier of a patient

    try:
        api_instance.api_patients_delete_patient(patient_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_patients_delete_patient: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patient_id** | **str**| The unique identifier of a patient | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_patients_find_patient**
> ApiPatientsGetAllPatients200ResponseItemsInner api_patients_find_patient(group_id, patient_id)



Internal endpoint to find patient by patient_id and group_id

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_patients
from core_patients.models.api_patients_get_all_patients200_response_items_inner import ApiPatientsGetAllPatients200ResponseItemsInner
from core_patients.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/patients
# See configuration.py for a list of all supported configuration parameters.
configuration = core_patients.Configuration(
    host = "https://launchpad.imbio.com/api/v1/patients"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_patients.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_patients.DefaultApi(api_client)
    group_id = 56 # int | Filter by group 
    patient_id = 'patient_id_example' # str | Filter by patient_id 

    try:
        api_response = api_instance.api_patients_find_patient(group_id, patient_id)
        print("The response of DefaultApi->api_patients_find_patient:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_patients_find_patient: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| Filter by group  | 
 **patient_id** | **str**| Filter by patient_id  | 

### Return type

[**ApiPatientsGetAllPatients200ResponseItemsInner**](ApiPatientsGetAllPatients200ResponseItemsInner.md)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | details of a patient |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_patients_get_all_patients**
> ApiPatientsGetAllPatients200Response api_patients_get_all_patients(limit=limit, offset=offset, order=order, search=search, algorithm_id=algorithm_id, group_id=group_id, patient_id=patient_id, order_by=order_by, status=status, job_after=job_after, job_before=job_before)



Returns all profiles

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_patients
from core_patients.models.api_patients_get_all_patients200_response import ApiPatientsGetAllPatients200Response
from core_patients.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/patients
# See configuration.py for a list of all supported configuration parameters.
configuration = core_patients.Configuration(
    host = "https://launchpad.imbio.com/api/v1/patients"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_patients.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_patients.DefaultApi(api_client)
    limit = 50 # int | Limit the number of returned roles to indicated value (default 50).  (optional) (default to 50)
    offset = 0 # int | When set the query returns 'limit' number of roles but skip 'offset' number of roles at the beginning.  (optional) (default to 0)
    order = 'order_example' # str | Specify the order of the listing: desc, asc  (optional)
    search = 'search_example' # str | Specify a search string  (optional)
    algorithm_id = [56] # List[int] | Filter by algorithm(s)  (optional)
    group_id = [56] # List[int] | Filter by group(s)  (optional)
    patient_id = ['patient_id_example'] # List[str] | Filter by patient_id(s)  (optional)
    order_by = 'order_by_example' # str | Order by the provided argument name.  (optional)
    status = ['status_example'] # List[str] | Restrict the list of jobs to only those with provided statuses.  (optional)
    job_after = '2013-10-20' # date | Restrict the list of jobs created after this date (optional)
    job_before = '2013-10-20' # date | Restrict the list of jobs created before this date (optional)

    try:
        api_response = api_instance.api_patients_get_all_patients(limit=limit, offset=offset, order=order, search=search, algorithm_id=algorithm_id, group_id=group_id, patient_id=patient_id, order_by=order_by, status=status, job_after=job_after, job_before=job_before)
        print("The response of DefaultApi->api_patients_get_all_patients:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_patients_get_all_patients: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Limit the number of returned roles to indicated value (default 50).  | [optional] [default to 50]
 **offset** | **int**| When set the query returns &#39;limit&#39; number of roles but skip &#39;offset&#39; number of roles at the beginning.  | [optional] [default to 0]
 **order** | **str**| Specify the order of the listing: desc, asc  | [optional] 
 **search** | **str**| Specify a search string  | [optional] 
 **algorithm_id** | [**List[int]**](int.md)| Filter by algorithm(s)  | [optional] 
 **group_id** | [**List[int]**](int.md)| Filter by group(s)  | [optional] 
 **patient_id** | [**List[str]**](str.md)| Filter by patient_id(s)  | [optional] 
 **order_by** | **str**| Order by the provided argument name.  | [optional] 
 **status** | [**List[str]**](str.md)| Restrict the list of jobs to only those with provided statuses.  | [optional] 
 **job_after** | **date**| Restrict the list of jobs created after this date | [optional] 
 **job_before** | **date**| Restrict the list of jobs created before this date | [optional] 

### Return type

[**ApiPatientsGetAllPatients200Response**](ApiPatientsGetAllPatients200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get all resource |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_patients_get_all_patients_without_job_restrictaion**
> ApiPatientsGetAllPatients200Response api_patients_get_all_patients_without_job_restrictaion(limit=limit, offset=offset, order=order, search=search, group_id=group_id, patient_id=patient_id)



Returns all profiles

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_patients
from core_patients.models.api_patients_get_all_patients200_response import ApiPatientsGetAllPatients200Response
from core_patients.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/patients
# See configuration.py for a list of all supported configuration parameters.
configuration = core_patients.Configuration(
    host = "https://launchpad.imbio.com/api/v1/patients"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_patients.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_patients.DefaultApi(api_client)
    limit = 50 # int | Limit the number of returned roles to indicated value (default 50).  (optional) (default to 50)
    offset = 0 # int | When set the query returns 'limit' number of roles but skip 'offset' number of roles at the beginning.  (optional) (default to 0)
    order = 'order_example' # str | Specify the order of the listing: desc, asc  (optional)
    search = 'search_example' # str | Specify a search string  (optional)
    group_id = [56] # List[int] | Filter by group(s)  (optional)
    patient_id = ['patient_id_example'] # List[str] | Filter by patient_id(s)  (optional)

    try:
        api_response = api_instance.api_patients_get_all_patients_without_job_restrictaion(limit=limit, offset=offset, order=order, search=search, group_id=group_id, patient_id=patient_id)
        print("The response of DefaultApi->api_patients_get_all_patients_without_job_restrictaion:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_patients_get_all_patients_without_job_restrictaion: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Limit the number of returned roles to indicated value (default 50).  | [optional] [default to 50]
 **offset** | **int**| When set the query returns &#39;limit&#39; number of roles but skip &#39;offset&#39; number of roles at the beginning.  | [optional] [default to 0]
 **order** | **str**| Specify the order of the listing: desc, asc  | [optional] 
 **search** | **str**| Specify a search string  | [optional] 
 **group_id** | [**List[int]**](int.md)| Filter by group(s)  | [optional] 
 **patient_id** | [**List[str]**](str.md)| Filter by patient_id(s)  | [optional] 

### Return type

[**ApiPatientsGetAllPatients200Response**](ApiPatientsGetAllPatients200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get all resource |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_patients_get_patient**
> ApiPatientsGetAllPatients200ResponseItemsInner api_patients_get_patient(patient_id)



Returns the current settings for the patient. 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_patients
from core_patients.models.api_patients_get_all_patients200_response_items_inner import ApiPatientsGetAllPatients200ResponseItemsInner
from core_patients.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/patients
# See configuration.py for a list of all supported configuration parameters.
configuration = core_patients.Configuration(
    host = "https://launchpad.imbio.com/api/v1/patients"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_patients.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_patients.DefaultApi(api_client)
    patient_id = 'patient_id_example' # str | The unique identifier of a patient

    try:
        api_response = api_instance.api_patients_get_patient(patient_id)
        print("The response of DefaultApi->api_patients_get_patient:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_patients_get_patient: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patient_id** | **str**| The unique identifier of a patient | 

### Return type

[**ApiPatientsGetAllPatients200ResponseItemsInner**](ApiPatientsGetAllPatients200ResponseItemsInner.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | details of a patient |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_patients_post_patient**
> ApiPatientsPostPatient200Response api_patients_post_patient(api_patients_post_patient_request)



Creates new patient endpoint

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_patients
from core_patients.models.api_patients_post_patient200_response import ApiPatientsPostPatient200Response
from core_patients.models.api_patients_post_patient_request import ApiPatientsPostPatientRequest
from core_patients.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/patients
# See configuration.py for a list of all supported configuration parameters.
configuration = core_patients.Configuration(
    host = "https://launchpad.imbio.com/api/v1/patients"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_patients.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_patients.DefaultApi(api_client)
    api_patients_post_patient_request = core_patients.ApiPatientsPostPatientRequest() # ApiPatientsPostPatientRequest | Initial properties of the patient.

    try:
        api_response = api_instance.api_patients_post_patient(api_patients_post_patient_request)
        print("The response of DefaultApi->api_patients_post_patient:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_patients_post_patient: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_patients_post_patient_request** | [**ApiPatientsPostPatientRequest**](ApiPatientsPostPatientRequest.md)| Initial properties of the patient. | 

### Return type

[**ApiPatientsPostPatient200Response**](ApiPatientsPostPatient200Response.md)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

