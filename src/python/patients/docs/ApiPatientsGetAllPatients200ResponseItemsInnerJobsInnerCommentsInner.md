# ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner

description of a comment in response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the comment | [optional] 
**created_at** | **str** | timestamp when the comment was created | [optional] 
**modified_at** | **str** | timestamp when the comment was modified | [optional] 
**value** | **str** | content of the comment | [optional] 
**owner_id** | **int** | The id of the owner  | [optional] 
**modified_by** | [**ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerOwner**](ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerOwner.md) |  | [optional] 
**allowed_actions** | [**ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions**](ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInnerAllowedActions.md) |  | [optional] 

## Example

```python
from core_patients.models.api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner import ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner from a JSON string
api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_instance = ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner.from_json(json)
# print the JSON string representation of the object
print ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner.to_json()

# convert the object into a dict
api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_dict = api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_instance.to_dict()
# create an instance of ApiPatientsGetAllPatients200ResponseItemsInnerJobsInnerCommentsInner from a dict
api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_form_dict = api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner.from_dict(api_patients_get_all_patients200_response_items_inner_jobs_inner_comments_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


