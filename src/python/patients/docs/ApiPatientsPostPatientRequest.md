# ApiPatientsPostPatientRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**patient_id** | **str** |  | 
**patient_name** | **str** |  | [optional] 
**patient_birthdate** | **str** |  | [optional] 
**patient_age** | **int** |  | [optional] 
**patient_sex** | **str** |  | [optional] 
**group_id** | **int** |  | 

## Example

```python
from core_patients.models.api_patients_post_patient_request import ApiPatientsPostPatientRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiPatientsPostPatientRequest from a JSON string
api_patients_post_patient_request_instance = ApiPatientsPostPatientRequest.from_json(json)
# print the JSON string representation of the object
print ApiPatientsPostPatientRequest.to_json()

# convert the object into a dict
api_patients_post_patient_request_dict = api_patients_post_patient_request_instance.to_dict()
# create an instance of ApiPatientsPostPatientRequest from a dict
api_patients_post_patient_request_form_dict = api_patients_post_patient_request.from_dict(api_patients_post_patient_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


