# ApiAlgorithmsPostAlgorithmRequest

Description of a new algorithm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the algorithm  | 
**description** | **str** | The description of the algorithm  | [optional] 
**active** | **bool** | Shows if this algorithm can be run by jobs  | [optional] 
**post_treatment_algorithm** | **bool** | Shows if the algorithm is a post treatment algorithm  | [optional] 
**archive_path** | **str** | The path to the algorithm archive  | 
**compute_instance_type** | **str** | The instance type required to run the algorithm  | 
**shell_command** | **str** | The command to run in the algorithm&#39;s docker container  | 
**validator_orthanc** | **str** | Contains the validator template that will be run on the Study  | [optional] 
**required_number_of_assets** | **int** |  | 
**logo_url** | **str** | The default url for an algorithm  | [optional] 
**series_description** | **List[object]** | Description of the required series  | [optional] 

## Example

```python
from core_algorithms.models.api_algorithms_post_algorithm_request import ApiAlgorithmsPostAlgorithmRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiAlgorithmsPostAlgorithmRequest from a JSON string
api_algorithms_post_algorithm_request_instance = ApiAlgorithmsPostAlgorithmRequest.from_json(json)
# print the JSON string representation of the object
print ApiAlgorithmsPostAlgorithmRequest.to_json()

# convert the object into a dict
api_algorithms_post_algorithm_request_dict = api_algorithms_post_algorithm_request_instance.to_dict()
# create an instance of ApiAlgorithmsPostAlgorithmRequest from a dict
api_algorithms_post_algorithm_request_form_dict = api_algorithms_post_algorithm_request.from_dict(api_algorithms_post_algorithm_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


