# ApiAlgorithmsGetAlgorithmArchiveDownloadUrl200Response

Contains the download url for an algorithm archive

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**download_url** | **str** | The download url  | [optional] 

## Example

```python
from core_algorithms.models.api_algorithms_get_algorithm_archive_download_url200_response import ApiAlgorithmsGetAlgorithmArchiveDownloadUrl200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiAlgorithmsGetAlgorithmArchiveDownloadUrl200Response from a JSON string
api_algorithms_get_algorithm_archive_download_url200_response_instance = ApiAlgorithmsGetAlgorithmArchiveDownloadUrl200Response.from_json(json)
# print the JSON string representation of the object
print ApiAlgorithmsGetAlgorithmArchiveDownloadUrl200Response.to_json()

# convert the object into a dict
api_algorithms_get_algorithm_archive_download_url200_response_dict = api_algorithms_get_algorithm_archive_download_url200_response_instance.to_dict()
# create an instance of ApiAlgorithmsGetAlgorithmArchiveDownloadUrl200Response from a dict
api_algorithms_get_algorithm_archive_download_url200_response_form_dict = api_algorithms_get_algorithm_archive_download_url200_response.from_dict(api_algorithms_get_algorithm_archive_download_url200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


