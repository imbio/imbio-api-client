# core_algorithms.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/algorithms*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_algorithms_delete_algorithm**](DefaultApi.md#api_algorithms_delete_algorithm) | **DELETE** /{algorithm_id} | 
[**api_algorithms_get_algorithm**](DefaultApi.md#api_algorithms_get_algorithm) | **GET** /{algorithm_id} | 
[**api_algorithms_get_algorithm_archive_download_url**](DefaultApi.md#api_algorithms_get_algorithm_archive_download_url) | **GET** /{algorithm_id}/download_url | 
[**api_algorithms_get_all_algorithms**](DefaultApi.md#api_algorithms_get_all_algorithms) | **GET** / | 
[**api_algorithms_post_algorithm**](DefaultApi.md#api_algorithms_post_algorithm) | **POST** / | 
[**api_algorithms_post_sync_algorithms**](DefaultApi.md#api_algorithms_post_sync_algorithms) | **POST** /actions/sync_algorithms | 
[**api_algorithms_put_algorithm**](DefaultApi.md#api_algorithms_put_algorithm) | **PUT** /{algorithm_id} | 


# **api_algorithms_delete_algorithm**
> api_algorithms_delete_algorithm(algorithm_id)



Deletes one algorithm. 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_algorithms
from core_algorithms.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/algorithms
# See configuration.py for a list of all supported configuration parameters.
configuration = core_algorithms.Configuration(
    host = "https://launchpad.imbio.com/api/v1/algorithms"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_algorithms.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_algorithms.DefaultApi(api_client)
    algorithm_id = 'algorithm_id_example' # str | The unique identifier of an algorithm

    try:
        api_instance.api_algorithms_delete_algorithm(algorithm_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_algorithms_delete_algorithm: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **algorithm_id** | **str**| The unique identifier of an algorithm | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_algorithms_get_algorithm**
> ApiAlgorithmsGetAllAlgorithms200ResponseItemsInner api_algorithms_get_algorithm(algorithm_id)



Returns the current settings for the algorithm. 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_algorithms
from core_algorithms.models.api_algorithms_get_all_algorithms200_response_items_inner import ApiAlgorithmsGetAllAlgorithms200ResponseItemsInner
from core_algorithms.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/algorithms
# See configuration.py for a list of all supported configuration parameters.
configuration = core_algorithms.Configuration(
    host = "https://launchpad.imbio.com/api/v1/algorithms"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_algorithms.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_algorithms.DefaultApi(api_client)
    algorithm_id = 'algorithm_id_example' # str | The unique identifier of an algorithm

    try:
        api_response = api_instance.api_algorithms_get_algorithm(algorithm_id)
        print("The response of DefaultApi->api_algorithms_get_algorithm:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_algorithms_get_algorithm: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **algorithm_id** | **str**| The unique identifier of an algorithm | 

### Return type

[**ApiAlgorithmsGetAllAlgorithms200ResponseItemsInner**](ApiAlgorithmsGetAllAlgorithms200ResponseItemsInner.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The settings for the algorithm. |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_algorithms_get_algorithm_archive_download_url**
> ApiAlgorithmsGetAlgorithmArchiveDownloadUrl200Response api_algorithms_get_algorithm_archive_download_url(algorithm_id)



Get download url for algorithm archive

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_algorithms
from core_algorithms.models.api_algorithms_get_algorithm_archive_download_url200_response import ApiAlgorithmsGetAlgorithmArchiveDownloadUrl200Response
from core_algorithms.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/algorithms
# See configuration.py for a list of all supported configuration parameters.
configuration = core_algorithms.Configuration(
    host = "https://launchpad.imbio.com/api/v1/algorithms"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_algorithms.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_algorithms.DefaultApi(api_client)
    algorithm_id = 'algorithm_id_example' # str | The unique identifier of an algorithm

    try:
        api_response = api_instance.api_algorithms_get_algorithm_archive_download_url(algorithm_id)
        print("The response of DefaultApi->api_algorithms_get_algorithm_archive_download_url:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_algorithms_get_algorithm_archive_download_url: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **algorithm_id** | **str**| The unique identifier of an algorithm | 

### Return type

[**ApiAlgorithmsGetAlgorithmArchiveDownloadUrl200Response**](ApiAlgorithmsGetAlgorithmArchiveDownloadUrl200Response.md)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the algorithm archive download url |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_algorithms_get_all_algorithms**
> ApiAlgorithmsGetAllAlgorithms200Response api_algorithms_get_all_algorithms(limit=limit, offset=offset, order=order, search=search, item_id=item_id, workflow_id=workflow_id, response_format=response_format, order_by=order_by, active=active, compute_instance_type=compute_instance_type)



Returns all algorithms descriptions (but not the archive)

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_algorithms
from core_algorithms.models.api_algorithms_get_all_algorithms200_response import ApiAlgorithmsGetAllAlgorithms200Response
from core_algorithms.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/algorithms
# See configuration.py for a list of all supported configuration parameters.
configuration = core_algorithms.Configuration(
    host = "https://launchpad.imbio.com/api/v1/algorithms"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_algorithms.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_algorithms.DefaultApi(api_client)
    limit = 50 # int | Limit the number of returned roles to indicated value (default 50).  (optional) (default to 50)
    offset = 0 # int | When set the query returns 'limit' number of roles but skip 'offset' number of roles at the beginning.  (optional) (default to 0)
    order = 'order_example' # str | Specify the order of the listing: desc, asc  (optional)
    search = 'search_example' # str | Specify a search string  (optional)
    item_id = [56] # List[int] | Filter resource by their primary id  (optional)
    workflow_id = [56] # List[int] | Filter by workflow(s)  (optional)
    response_format = 'response_format_example' # str | How detialed the response should be  (optional)
    order_by = 'order_by_example' # str | Order by the provided argument name.  (optional)
    active = True # bool | Filter for active or inactive algorithms  (optional)
    compute_instance_type = ['compute_instance_type_example'] # List[str] | Filter for instance type  (optional)

    try:
        api_response = api_instance.api_algorithms_get_all_algorithms(limit=limit, offset=offset, order=order, search=search, item_id=item_id, workflow_id=workflow_id, response_format=response_format, order_by=order_by, active=active, compute_instance_type=compute_instance_type)
        print("The response of DefaultApi->api_algorithms_get_all_algorithms:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_algorithms_get_all_algorithms: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Limit the number of returned roles to indicated value (default 50).  | [optional] [default to 50]
 **offset** | **int**| When set the query returns &#39;limit&#39; number of roles but skip &#39;offset&#39; number of roles at the beginning.  | [optional] [default to 0]
 **order** | **str**| Specify the order of the listing: desc, asc  | [optional] 
 **search** | **str**| Specify a search string  | [optional] 
 **item_id** | [**List[int]**](int.md)| Filter resource by their primary id  | [optional] 
 **workflow_id** | [**List[int]**](int.md)| Filter by workflow(s)  | [optional] 
 **response_format** | **str**| How detialed the response should be  | [optional] 
 **order_by** | **str**| Order by the provided argument name.  | [optional] 
 **active** | **bool**| Filter for active or inactive algorithms  | [optional] 
 **compute_instance_type** | [**List[str]**](str.md)| Filter for instance type  | [optional] 

### Return type

[**ApiAlgorithmsGetAllAlgorithms200Response**](ApiAlgorithmsGetAllAlgorithms200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get all resource |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_algorithms_post_algorithm**
> ApiAlgorithmsPostAlgorithm200Response api_algorithms_post_algorithm(api_algorithms_post_algorithm_request)



Create a new algorithm description; this will not yet contain the archive, the archive will be uploaded after this is created 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_algorithms
from core_algorithms.models.api_algorithms_post_algorithm200_response import ApiAlgorithmsPostAlgorithm200Response
from core_algorithms.models.api_algorithms_post_algorithm_request import ApiAlgorithmsPostAlgorithmRequest
from core_algorithms.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/algorithms
# See configuration.py for a list of all supported configuration parameters.
configuration = core_algorithms.Configuration(
    host = "https://launchpad.imbio.com/api/v1/algorithms"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_algorithms.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_algorithms.DefaultApi(api_client)
    api_algorithms_post_algorithm_request = core_algorithms.ApiAlgorithmsPostAlgorithmRequest() # ApiAlgorithmsPostAlgorithmRequest | Initial properties of the algorithm. 

    try:
        api_response = api_instance.api_algorithms_post_algorithm(api_algorithms_post_algorithm_request)
        print("The response of DefaultApi->api_algorithms_post_algorithm:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_algorithms_post_algorithm: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_algorithms_post_algorithm_request** | [**ApiAlgorithmsPostAlgorithmRequest**](ApiAlgorithmsPostAlgorithmRequest.md)| Initial properties of the algorithm.  | 

### Return type

[**ApiAlgorithmsPostAlgorithm200Response**](ApiAlgorithmsPostAlgorithm200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_algorithms_post_sync_algorithms**
> api_algorithms_post_sync_algorithms()



Create a new algorithm description; this will not yet contain the archive, the archive will be uploaded after this is created 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_algorithms
from core_algorithms.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/algorithms
# See configuration.py for a list of all supported configuration parameters.
configuration = core_algorithms.Configuration(
    host = "https://launchpad.imbio.com/api/v1/algorithms"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_algorithms.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_algorithms.DefaultApi(api_client)

    try:
        api_instance.api_algorithms_post_sync_algorithms()
    except Exception as e:
        print("Exception when calling DefaultApi->api_algorithms_post_sync_algorithms: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_algorithms_put_algorithm**
> api_algorithms_put_algorithm(algorithm_id, api_algorithms_put_algorithm_request)



Updates the current settings of this algorithm. 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_algorithms
from core_algorithms.models.api_algorithms_put_algorithm_request import ApiAlgorithmsPutAlgorithmRequest
from core_algorithms.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/algorithms
# See configuration.py for a list of all supported configuration parameters.
configuration = core_algorithms.Configuration(
    host = "https://launchpad.imbio.com/api/v1/algorithms"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_algorithms.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_algorithms.DefaultApi(api_client)
    algorithm_id = 'algorithm_id_example' # str | The unique identifier of an algorithm
    api_algorithms_put_algorithm_request = core_algorithms.ApiAlgorithmsPutAlgorithmRequest() # ApiAlgorithmsPutAlgorithmRequest | Properties of the algorithm. 

    try:
        api_instance.api_algorithms_put_algorithm(algorithm_id, api_algorithms_put_algorithm_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_algorithms_put_algorithm: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **algorithm_id** | **str**| The unique identifier of an algorithm | 
 **api_algorithms_put_algorithm_request** | [**ApiAlgorithmsPutAlgorithmRequest**](ApiAlgorithmsPutAlgorithmRequest.md)| Properties of the algorithm.  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

