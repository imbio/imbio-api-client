# ApiAlgorithmsGetAllAlgorithms200ResponseItemsInner

Description of a returned algorithm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Algorithm Id  | [optional] 
**name** | **str** | The name of the algorithm  | [optional] 
**description** | **str** | The description of the algorithm  | [optional] 
**version** | **str** | The version of the algorithm  | [optional] 
**active** | **bool** | The algorithm can only be run if this is set to true  | [optional] 
**post_treatment_algorithm** | **bool** | Shows if the algorithm is a post treatment algorithm  | [optional] 
**archive_path** | **str** | The path to the algorithm archive  | [optional] 
**archive_name** | **str** | The path to the algorithm archive  | [optional] 
**compute_instance_type** | **str** | The instance type required to run the algorithm  | [optional] 
**shell_command** | **str** | The command to run in the algorithm&#39;s docker container  | [optional] 
**owner_id** | **int** | The id of the owner  | [optional] 
**validator_orthanc** | **str** | Contains the validator template that will be run on the Study  | [optional] 
**required_number_of_assets** | **int** |  | [optional] 
**logo_url** | **str** | The default url for an algorithm  | [optional] 
**show_arguments** | **List[object]** | Shows the algorithm arguments that should be displayed on the create job form  | [optional] 
**series_description** | **List[object]** | Description of the required series  | [optional] 
**workflows** | [**List[ApiAlgorithmsGetAllAlgorithms200ResponseItemsInnerWorkflowsInner]**](ApiAlgorithmsGetAllAlgorithms200ResponseItemsInnerWorkflowsInner.md) | Id of workflows, that the algorithm belongs to | [optional] 

## Example

```python
from core_algorithms.models.api_algorithms_get_all_algorithms200_response_items_inner import ApiAlgorithmsGetAllAlgorithms200ResponseItemsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiAlgorithmsGetAllAlgorithms200ResponseItemsInner from a JSON string
api_algorithms_get_all_algorithms200_response_items_inner_instance = ApiAlgorithmsGetAllAlgorithms200ResponseItemsInner.from_json(json)
# print the JSON string representation of the object
print ApiAlgorithmsGetAllAlgorithms200ResponseItemsInner.to_json()

# convert the object into a dict
api_algorithms_get_all_algorithms200_response_items_inner_dict = api_algorithms_get_all_algorithms200_response_items_inner_instance.to_dict()
# create an instance of ApiAlgorithmsGetAllAlgorithms200ResponseItemsInner from a dict
api_algorithms_get_all_algorithms200_response_items_inner_form_dict = api_algorithms_get_all_algorithms200_response_items_inner.from_dict(api_algorithms_get_all_algorithms200_response_items_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


