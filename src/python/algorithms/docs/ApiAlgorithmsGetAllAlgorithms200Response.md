# ApiAlgorithmsGetAllAlgorithms200Response


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**num_of_all** | **int** | How many algorithms there are currently  | [optional] 
**index** | **int** | Algorithms are returned from this index  | [optional] 
**items** | [**List[ApiAlgorithmsGetAllAlgorithms200ResponseItemsInner]**](ApiAlgorithmsGetAllAlgorithms200ResponseItemsInner.md) | The list of algorithms returned from in this request  | [optional] 

## Example

```python
from core_algorithms.models.api_algorithms_get_all_algorithms200_response import ApiAlgorithmsGetAllAlgorithms200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiAlgorithmsGetAllAlgorithms200Response from a JSON string
api_algorithms_get_all_algorithms200_response_instance = ApiAlgorithmsGetAllAlgorithms200Response.from_json(json)
# print the JSON string representation of the object
print ApiAlgorithmsGetAllAlgorithms200Response.to_json()

# convert the object into a dict
api_algorithms_get_all_algorithms200_response_dict = api_algorithms_get_all_algorithms200_response_instance.to_dict()
# create an instance of ApiAlgorithmsGetAllAlgorithms200Response from a dict
api_algorithms_get_all_algorithms200_response_form_dict = api_algorithms_get_all_algorithms200_response.from_dict(api_algorithms_get_all_algorithms200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


