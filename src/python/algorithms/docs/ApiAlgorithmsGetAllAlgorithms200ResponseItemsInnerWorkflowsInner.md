# ApiAlgorithmsGetAllAlgorithms200ResponseItemsInnerWorkflowsInner

Workflow object description

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from core_algorithms.models.api_algorithms_get_all_algorithms200_response_items_inner_workflows_inner import ApiAlgorithmsGetAllAlgorithms200ResponseItemsInnerWorkflowsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiAlgorithmsGetAllAlgorithms200ResponseItemsInnerWorkflowsInner from a JSON string
api_algorithms_get_all_algorithms200_response_items_inner_workflows_inner_instance = ApiAlgorithmsGetAllAlgorithms200ResponseItemsInnerWorkflowsInner.from_json(json)
# print the JSON string representation of the object
print ApiAlgorithmsGetAllAlgorithms200ResponseItemsInnerWorkflowsInner.to_json()

# convert the object into a dict
api_algorithms_get_all_algorithms200_response_items_inner_workflows_inner_dict = api_algorithms_get_all_algorithms200_response_items_inner_workflows_inner_instance.to_dict()
# create an instance of ApiAlgorithmsGetAllAlgorithms200ResponseItemsInnerWorkflowsInner from a dict
api_algorithms_get_all_algorithms200_response_items_inner_workflows_inner_form_dict = api_algorithms_get_all_algorithms200_response_items_inner_workflows_inner.from_dict(api_algorithms_get_all_algorithms200_response_items_inner_workflows_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


