# ApiAlgorithmsPutAlgorithmRequest

Description of a new algorithm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the algorithm  | [optional] 
**description** | **str** | The description of the algorithm  | [optional] 
**active** | **bool** | Shows if this algorithm can be run by jobs  | [optional] 
**post_treatment_algorithm** | **bool** | Shows if the algorithm is a post treatment algorithm  | [optional] 
**archive_path** | **str** | The path to the algorithm archive  | [optional] 
**compute_instance_type** | **str** | The instance type required to run the algorithm  | [optional] 
**shell_command** | **str** | The command to run in the algorithm&#39;s docker container  | [optional] 
**validator_orthanc** | **str** | Contains the validator template that will be run on the Study  | [optional] 
**required_number_of_assets** | **int** |  | [optional] 
**logo_url** | **str** | The default url for an algorithm  | [optional] 
**series_description** | **List[object]** | Description of the required series  | [optional] 

## Example

```python
from core_algorithms.models.api_algorithms_put_algorithm_request import ApiAlgorithmsPutAlgorithmRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiAlgorithmsPutAlgorithmRequest from a JSON string
api_algorithms_put_algorithm_request_instance = ApiAlgorithmsPutAlgorithmRequest.from_json(json)
# print the JSON string representation of the object
print ApiAlgorithmsPutAlgorithmRequest.to_json()

# convert the object into a dict
api_algorithms_put_algorithm_request_dict = api_algorithms_put_algorithm_request_instance.to_dict()
# create an instance of ApiAlgorithmsPutAlgorithmRequest from a dict
api_algorithms_put_algorithm_request_form_dict = api_algorithms_put_algorithm_request.from_dict(api_algorithms_put_algorithm_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


