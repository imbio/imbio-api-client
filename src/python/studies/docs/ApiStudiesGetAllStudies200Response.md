# ApiStudiesGetAllStudies200Response

The paginated response for get all studies request

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**num_of_all** | **int** | How many studies there are currently | [optional] 
**index** | **int** | Studies returned from this index | [optional] 
**items** | [**List[ApiStudiesGetAllStudies200ResponseItemsInner]**](ApiStudiesGetAllStudies200ResponseItemsInner.md) | The list of returned studies | [optional] 

## Example

```python
from core_studies.models.api_studies_get_all_studies200_response import ApiStudiesGetAllStudies200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiStudiesGetAllStudies200Response from a JSON string
api_studies_get_all_studies200_response_instance = ApiStudiesGetAllStudies200Response.from_json(json)
# print the JSON string representation of the object
print ApiStudiesGetAllStudies200Response.to_json()

# convert the object into a dict
api_studies_get_all_studies200_response_dict = api_studies_get_all_studies200_response_instance.to_dict()
# create an instance of ApiStudiesGetAllStudies200Response from a dict
api_studies_get_all_studies200_response_form_dict = api_studies_get_all_studies200_response.from_dict(api_studies_get_all_studies200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


