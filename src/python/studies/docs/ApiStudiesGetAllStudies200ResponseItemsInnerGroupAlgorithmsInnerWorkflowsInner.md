# ApiStudiesGetAllStudies200ResponseItemsInnerGroupAlgorithmsInnerWorkflowsInner

Workflow object description

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from core_studies.models.api_studies_get_all_studies200_response_items_inner_group_algorithms_inner_workflows_inner import ApiStudiesGetAllStudies200ResponseItemsInnerGroupAlgorithmsInnerWorkflowsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiStudiesGetAllStudies200ResponseItemsInnerGroupAlgorithmsInnerWorkflowsInner from a JSON string
api_studies_get_all_studies200_response_items_inner_group_algorithms_inner_workflows_inner_instance = ApiStudiesGetAllStudies200ResponseItemsInnerGroupAlgorithmsInnerWorkflowsInner.from_json(json)
# print the JSON string representation of the object
print ApiStudiesGetAllStudies200ResponseItemsInnerGroupAlgorithmsInnerWorkflowsInner.to_json()

# convert the object into a dict
api_studies_get_all_studies200_response_items_inner_group_algorithms_inner_workflows_inner_dict = api_studies_get_all_studies200_response_items_inner_group_algorithms_inner_workflows_inner_instance.to_dict()
# create an instance of ApiStudiesGetAllStudies200ResponseItemsInnerGroupAlgorithmsInnerWorkflowsInner from a dict
api_studies_get_all_studies200_response_items_inner_group_algorithms_inner_workflows_inner_form_dict = api_studies_get_all_studies200_response_items_inner_group_algorithms_inner_workflows_inner.from_dict(api_studies_get_all_studies200_response_items_inner_group_algorithms_inner_workflows_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


