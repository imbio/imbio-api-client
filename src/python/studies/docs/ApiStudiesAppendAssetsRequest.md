# ApiStudiesAppendAssetsRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assets** | **List[int]** | List of asset ids to append to the study | 

## Example

```python
from core_studies.models.api_studies_append_assets_request import ApiStudiesAppendAssetsRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiStudiesAppendAssetsRequest from a JSON string
api_studies_append_assets_request_instance = ApiStudiesAppendAssetsRequest.from_json(json)
# print the JSON string representation of the object
print ApiStudiesAppendAssetsRequest.to_json()

# convert the object into a dict
api_studies_append_assets_request_dict = api_studies_append_assets_request_instance.to_dict()
# create an instance of ApiStudiesAppendAssetsRequest from a dict
api_studies_append_assets_request_form_dict = api_studies_append_assets_request.from_dict(api_studies_append_assets_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


