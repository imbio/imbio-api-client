# ApiStudiesGetStudy404Response

The error object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **int** | The error code for this error | [optional] 
**message** | **str** | The error message for this error | [optional] 

## Example

```python
from core_studies.models.api_studies_get_study404_response import ApiStudiesGetStudy404Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiStudiesGetStudy404Response from a JSON string
api_studies_get_study404_response_instance = ApiStudiesGetStudy404Response.from_json(json)
# print the JSON string representation of the object
print ApiStudiesGetStudy404Response.to_json()

# convert the object into a dict
api_studies_get_study404_response_dict = api_studies_get_study404_response_instance.to_dict()
# create an instance of ApiStudiesGetStudy404Response from a dict
api_studies_get_study404_response_form_dict = api_studies_get_study404_response.from_dict(api_studies_get_study404_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


