# ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAssetsInner

Study properties parsed from the DICOM study that will be used by the UI/bridge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Asset Id | [optional] 
**name** | **str** | An optional name to the asset  | [optional] 
**asset_type** | **str** | The type of the asset  | [optional] 
**uploaded_at** | **str** | The time when the asset was uploaded  | [optional] 
**owner_id** | **int** | The id of the owner  | [optional] 
**group_id** | **int** | The id of the group whom the asset belongs to  | [optional] 
**study_id** | **int** | The id of the study whom the asset belongs to  | [optional] 
**study_date** | **str** | The date when the study was created  | [optional] 
**patient_name** | **str** | The name of the patient  | [optional] 
**accession_number** | **str** | The accession of the study  | [optional] 
**deidentified** | **bool** | The asset was deidentified  | [optional] 
**has_archive** | **bool** | The asset has an archive file  | [optional] 
**has_csv_report** | **bool** | The asset has csv report  | [optional] 
**has_pdf_report** | **bool** | The asset has pdf report  | [optional] 
**cleaned_up** | **bool** | The asset archives have been removed from the asset  | [optional] 
**series_options_log** | **object** | Option that the series was selected from | [optional] 
**deid_values** | **object** | Object containing deidentified dicom attributes and their values | [optional] 

## Example

```python
from core_studies.models.api_studies_get_all_studies200_response_items_inner_jobs_inner_assets_inner import ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAssetsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAssetsInner from a JSON string
api_studies_get_all_studies200_response_items_inner_jobs_inner_assets_inner_instance = ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAssetsInner.from_json(json)
# print the JSON string representation of the object
print ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAssetsInner.to_json()

# convert the object into a dict
api_studies_get_all_studies200_response_items_inner_jobs_inner_assets_inner_dict = api_studies_get_all_studies200_response_items_inner_jobs_inner_assets_inner_instance.to_dict()
# create an instance of ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAssetsInner from a dict
api_studies_get_all_studies200_response_items_inner_jobs_inner_assets_inner_form_dict = api_studies_get_all_studies200_response_items_inner_jobs_inner_assets_inner.from_dict(api_studies_get_all_studies200_response_items_inner_jobs_inner_assets_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


