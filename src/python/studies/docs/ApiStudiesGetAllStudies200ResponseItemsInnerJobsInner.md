# ApiStudiesGetAllStudies200ResponseItemsInnerJobsInner


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of a job | [optional] 
**name** | **str** | name of a job | [optional] 
**workflows** | [**List[ApiStudiesGetAllStudies200ResponseItemsInnerGroupAlgorithmsInnerWorkflowsInner]**](ApiStudiesGetAllStudies200ResponseItemsInnerGroupAlgorithmsInnerWorkflowsInner.md) |  | [optional] 
**algorithm** | [**ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm**](ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm.md) |  | [optional] 
**status** | **str** | status of a job | [optional] 
**created_at** | **str** | The time when the job was created | [optional] 
**output_asset_id** | **int** | output asset id | [optional] 
**assets** | [**List[ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAssetsInner]**](ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAssetsInner.md) |  | [optional] 

## Example

```python
from core_studies.models.api_studies_get_all_studies200_response_items_inner_jobs_inner import ApiStudiesGetAllStudies200ResponseItemsInnerJobsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiStudiesGetAllStudies200ResponseItemsInnerJobsInner from a JSON string
api_studies_get_all_studies200_response_items_inner_jobs_inner_instance = ApiStudiesGetAllStudies200ResponseItemsInnerJobsInner.from_json(json)
# print the JSON string representation of the object
print ApiStudiesGetAllStudies200ResponseItemsInnerJobsInner.to_json()

# convert the object into a dict
api_studies_get_all_studies200_response_items_inner_jobs_inner_dict = api_studies_get_all_studies200_response_items_inner_jobs_inner_instance.to_dict()
# create an instance of ApiStudiesGetAllStudies200ResponseItemsInnerJobsInner from a dict
api_studies_get_all_studies200_response_items_inner_jobs_inner_form_dict = api_studies_get_all_studies200_response_items_inner_jobs_inner.from_dict(api_studies_get_all_studies200_response_items_inner_jobs_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


