# ApiStudiesPostStudyRequest

Study properties parsed from the DICOM study that will be used by the UI/bridge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **str** | The UID of the study (StudyInstanceUID)  | [optional] 
**study_date** | **str** | The time when the study was created  | [optional] 
**patient_id** | **int** | The patient&#39;s ID from ICCP  | [optional] 
**patient_name** | **str** | The patient&#39;s name  | [optional] 
**patient_birthdate** | **str** | The patient&#39;s birthdate  | [optional] 
**patient_age** | **int** | The patient&#39;s age in year  | [optional] 
**patient_sex** | **str** | The gender of the patient  | [optional] 
**accession_number** | **str** | The accession number  | [optional] 
**group_id** | **int** | The group&#39;s id  | [optional] 

## Example

```python
from core_studies.models.api_studies_post_study_request import ApiStudiesPostStudyRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiStudiesPostStudyRequest from a JSON string
api_studies_post_study_request_instance = ApiStudiesPostStudyRequest.from_json(json)
# print the JSON string representation of the object
print ApiStudiesPostStudyRequest.to_json()

# convert the object into a dict
api_studies_post_study_request_dict = api_studies_post_study_request_instance.to_dict()
# create an instance of ApiStudiesPostStudyRequest from a dict
api_studies_post_study_request_form_dict = api_studies_post_study_request.from_dict(api_studies_post_study_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


