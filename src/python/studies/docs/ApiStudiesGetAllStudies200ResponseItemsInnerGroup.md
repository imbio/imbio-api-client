# ApiStudiesGetAllStudies200ResponseItemsInnerGroup

group's information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Group Id | [optional] 
**name** | **str** | The name used by a group | 
**description** | **str** | description of a group | [optional] 
**review_workflow_enabled** | **bool** | Shows if the review workflow is enabled | [optional] 
**qa_workflow_enabled** | **bool** | Shows if the qa review workflow is enabled | [optional] 
**overread_workflow_enabled** | **bool** | Shows if the overread workflow is enabled | [optional] 
**overread_request_enabled** | **bool** | Flag to decide whether overread request functionality is enabled for the group | [optional] 
**seg_editor_enabled** | **bool** | Shows if the segmentation editor is enabled | [optional] 
**editor_target** | **str** | The segmentation editor in use by a group | [optional] 
**deidentify** | **str** | Flag for deidentify data before uploading | [optional] 
**storage_time** | **int** | How many days to store assets | [optional] 
**research** | **bool** | shows if this group runs algorithms for research | [optional] 
**region** | **str** | shows the region of the group | [optional] 
**timezone** | **str** | shows the timezone of the group | [optional] 
**algorithms** | [**List[ApiStudiesGetAllStudies200ResponseItemsInnerGroupAlgorithmsInner]**](ApiStudiesGetAllStudies200ResponseItemsInnerGroupAlgorithmsInner.md) | Some info of the algorithms belonging to group | [optional] 
**allow_notification_suppress** | **bool** | Disable notification filtering on user level. | [optional] 
**notifications** | [**List[ApiStudiesGetAllStudies200ResponseItemsInnerGroupNotificationsInner]**](ApiStudiesGetAllStudies200ResponseItemsInnerGroupNotificationsInner.md) | Some info of the algorithms belonging to group | [optional] 
**workflows** | [**List[ApiStudiesGetAllStudies200ResponseItemsInnerGroupAlgorithmsInnerWorkflowsInner]**](ApiStudiesGetAllStudies200ResponseItemsInnerGroupAlgorithmsInnerWorkflowsInner.md) | Some info of the algorithms belonging to group | [optional] 
**en_upload_any_series** | **bool** | Lets the group to upload any series including invalide series | [optional] 
**disable_series_preprocessing** | **bool** | Disable series preprocessing | [optional] 

## Example

```python
from core_studies.models.api_studies_get_all_studies200_response_items_inner_group import ApiStudiesGetAllStudies200ResponseItemsInnerGroup

# TODO update the JSON string below
json = "{}"
# create an instance of ApiStudiesGetAllStudies200ResponseItemsInnerGroup from a JSON string
api_studies_get_all_studies200_response_items_inner_group_instance = ApiStudiesGetAllStudies200ResponseItemsInnerGroup.from_json(json)
# print the JSON string representation of the object
print ApiStudiesGetAllStudies200ResponseItemsInnerGroup.to_json()

# convert the object into a dict
api_studies_get_all_studies200_response_items_inner_group_dict = api_studies_get_all_studies200_response_items_inner_group_instance.to_dict()
# create an instance of ApiStudiesGetAllStudies200ResponseItemsInnerGroup from a dict
api_studies_get_all_studies200_response_items_inner_group_form_dict = api_studies_get_all_studies200_response_items_inner_group.from_dict(api_studies_get_all_studies200_response_items_inner_group_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


