# ApiStudiesGetAllStudies200ResponseItemsInner

Study properties parsed from the DICOM study that will be used by the UI/bridge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The id of the study  | [optional] 
**uid** | **str** | The UID of the study  | [optional] 
**study_hash** | **str** | Hash to identify the patient  | [optional] 
**study_date** | **str** | The time when the study was created  | [optional] 
**patient_id** | **int** | The patient&#39;s ID from ICCP  | [optional] 
**patient_name** | **str** | The patient&#39;s name  | [optional] 
**patient_birthdate** | **str** | The patient&#39;s birthdate  | [optional] 
**patient_age** | **int** | The patient&#39;s age in year  | [optional] 
**patient_sex** | **str** | The gender of the patient  | [optional] 
**accession_number** | **str** | The accession number  | [optional] 
**group** | [**ApiStudiesGetAllStudies200ResponseItemsInnerGroup**](ApiStudiesGetAllStudies200ResponseItemsInnerGroup.md) |  | [optional] 
**jobs** | [**List[ApiStudiesGetAllStudies200ResponseItemsInnerJobsInner]**](ApiStudiesGetAllStudies200ResponseItemsInnerJobsInner.md) |  | [optional] 

## Example

```python
from core_studies.models.api_studies_get_all_studies200_response_items_inner import ApiStudiesGetAllStudies200ResponseItemsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiStudiesGetAllStudies200ResponseItemsInner from a JSON string
api_studies_get_all_studies200_response_items_inner_instance = ApiStudiesGetAllStudies200ResponseItemsInner.from_json(json)
# print the JSON string representation of the object
print ApiStudiesGetAllStudies200ResponseItemsInner.to_json()

# convert the object into a dict
api_studies_get_all_studies200_response_items_inner_dict = api_studies_get_all_studies200_response_items_inner_instance.to_dict()
# create an instance of ApiStudiesGetAllStudies200ResponseItemsInner from a dict
api_studies_get_all_studies200_response_items_inner_form_dict = api_studies_get_all_studies200_response_items_inner.from_dict(api_studies_get_all_studies200_response_items_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


