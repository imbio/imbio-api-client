# ApiStudiesGetAllStudies200ResponseItemsInnerGroupNotificationsInner

All notification related information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The notification id  | [optional] 
**type** | **str** | The notification type  | [optional] 
**name** | **str** | The notification name  | [optional] 
**description** | **str** | The notification description  | [optional] 
**notification_group** | **str** | The notification group  | [optional] 
**template** | **str** | The notification template  | [optional] 

## Example

```python
from core_studies.models.api_studies_get_all_studies200_response_items_inner_group_notifications_inner import ApiStudiesGetAllStudies200ResponseItemsInnerGroupNotificationsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiStudiesGetAllStudies200ResponseItemsInnerGroupNotificationsInner from a JSON string
api_studies_get_all_studies200_response_items_inner_group_notifications_inner_instance = ApiStudiesGetAllStudies200ResponseItemsInnerGroupNotificationsInner.from_json(json)
# print the JSON string representation of the object
print ApiStudiesGetAllStudies200ResponseItemsInnerGroupNotificationsInner.to_json()

# convert the object into a dict
api_studies_get_all_studies200_response_items_inner_group_notifications_inner_dict = api_studies_get_all_studies200_response_items_inner_group_notifications_inner_instance.to_dict()
# create an instance of ApiStudiesGetAllStudies200ResponseItemsInnerGroupNotificationsInner from a dict
api_studies_get_all_studies200_response_items_inner_group_notifications_inner_form_dict = api_studies_get_all_studies200_response_items_inner_group_notifications_inner.from_dict(api_studies_get_all_studies200_response_items_inner_group_notifications_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


