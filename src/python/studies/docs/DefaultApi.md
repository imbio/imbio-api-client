# core_studies.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/studies*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_studies_append_assets**](DefaultApi.md#api_studies_append_assets) | **POST** /actions/{study_id}/assets | 
[**api_studies_get_all_studies**](DefaultApi.md#api_studies_get_all_studies) | **GET** / | 
[**api_studies_get_study**](DefaultApi.md#api_studies_get_study) | **GET** /{study_id} | 
[**api_studies_post_study**](DefaultApi.md#api_studies_post_study) | **POST** / | 


# **api_studies_append_assets**
> api_studies_append_assets(study_id, api_studies_append_assets_request)



Creates a rule for the specified ruleset. 

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_studies
from core_studies.models.api_studies_append_assets_request import ApiStudiesAppendAssetsRequest
from core_studies.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/studies
# See configuration.py for a list of all supported configuration parameters.
configuration = core_studies.Configuration(
    host = "https://launchpad.imbio.com/api/v1/studies"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_studies.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_studies.DefaultApi(api_client)
    study_id = 56 # int | The unique identifier of a study
    api_studies_append_assets_request = core_studies.ApiStudiesAppendAssetsRequest() # ApiStudiesAppendAssetsRequest | object containing list of asset ids to append

    try:
        api_instance.api_studies_append_assets(study_id, api_studies_append_assets_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_studies_append_assets: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **study_id** | **int**| The unique identifier of a study | 
 **api_studies_append_assets_request** | [**ApiStudiesAppendAssetsRequest**](ApiStudiesAppendAssetsRequest.md)| object containing list of asset ids to append | 

### Return type

void (empty response body)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_studies_get_all_studies**
> ApiStudiesGetAllStudies200Response api_studies_get_all_studies(limit=limit, offset=offset, order=order, search=search, item_id=item_id, group_id=group_id, study_hash=study_hash, study_instance_uid=study_instance_uid, patient_id=patient_id)



Returns all studies

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_studies
from core_studies.models.api_studies_get_all_studies200_response import ApiStudiesGetAllStudies200Response
from core_studies.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/studies
# See configuration.py for a list of all supported configuration parameters.
configuration = core_studies.Configuration(
    host = "https://launchpad.imbio.com/api/v1/studies"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_studies.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_studies.DefaultApi(api_client)
    limit = 50 # int | Limit the number of returned roles to indicated value (default 50).  (optional) (default to 50)
    offset = 0 # int | When set the query returns 'limit' number of roles but skip 'offset' number of roles at the beginning.  (optional) (default to 0)
    order = 'order_example' # str | Specify the order of the listing: desc, asc  (optional)
    search = 'search_example' # str | Specify a search string  (optional)
    item_id = [56] # List[int] | Filter resource by their primary id  (optional)
    group_id = [56] # List[int] | Filter by group(s)  (optional)
    study_hash = ['study_hash_example'] # List[str] | Filter jobs by study_hash (optional)
    study_instance_uid = 'study_instance_uid_example' # str | Searches on StudyInstanceUID parameter (optional)
    patient_id = 56 # int | Searches on patient_id (db id) parameter (optional)

    try:
        api_response = api_instance.api_studies_get_all_studies(limit=limit, offset=offset, order=order, search=search, item_id=item_id, group_id=group_id, study_hash=study_hash, study_instance_uid=study_instance_uid, patient_id=patient_id)
        print("The response of DefaultApi->api_studies_get_all_studies:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_studies_get_all_studies: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Limit the number of returned roles to indicated value (default 50).  | [optional] [default to 50]
 **offset** | **int**| When set the query returns &#39;limit&#39; number of roles but skip &#39;offset&#39; number of roles at the beginning.  | [optional] [default to 0]
 **order** | **str**| Specify the order of the listing: desc, asc  | [optional] 
 **search** | **str**| Specify a search string  | [optional] 
 **item_id** | [**List[int]**](int.md)| Filter resource by their primary id  | [optional] 
 **group_id** | [**List[int]**](int.md)| Filter by group(s)  | [optional] 
 **study_hash** | [**List[str]**](str.md)| Filter jobs by study_hash | [optional] 
 **study_instance_uid** | **str**| Searches on StudyInstanceUID parameter | [optional] 
 **patient_id** | **int**| Searches on patient_id (db id) parameter | [optional] 

### Return type

[**ApiStudiesGetAllStudies200Response**](ApiStudiesGetAllStudies200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get all resource |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_studies_get_study**
> ApiStudiesGetAllStudies200ResponseItemsInner api_studies_get_study(study_id)



Returns the study by that id 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_studies
from core_studies.models.api_studies_get_all_studies200_response_items_inner import ApiStudiesGetAllStudies200ResponseItemsInner
from core_studies.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/studies
# See configuration.py for a list of all supported configuration parameters.
configuration = core_studies.Configuration(
    host = "https://launchpad.imbio.com/api/v1/studies"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_studies.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_studies.DefaultApi(api_client)
    study_id = 56 # int | The unique identifier of a study

    try:
        api_response = api_instance.api_studies_get_study(study_id)
        print("The response of DefaultApi->api_studies_get_study:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_studies_get_study: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **study_id** | **int**| The unique identifier of a study | 

### Return type

[**ApiStudiesGetAllStudies200ResponseItemsInner**](ApiStudiesGetAllStudies200ResponseItemsInner.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | details of a study |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_studies_post_study**
> ApiStudiesPostStudy200Response api_studies_post_study(api_studies_post_study_request=api_studies_post_study_request)



Create a new study

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_studies
from core_studies.models.api_studies_post_study200_response import ApiStudiesPostStudy200Response
from core_studies.models.api_studies_post_study_request import ApiStudiesPostStudyRequest
from core_studies.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/studies
# See configuration.py for a list of all supported configuration parameters.
configuration = core_studies.Configuration(
    host = "https://launchpad.imbio.com/api/v1/studies"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_studies.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_studies.DefaultApi(api_client)
    api_studies_post_study_request = core_studies.ApiStudiesPostStudyRequest() # ApiStudiesPostStudyRequest | Study data  (optional)

    try:
        api_response = api_instance.api_studies_post_study(api_studies_post_study_request=api_studies_post_study_request)
        print("The response of DefaultApi->api_studies_post_study:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_studies_post_study: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_studies_post_study_request** | [**ApiStudiesPostStudyRequest**](ApiStudiesPostStudyRequest.md)| Study data  | [optional] 

### Return type

[**ApiStudiesPostStudy200Response**](ApiStudiesPostStudy200Response.md)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

