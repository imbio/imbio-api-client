# ApiStudiesPostStudy200Response


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 

## Example

```python
from core_studies.models.api_studies_post_study200_response import ApiStudiesPostStudy200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiStudiesPostStudy200Response from a JSON string
api_studies_post_study200_response_instance = ApiStudiesPostStudy200Response.from_json(json)
# print the JSON string representation of the object
print ApiStudiesPostStudy200Response.to_json()

# convert the object into a dict
api_studies_post_study200_response_dict = api_studies_post_study200_response_instance.to_dict()
# create an instance of ApiStudiesPostStudy200Response from a dict
api_studies_post_study200_response_form_dict = api_studies_post_study200_response.from_dict(api_studies_post_study200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


