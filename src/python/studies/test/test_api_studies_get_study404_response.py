# coding: utf-8

"""
    IMBIO core api for studies

    Methods for managing studies 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_studies.models.api_studies_get_study404_response import ApiStudiesGetStudy404Response

class TestApiStudiesGetStudy404Response(unittest.TestCase):
    """ApiStudiesGetStudy404Response unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiStudiesGetStudy404Response:
        """Test ApiStudiesGetStudy404Response
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiStudiesGetStudy404Response`
        """
        model = ApiStudiesGetStudy404Response()
        if include_optional:
            return ApiStudiesGetStudy404Response(
                code = 56,
                message = ''
            )
        else:
            return ApiStudiesGetStudy404Response(
        )
        """

    def testApiStudiesGetStudy404Response(self):
        """Test ApiStudiesGetStudy404Response"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
