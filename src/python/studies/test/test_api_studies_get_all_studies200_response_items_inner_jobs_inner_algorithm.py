# coding: utf-8

"""
    IMBIO core api for studies

    Methods for managing studies 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_studies.models.api_studies_get_all_studies200_response_items_inner_jobs_inner_algorithm import ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm

class TestApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm(unittest.TestCase):
    """ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm:
        """Test ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm`
        """
        model = ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm()
        if include_optional:
            return ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm(
                id = 56,
                name = '',
                version = '',
                logo_url = '',
                body_part = '',
                description = '',
                validator_orthanc = '',
                required_number_of_assets = 56
            )
        else:
            return ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm(
        )
        """

    def testApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm(self):
        """Test ApiStudiesGetAllStudies200ResponseItemsInnerJobsInnerAlgorithm"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
