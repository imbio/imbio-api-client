# ApiUsersGetUserProfile200ResponseNotificationsInner

All notification related information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The notification id  | [optional] 
**type** | **str** | The notification type  | [optional] 
**name** | **str** | The notification name  | [optional] 
**description** | **str** | The notification description  | [optional] 
**notification_group** | **str** | The notification group  | [optional] 
**template** | **str** | The notification template  | [optional] 
**allow_suppress_by** | **List[int]** |  | [optional] 
**forced_by** | **List[int]** |  | [optional] 
**enabled** | **bool** |  | [optional] 

## Example

```python
from core_users.models.api_users_get_user_profile200_response_notifications_inner import ApiUsersGetUserProfile200ResponseNotificationsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiUsersGetUserProfile200ResponseNotificationsInner from a JSON string
api_users_get_user_profile200_response_notifications_inner_instance = ApiUsersGetUserProfile200ResponseNotificationsInner.from_json(json)
# print the JSON string representation of the object
print ApiUsersGetUserProfile200ResponseNotificationsInner.to_json()

# convert the object into a dict
api_users_get_user_profile200_response_notifications_inner_dict = api_users_get_user_profile200_response_notifications_inner_instance.to_dict()
# create an instance of ApiUsersGetUserProfile200ResponseNotificationsInner from a dict
api_users_get_user_profile200_response_notifications_inner_form_dict = api_users_get_user_profile200_response_notifications_inner.from_dict(api_users_get_user_profile200_response_notifications_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


