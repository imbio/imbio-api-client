# ApiUsersGetAllUsers200ResponseItemsInnerRolesInner


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of a role | [optional] 
**name** | **str** | name of a role | [optional] 

## Example

```python
from core_users.models.api_users_get_all_users200_response_items_inner_roles_inner import ApiUsersGetAllUsers200ResponseItemsInnerRolesInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiUsersGetAllUsers200ResponseItemsInnerRolesInner from a JSON string
api_users_get_all_users200_response_items_inner_roles_inner_instance = ApiUsersGetAllUsers200ResponseItemsInnerRolesInner.from_json(json)
# print the JSON string representation of the object
print ApiUsersGetAllUsers200ResponseItemsInnerRolesInner.to_json()

# convert the object into a dict
api_users_get_all_users200_response_items_inner_roles_inner_dict = api_users_get_all_users200_response_items_inner_roles_inner_instance.to_dict()
# create an instance of ApiUsersGetAllUsers200ResponseItemsInnerRolesInner from a dict
api_users_get_all_users200_response_items_inner_roles_inner_form_dict = api_users_get_all_users200_response_items_inner_roles_inner.from_dict(api_users_get_all_users200_response_items_inner_roles_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


