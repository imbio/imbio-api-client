# core_users.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/users*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_users_create_user**](DefaultApi.md#api_users_create_user) | **POST** / | 
[**api_users_delete_user**](DefaultApi.md#api_users_delete_user) | **DELETE** /{user_id} | 
[**api_users_get_all_users**](DefaultApi.md#api_users_get_all_users) | **GET** / | 
[**api_users_get_user**](DefaultApi.md#api_users_get_user) | **GET** /{user_id} | 
[**api_users_get_user_profile**](DefaultApi.md#api_users_get_user_profile) | **GET** /user-profile | 
[**api_users_put_user**](DefaultApi.md#api_users_put_user) | **PUT** /{user_id} | 
[**api_users_put_user_profile**](DefaultApi.md#api_users_put_user_profile) | **PUT** /user-profile | 


# **api_users_create_user**
> ApiUsersCreateUser200Response api_users_create_user(api_users_create_user_request)



Create a new user

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_users
from core_users.models.api_users_create_user200_response import ApiUsersCreateUser200Response
from core_users.models.api_users_create_user_request import ApiUsersCreateUserRequest
from core_users.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/users
# See configuration.py for a list of all supported configuration parameters.
configuration = core_users.Configuration(
    host = "https://launchpad.imbio.com/api/v1/users"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_users.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_users.DefaultApi(api_client)
    api_users_create_user_request = core_users.ApiUsersCreateUserRequest() # ApiUsersCreateUserRequest | Initial properties of the user.

    try:
        api_response = api_instance.api_users_create_user(api_users_create_user_request)
        print("The response of DefaultApi->api_users_create_user:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_users_create_user: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_users_create_user_request** | [**ApiUsersCreateUserRequest**](ApiUsersCreateUserRequest.md)| Initial properties of the user. | 

### Return type

[**ApiUsersCreateUser200Response**](ApiUsersCreateUser200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_users_delete_user**
> api_users_delete_user(user_id)



Deletes one user

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_users
from core_users.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/users
# See configuration.py for a list of all supported configuration parameters.
configuration = core_users.Configuration(
    host = "https://launchpad.imbio.com/api/v1/users"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_users.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_users.DefaultApi(api_client)
    user_id = 'user_id_example' # str | The unique identifier of a user

    try:
        api_instance.api_users_delete_user(user_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_users_delete_user: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**| The unique identifier of a user | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_users_get_all_users**
> ApiUsersGetAllUsers200Response api_users_get_all_users(limit=limit, offset=offset, order=order, search=search, group_id=group_id, item_id=item_id, response_format=response_format, order_by=order_by, role_id=role_id, is_active=is_active, confirmed=confirmed)



Returns all users

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_users
from core_users.models.api_users_get_all_users200_response import ApiUsersGetAllUsers200Response
from core_users.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/users
# See configuration.py for a list of all supported configuration parameters.
configuration = core_users.Configuration(
    host = "https://launchpad.imbio.com/api/v1/users"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_users.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_users.DefaultApi(api_client)
    limit = 50 # int | Limit the number of returned roles to indicated value (default 50).  (optional) (default to 50)
    offset = 0 # int | When set the query returns 'limit' number of roles but skip 'offset' number of roles at the beginning.  (optional) (default to 0)
    order = 'order_example' # str | Specify the order of the listing: desc, asc  (optional)
    search = 'search_example' # str | Specify a search string  (optional)
    group_id = [56] # List[int] | Filter by group(s)  (optional)
    item_id = [56] # List[int] | Filter resource by their primary id  (optional)
    response_format = 'response_format_example' # str | How detialed the response should be  (optional)
    order_by = 'order_by_example' # str | Order by the provided argument name.  (optional)
    role_id = [56] # List[int] | Restrict the list of users to only those who's belonging to a specific role  (optional)
    is_active = True # bool | Restrict the list of users to only those with provided statuses.  (optional)
    confirmed = True # bool | Restrict the list of users to only those with provided algorithm names.  (optional)

    try:
        api_response = api_instance.api_users_get_all_users(limit=limit, offset=offset, order=order, search=search, group_id=group_id, item_id=item_id, response_format=response_format, order_by=order_by, role_id=role_id, is_active=is_active, confirmed=confirmed)
        print("The response of DefaultApi->api_users_get_all_users:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_users_get_all_users: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Limit the number of returned roles to indicated value (default 50).  | [optional] [default to 50]
 **offset** | **int**| When set the query returns &#39;limit&#39; number of roles but skip &#39;offset&#39; number of roles at the beginning.  | [optional] [default to 0]
 **order** | **str**| Specify the order of the listing: desc, asc  | [optional] 
 **search** | **str**| Specify a search string  | [optional] 
 **group_id** | [**List[int]**](int.md)| Filter by group(s)  | [optional] 
 **item_id** | [**List[int]**](int.md)| Filter resource by their primary id  | [optional] 
 **response_format** | **str**| How detialed the response should be  | [optional] 
 **order_by** | **str**| Order by the provided argument name.  | [optional] 
 **role_id** | [**List[int]**](int.md)| Restrict the list of users to only those who&#39;s belonging to a specific role  | [optional] 
 **is_active** | **bool**| Restrict the list of users to only those with provided statuses.  | [optional] 
 **confirmed** | **bool**| Restrict the list of users to only those with provided algorithm names.  | [optional] 

### Return type

[**ApiUsersGetAllUsers200Response**](ApiUsersGetAllUsers200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get all resource |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_users_get_user**
> ApiUsersGetAllUsers200ResponseItemsInner api_users_get_user(user_id)



Returns details of a user

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_users
from core_users.models.api_users_get_all_users200_response_items_inner import ApiUsersGetAllUsers200ResponseItemsInner
from core_users.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/users
# See configuration.py for a list of all supported configuration parameters.
configuration = core_users.Configuration(
    host = "https://launchpad.imbio.com/api/v1/users"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_users.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_users.DefaultApi(api_client)
    user_id = 'user_id_example' # str | The unique identifier of a user

    try:
        api_response = api_instance.api_users_get_user(user_id)
        print("The response of DefaultApi->api_users_get_user:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_users_get_user: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**| The unique identifier of a user | 

### Return type

[**ApiUsersGetAllUsers200ResponseItemsInner**](ApiUsersGetAllUsers200ResponseItemsInner.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | details of a user |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_users_get_user_profile**
> ApiUsersGetUserProfile200Response api_users_get_user_profile()



Returns the personal profile of the logged-in user

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_users
from core_users.models.api_users_get_user_profile200_response import ApiUsersGetUserProfile200Response
from core_users.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/users
# See configuration.py for a list of all supported configuration parameters.
configuration = core_users.Configuration(
    host = "https://launchpad.imbio.com/api/v1/users"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_users.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_users.DefaultApi(api_client)

    try:
        api_response = api_instance.api_users_get_user_profile()
        print("The response of DefaultApi->api_users_get_user_profile:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_users_get_user_profile: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiUsersGetUserProfile200Response**](ApiUsersGetUserProfile200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | personal profile of the logged-in user |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_users_put_user**
> api_users_put_user(user_id, api_users_put_user_request)



Updates one user

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_users
from core_users.models.api_users_put_user_request import ApiUsersPutUserRequest
from core_users.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/users
# See configuration.py for a list of all supported configuration parameters.
configuration = core_users.Configuration(
    host = "https://launchpad.imbio.com/api/v1/users"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_users.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_users.DefaultApi(api_client)
    user_id = 'user_id_example' # str | The unique identifier of a user
    api_users_put_user_request = core_users.ApiUsersPutUserRequest() # ApiUsersPutUserRequest | The data for update

    try:
        api_instance.api_users_put_user(user_id, api_users_put_user_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_users_put_user: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**| The unique identifier of a user | 
 **api_users_put_user_request** | [**ApiUsersPutUserRequest**](ApiUsersPutUserRequest.md)| The data for update | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_users_put_user_profile**
> api_users_put_user_profile(api_users_put_user_profile_request)



Modify the personal profile of the logged-in user

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_users
from core_users.models.api_users_put_user_profile_request import ApiUsersPutUserProfileRequest
from core_users.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/users
# See configuration.py for a list of all supported configuration parameters.
configuration = core_users.Configuration(
    host = "https://launchpad.imbio.com/api/v1/users"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_users.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_users.DefaultApi(api_client)
    api_users_put_user_profile_request = core_users.ApiUsersPutUserProfileRequest() # ApiUsersPutUserProfileRequest | The data for profile update

    try:
        api_instance.api_users_put_user_profile(api_users_put_user_profile_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_users_put_user_profile: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_users_put_user_profile_request** | [**ApiUsersPutUserProfileRequest**](ApiUsersPutUserProfileRequest.md)| The data for profile update | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

