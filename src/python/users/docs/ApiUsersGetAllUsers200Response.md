# ApiUsersGetAllUsers200Response

The paginated response for get all users request

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**num_of_all** | **int** | How many users there are currently | [optional] 
**index** | **int** | Users returned from this index | [optional] 
**items** | [**List[ApiUsersGetAllUsers200ResponseItemsInner]**](ApiUsersGetAllUsers200ResponseItemsInner.md) | The list of returned users | [optional] 

## Example

```python
from core_users.models.api_users_get_all_users200_response import ApiUsersGetAllUsers200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiUsersGetAllUsers200Response from a JSON string
api_users_get_all_users200_response_instance = ApiUsersGetAllUsers200Response.from_json(json)
# print the JSON string representation of the object
print ApiUsersGetAllUsers200Response.to_json()

# convert the object into a dict
api_users_get_all_users200_response_dict = api_users_get_all_users200_response_instance.to_dict()
# create an instance of ApiUsersGetAllUsers200Response from a dict
api_users_get_all_users200_response_form_dict = api_users_get_all_users200_response.from_dict(api_users_get_all_users200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


