# ApiUsersPutUserRequest

User information to update

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name used by a user | [optional] 
**email** | **str** | User&#39;s email | [optional] 
**password** | **str** | User&#39;s new password | [optional] 
**force_password_change** | **bool** | Set this flag to notify the UI to force a password change | [optional] 
**is_active** | **bool** | True if user is active | [optional] 
**confirmed** | **bool** | True if confirmed | [optional] 
**groups** | **List[int]** | Groups that the user belongs to | [optional] 
**roles** | **List[int]** | Roles that the user belongs to | [optional] 

## Example

```python
from core_users.models.api_users_put_user_request import ApiUsersPutUserRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiUsersPutUserRequest from a JSON string
api_users_put_user_request_instance = ApiUsersPutUserRequest.from_json(json)
# print the JSON string representation of the object
print ApiUsersPutUserRequest.to_json()

# convert the object into a dict
api_users_put_user_request_dict = api_users_put_user_request_instance.to_dict()
# create an instance of ApiUsersPutUserRequest from a dict
api_users_put_user_request_form_dict = api_users_put_user_request.from_dict(api_users_put_user_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


