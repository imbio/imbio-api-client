# ApiUsersGetUserProfile200Response

User profile description

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notifications** | [**List[ApiUsersGetUserProfile200ResponseNotificationsInner]**](ApiUsersGetUserProfile200ResponseNotificationsInner.md) | The list of notifications | [optional] 
**language** | **str** | Preferred language by the user | [optional] 

## Example

```python
from core_users.models.api_users_get_user_profile200_response import ApiUsersGetUserProfile200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiUsersGetUserProfile200Response from a JSON string
api_users_get_user_profile200_response_instance = ApiUsersGetUserProfile200Response.from_json(json)
# print the JSON string representation of the object
print ApiUsersGetUserProfile200Response.to_json()

# convert the object into a dict
api_users_get_user_profile200_response_dict = api_users_get_user_profile200_response_instance.to_dict()
# create an instance of ApiUsersGetUserProfile200Response from a dict
api_users_get_user_profile200_response_form_dict = api_users_get_user_profile200_response.from_dict(api_users_get_user_profile200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


