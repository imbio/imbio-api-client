# ApiUsersGetAllUsers200ResponseItemsInnerGroupsInnerWorkflowsInner

Workflow object description

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from core_users.models.api_users_get_all_users200_response_items_inner_groups_inner_workflows_inner import ApiUsersGetAllUsers200ResponseItemsInnerGroupsInnerWorkflowsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiUsersGetAllUsers200ResponseItemsInnerGroupsInnerWorkflowsInner from a JSON string
api_users_get_all_users200_response_items_inner_groups_inner_workflows_inner_instance = ApiUsersGetAllUsers200ResponseItemsInnerGroupsInnerWorkflowsInner.from_json(json)
# print the JSON string representation of the object
print ApiUsersGetAllUsers200ResponseItemsInnerGroupsInnerWorkflowsInner.to_json()

# convert the object into a dict
api_users_get_all_users200_response_items_inner_groups_inner_workflows_inner_dict = api_users_get_all_users200_response_items_inner_groups_inner_workflows_inner_instance.to_dict()
# create an instance of ApiUsersGetAllUsers200ResponseItemsInnerGroupsInnerWorkflowsInner from a dict
api_users_get_all_users200_response_items_inner_groups_inner_workflows_inner_form_dict = api_users_get_all_users200_response_items_inner_groups_inner_workflows_inner.from_dict(api_users_get_all_users200_response_items_inner_groups_inner_workflows_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


