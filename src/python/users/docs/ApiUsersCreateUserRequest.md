# ApiUsersCreateUserRequest

Initial data for user

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** | The name used by a user | 
**email** | **str** | User&#39;s email | 
**password** | **str** | User&#39;s password | 
**groups** | **List[int]** | Groups that the user is going to belong to | 
**roles** | **List[int]** | list of roles that have the right of one item | 

## Example

```python
from core_users.models.api_users_create_user_request import ApiUsersCreateUserRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiUsersCreateUserRequest from a JSON string
api_users_create_user_request_instance = ApiUsersCreateUserRequest.from_json(json)
# print the JSON string representation of the object
print ApiUsersCreateUserRequest.to_json()

# convert the object into a dict
api_users_create_user_request_dict = api_users_create_user_request_instance.to_dict()
# create an instance of ApiUsersCreateUserRequest from a dict
api_users_create_user_request_form_dict = api_users_create_user_request.from_dict(api_users_create_user_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


