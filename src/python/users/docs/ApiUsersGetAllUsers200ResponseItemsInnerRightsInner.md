# ApiUsersGetAllUsers200ResponseItemsInnerRightsInner

Permissions info

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Rigth id | 
**name** | **str** | Name of the Right | 
**description** | **str** | Description of the Right | [optional] 

## Example

```python
from core_users.models.api_users_get_all_users200_response_items_inner_rights_inner import ApiUsersGetAllUsers200ResponseItemsInnerRightsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiUsersGetAllUsers200ResponseItemsInnerRightsInner from a JSON string
api_users_get_all_users200_response_items_inner_rights_inner_instance = ApiUsersGetAllUsers200ResponseItemsInnerRightsInner.from_json(json)
# print the JSON string representation of the object
print ApiUsersGetAllUsers200ResponseItemsInnerRightsInner.to_json()

# convert the object into a dict
api_users_get_all_users200_response_items_inner_rights_inner_dict = api_users_get_all_users200_response_items_inner_rights_inner_instance.to_dict()
# create an instance of ApiUsersGetAllUsers200ResponseItemsInnerRightsInner from a dict
api_users_get_all_users200_response_items_inner_rights_inner_form_dict = api_users_get_all_users200_response_items_inner_rights_inner.from_dict(api_users_get_all_users200_response_items_inner_rights_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


