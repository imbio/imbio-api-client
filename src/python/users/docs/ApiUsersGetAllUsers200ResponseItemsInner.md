# ApiUsersGetAllUsers200ResponseItemsInner

User's information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The id of the user | [optional] 
**name** | **str** | The name used by a user | 
**email** | **str** | User&#39;s email | 
**is_active** | **bool** | True if user is active | 
**confirmed** | **bool** | True if confirmed | 
**terms_and_conditions_accepted** | **bool** | accept terms and conditions | [optional] 
**groups** | [**List[ApiUsersGetAllUsers200ResponseItemsInnerGroupsInner]**](ApiUsersGetAllUsers200ResponseItemsInnerGroupsInner.md) | List of groups that the user belongs to | [optional] 
**roles** | [**List[ApiUsersGetAllUsers200ResponseItemsInnerRolesInner]**](ApiUsersGetAllUsers200ResponseItemsInnerRolesInner.md) | List of roles that the user belongs to | [optional] 
**rights** | [**List[ApiUsersGetAllUsers200ResponseItemsInnerRightsInner]**](ApiUsersGetAllUsers200ResponseItemsInnerRightsInner.md) | List of rights that the user has | [optional] 

## Example

```python
from core_users.models.api_users_get_all_users200_response_items_inner import ApiUsersGetAllUsers200ResponseItemsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiUsersGetAllUsers200ResponseItemsInner from a JSON string
api_users_get_all_users200_response_items_inner_instance = ApiUsersGetAllUsers200ResponseItemsInner.from_json(json)
# print the JSON string representation of the object
print ApiUsersGetAllUsers200ResponseItemsInner.to_json()

# convert the object into a dict
api_users_get_all_users200_response_items_inner_dict = api_users_get_all_users200_response_items_inner_instance.to_dict()
# create an instance of ApiUsersGetAllUsers200ResponseItemsInner from a dict
api_users_get_all_users200_response_items_inner_form_dict = api_users_get_all_users200_response_items_inner.from_dict(api_users_get_all_users200_response_items_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


