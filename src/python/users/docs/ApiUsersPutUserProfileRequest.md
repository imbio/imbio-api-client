# ApiUsersPutUserProfileRequest

User profile description

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notifications** | **List[int]** | The list of notification ids | [optional] 
**language** | **str** | Preferred language by the user | [optional] 

## Example

```python
from core_users.models.api_users_put_user_profile_request import ApiUsersPutUserProfileRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiUsersPutUserProfileRequest from a JSON string
api_users_put_user_profile_request_instance = ApiUsersPutUserProfileRequest.from_json(json)
# print the JSON string representation of the object
print ApiUsersPutUserProfileRequest.to_json()

# convert the object into a dict
api_users_put_user_profile_request_dict = api_users_put_user_profile_request_instance.to_dict()
# create an instance of ApiUsersPutUserProfileRequest from a dict
api_users_put_user_profile_request_form_dict = api_users_put_user_profile_request.from_dict(api_users_put_user_profile_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


