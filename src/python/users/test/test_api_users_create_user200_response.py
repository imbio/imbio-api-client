# coding: utf-8

"""
    IMBIO core api for users

    Methods for managing users 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_users.models.api_users_create_user200_response import ApiUsersCreateUser200Response

class TestApiUsersCreateUser200Response(unittest.TestCase):
    """ApiUsersCreateUser200Response unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiUsersCreateUser200Response:
        """Test ApiUsersCreateUser200Response
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiUsersCreateUser200Response`
        """
        model = ApiUsersCreateUser200Response()
        if include_optional:
            return ApiUsersCreateUser200Response(
                id = 56
            )
        else:
            return ApiUsersCreateUser200Response(
        )
        """

    def testApiUsersCreateUser200Response(self):
        """Test ApiUsersCreateUser200Response"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
