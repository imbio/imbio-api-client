# coding: utf-8

"""
    IMBIO core api for notifications

    Methods for notifications 

    The version of the OpenAPI document: v1.0.0
    Contact: zoltanribi@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_notifications.models.api_notifications_send_notifications_request_event_job_assets_inner import ApiNotificationsSendNotificationsRequestEventJobAssetsInner

class TestApiNotificationsSendNotificationsRequestEventJobAssetsInner(unittest.TestCase):
    """ApiNotificationsSendNotificationsRequestEventJobAssetsInner unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiNotificationsSendNotificationsRequestEventJobAssetsInner:
        """Test ApiNotificationsSendNotificationsRequestEventJobAssetsInner
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiNotificationsSendNotificationsRequestEventJobAssetsInner`
        """
        model = ApiNotificationsSendNotificationsRequestEventJobAssetsInner()
        if include_optional:
            return ApiNotificationsSendNotificationsRequestEventJobAssetsInner(
                id = 56,
                name = '',
                asset_type = 'input',
                uploaded_at = '',
                owner_id = 56,
                group_id = 56,
                study_id = 56,
                study_date = '',
                patient_name = '',
                accession_number = '',
                deidentified = True,
                has_archive = True,
                has_csv_report = True,
                has_pdf_report = True,
                cleaned_up = True,
                series_options_log = None,
                deid_values = None
            )
        else:
            return ApiNotificationsSendNotificationsRequestEventJobAssetsInner(
        )
        """

    def testApiNotificationsSendNotificationsRequestEventJobAssetsInner(self):
        """Test ApiNotificationsSendNotificationsRequestEventJobAssetsInner"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
