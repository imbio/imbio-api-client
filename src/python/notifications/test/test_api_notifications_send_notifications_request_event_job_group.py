# coding: utf-8

"""
    IMBIO core api for notifications

    Methods for notifications 

    The version of the OpenAPI document: v1.0.0
    Contact: zoltanribi@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_notifications.models.api_notifications_send_notifications_request_event_job_group import ApiNotificationsSendNotificationsRequestEventJobGroup

class TestApiNotificationsSendNotificationsRequestEventJobGroup(unittest.TestCase):
    """ApiNotificationsSendNotificationsRequestEventJobGroup unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiNotificationsSendNotificationsRequestEventJobGroup:
        """Test ApiNotificationsSendNotificationsRequestEventJobGroup
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiNotificationsSendNotificationsRequestEventJobGroup`
        """
        model = ApiNotificationsSendNotificationsRequestEventJobGroup()
        if include_optional:
            return ApiNotificationsSendNotificationsRequestEventJobGroup(
                id = 0,
                name = '',
                description = '',
                page_logo_url = '',
                report_logo_url = '',
                review_workflow_enabled = True,
                qa_workflow_enabled = True,
                overread_workflow_enabled = True,
                overread_request_enabled = True,
                seg_editor_enabled = True,
                deidentify = '',
                storage_time = 56,
                research = True,
                region = '',
                allow_notification_suppress = True,
                notifications = [
                    56
                    ],
                workflows = [
                    core_notifications.models.api_notifications_send_notifications_request_event_job_group_workflows_inner.api_notifications_send_notifications_request_event_job_group_workflows_inner(
                        id = 56, 
                        name = '', )
                    ],
                en_upload_any_series = True,
                disable_series_preprocessing = True
            )
        else:
            return ApiNotificationsSendNotificationsRequestEventJobGroup(
        )
        """

    def testApiNotificationsSendNotificationsRequestEventJobGroup(self):
        """Test ApiNotificationsSendNotificationsRequestEventJobGroup"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
