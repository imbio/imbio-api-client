# ApiNotificationsSendNotificationsRequestEventJobStudy

Study properties parsed from the DICOM study that will be used by the UI/bridge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The id of the study  | [optional] 
**uid** | **str** | The UID of the study  | [optional] 
**study_hash** | **str** | Hash to identify the patient  | [optional] 
**study_date** | **str** | The time when the study was created  | [optional] 
**patient_id** | **str** | The patient&#39;s ID from orthanc  | [optional] 
**patient_name** | **str** | The patient&#39;s name  | [optional] 
**patient_birthdate** | **str** | The patient&#39;s birthdate  | [optional] 
**patient_age** | **int** | The patient&#39;s age in year  | [optional] 
**patient_sex** | **str** | The gender of the patient  | [optional] 
**accession_number** | **str** | The accession number  | [optional] 

## Example

```python
from core_notifications.models.api_notifications_send_notifications_request_event_job_study import ApiNotificationsSendNotificationsRequestEventJobStudy

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsSendNotificationsRequestEventJobStudy from a JSON string
api_notifications_send_notifications_request_event_job_study_instance = ApiNotificationsSendNotificationsRequestEventJobStudy.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsSendNotificationsRequestEventJobStudy.to_json()

# convert the object into a dict
api_notifications_send_notifications_request_event_job_study_dict = api_notifications_send_notifications_request_event_job_study_instance.to_dict()
# create an instance of ApiNotificationsSendNotificationsRequestEventJobStudy from a dict
api_notifications_send_notifications_request_event_job_study_form_dict = api_notifications_send_notifications_request_event_job_study.from_dict(api_notifications_send_notifications_request_event_job_study_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


