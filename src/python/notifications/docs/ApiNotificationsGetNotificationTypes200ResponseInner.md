# ApiNotificationsGetNotificationTypes200ResponseInner

All notification related information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The notification id  | [optional] 
**type** | **str** | The notification type  | [optional] 
**name** | **str** | The notification name  | [optional] 
**description** | **str** | The notification description  | [optional] 
**notification_group** | **str** | The notification group  | [optional] 
**template** | **str** | The notification template  | [optional] 

## Example

```python
from core_notifications.models.api_notifications_get_notification_types200_response_inner import ApiNotificationsGetNotificationTypes200ResponseInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsGetNotificationTypes200ResponseInner from a JSON string
api_notifications_get_notification_types200_response_inner_instance = ApiNotificationsGetNotificationTypes200ResponseInner.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsGetNotificationTypes200ResponseInner.to_json()

# convert the object into a dict
api_notifications_get_notification_types200_response_inner_dict = api_notifications_get_notification_types200_response_inner_instance.to_dict()
# create an instance of ApiNotificationsGetNotificationTypes200ResponseInner from a dict
api_notifications_get_notification_types200_response_inner_form_dict = api_notifications_get_notification_types200_response_inner.from_dict(api_notifications_get_notification_types200_response_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


