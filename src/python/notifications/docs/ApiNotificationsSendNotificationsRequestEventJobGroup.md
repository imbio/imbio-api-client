# ApiNotificationsSendNotificationsRequestEventJobGroup

Nested group schema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The id of the group  | [optional] 
**name** | **str** | The name of the group  | [optional] 
**description** | **str** | The description of the group  | [optional] 
**page_logo_url** | **str** | The url of the group logo to show on the site  | [optional] 
**report_logo_url** | **str** | The url of the report logo to use in the reports  | [optional] 
**review_workflow_enabled** | **bool** | Shows if the review workflow is enabled | [optional] 
**qa_workflow_enabled** | **bool** | Shows if the qa review workflow is enabled | [optional] 
**overread_workflow_enabled** | **bool** | Shows if the overread workflow is enabled | [optional] 
**overread_request_enabled** | **bool** | Flag to decide whether overread request functionality is enabled for the group | [optional] 
**seg_editor_enabled** | **bool** | Shows if the segmentation editor is enabled | [optional] 
**deidentify** | **str** | Flag for deidentify data before uploading | [optional] 
**storage_time** | **int** | How many days to store assets | [optional] 
**research** | **bool** | shows if this the group runs algorithms for research | [optional] 
**region** | **str** | shows that the group is in what region | [optional] 
**allow_notification_suppress** | **bool** | Disable notification filtering on user level. | [optional] 
**notifications** | **List[int]** | List of allowed notifications | [optional] 
**workflows** | [**List[ApiNotificationsSendNotificationsRequestEventJobGroupWorkflowsInner]**](ApiNotificationsSendNotificationsRequestEventJobGroupWorkflowsInner.md) | Some info of the algorithms belonging to group | [optional] 
**en_upload_any_series** | **bool** | Lets the group to upload any series including invalide series | [optional] 
**disable_series_preprocessing** | **bool** | Disable series preprocessing | [optional] 

## Example

```python
from core_notifications.models.api_notifications_send_notifications_request_event_job_group import ApiNotificationsSendNotificationsRequestEventJobGroup

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsSendNotificationsRequestEventJobGroup from a JSON string
api_notifications_send_notifications_request_event_job_group_instance = ApiNotificationsSendNotificationsRequestEventJobGroup.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsSendNotificationsRequestEventJobGroup.to_json()

# convert the object into a dict
api_notifications_send_notifications_request_event_job_group_dict = api_notifications_send_notifications_request_event_job_group_instance.to_dict()
# create an instance of ApiNotificationsSendNotificationsRequestEventJobGroup from a dict
api_notifications_send_notifications_request_event_job_group_form_dict = api_notifications_send_notifications_request_event_job_group.from_dict(api_notifications_send_notifications_request_event_job_group_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


