# ApiNotificationsSendNotificationsRequestEventJobCommentsInner

description of a comment in response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the comment | [optional] 
**created_at** | **str** | timestamp when the comment was created | [optional] 
**modified_at** | **str** | timestamp when the comment was modified | [optional] 
**value** | **str** | content of the comment | [optional] 
**owner_id** | **int** | The id of the owner  | [optional] 
**modified_by** | [**ApiNotificationsSendNotificationsRequestEventJobOwner**](ApiNotificationsSendNotificationsRequestEventJobOwner.md) |  | [optional] 
**allowed_actions** | [**ApiNotificationsSendNotificationsRequestEventJobCommentsInnerAllowedActions**](ApiNotificationsSendNotificationsRequestEventJobCommentsInnerAllowedActions.md) |  | [optional] 

## Example

```python
from core_notifications.models.api_notifications_send_notifications_request_event_job_comments_inner import ApiNotificationsSendNotificationsRequestEventJobCommentsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsSendNotificationsRequestEventJobCommentsInner from a JSON string
api_notifications_send_notifications_request_event_job_comments_inner_instance = ApiNotificationsSendNotificationsRequestEventJobCommentsInner.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsSendNotificationsRequestEventJobCommentsInner.to_json()

# convert the object into a dict
api_notifications_send_notifications_request_event_job_comments_inner_dict = api_notifications_send_notifications_request_event_job_comments_inner_instance.to_dict()
# create an instance of ApiNotificationsSendNotificationsRequestEventJobCommentsInner from a dict
api_notifications_send_notifications_request_event_job_comments_inner_form_dict = api_notifications_send_notifications_request_event_job_comments_inner.from_dict(api_notifications_send_notifications_request_event_job_comments_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


