# ApiNotificationsSendNotificationsRequestEventJobHistoryInner


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of a job | [optional] 
**name** | **str** | name of a job | [optional] 
**workflows** | [**List[ApiNotificationsSendNotificationsRequestEventJobGroupWorkflowsInner]**](ApiNotificationsSendNotificationsRequestEventJobGroupWorkflowsInner.md) |  | [optional] 
**upload_source** | **str** | Source where the job was created from (eg. api|ui|bridge|etc.) | [optional] 
**algorithm** | [**ApiNotificationsSendNotificationsRequestEventJobHistoryInnerAlgorithm**](ApiNotificationsSendNotificationsRequestEventJobHistoryInnerAlgorithm.md) |  | [optional] 
**status** | **str** | status of a job | [optional] 
**created_at** | **str** | The time when the job was created | [optional] 

## Example

```python
from core_notifications.models.api_notifications_send_notifications_request_event_job_history_inner import ApiNotificationsSendNotificationsRequestEventJobHistoryInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsSendNotificationsRequestEventJobHistoryInner from a JSON string
api_notifications_send_notifications_request_event_job_history_inner_instance = ApiNotificationsSendNotificationsRequestEventJobHistoryInner.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsSendNotificationsRequestEventJobHistoryInner.to_json()

# convert the object into a dict
api_notifications_send_notifications_request_event_job_history_inner_dict = api_notifications_send_notifications_request_event_job_history_inner_instance.to_dict()
# create an instance of ApiNotificationsSendNotificationsRequestEventJobHistoryInner from a dict
api_notifications_send_notifications_request_event_job_history_inner_form_dict = api_notifications_send_notifications_request_event_job_history_inner.from_dict(api_notifications_send_notifications_request_event_job_history_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


