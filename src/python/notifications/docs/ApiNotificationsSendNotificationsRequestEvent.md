# ApiNotificationsSendNotificationsRequestEvent


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_type** | **str** | destination email | 
**job** | [**ApiNotificationsSendNotificationsRequestEventJob**](ApiNotificationsSendNotificationsRequestEventJob.md) |  | [optional] 
**jobs** | [**List[ApiNotificationsSendNotificationsRequestEventJob]**](ApiNotificationsSendNotificationsRequestEventJob.md) | List of jobs | [optional] 
**group** | **str** | Name of the group | [optional] 

## Example

```python
from core_notifications.models.api_notifications_send_notifications_request_event import ApiNotificationsSendNotificationsRequestEvent

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsSendNotificationsRequestEvent from a JSON string
api_notifications_send_notifications_request_event_instance = ApiNotificationsSendNotificationsRequestEvent.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsSendNotificationsRequestEvent.to_json()

# convert the object into a dict
api_notifications_send_notifications_request_event_dict = api_notifications_send_notifications_request_event_instance.to_dict()
# create an instance of ApiNotificationsSendNotificationsRequestEvent from a dict
api_notifications_send_notifications_request_event_form_dict = api_notifications_send_notifications_request_event.from_dict(api_notifications_send_notifications_request_event_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


