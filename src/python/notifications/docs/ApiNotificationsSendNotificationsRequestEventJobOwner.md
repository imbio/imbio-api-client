# ApiNotificationsSendNotificationsRequestEventJobOwner

Some info of a user

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of a user | [optional] 
**name** | **str** | name of a user or part of email | [optional] 
**email** | **str** | email of a user | [optional] 

## Example

```python
from core_notifications.models.api_notifications_send_notifications_request_event_job_owner import ApiNotificationsSendNotificationsRequestEventJobOwner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsSendNotificationsRequestEventJobOwner from a JSON string
api_notifications_send_notifications_request_event_job_owner_instance = ApiNotificationsSendNotificationsRequestEventJobOwner.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsSendNotificationsRequestEventJobOwner.to_json()

# convert the object into a dict
api_notifications_send_notifications_request_event_job_owner_dict = api_notifications_send_notifications_request_event_job_owner_instance.to_dict()
# create an instance of ApiNotificationsSendNotificationsRequestEventJobOwner from a dict
api_notifications_send_notifications_request_event_job_owner_form_dict = api_notifications_send_notifications_request_event_job_owner.from_dict(api_notifications_send_notifications_request_event_job_owner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


