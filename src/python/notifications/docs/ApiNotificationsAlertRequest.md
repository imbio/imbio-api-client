# ApiNotificationsAlertRequest

cause of the alert

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**qa_overdue** | **bool** | Send emails about the QA review is overdue jobs  | [optional] 

## Example

```python
from core_notifications.models.api_notifications_alert_request import ApiNotificationsAlertRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsAlertRequest from a JSON string
api_notifications_alert_request_instance = ApiNotificationsAlertRequest.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsAlertRequest.to_json()

# convert the object into a dict
api_notifications_alert_request_dict = api_notifications_alert_request_instance.to_dict()
# create an instance of ApiNotificationsAlertRequest from a dict
api_notifications_alert_request_form_dict = api_notifications_alert_request.from_dict(api_notifications_alert_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


