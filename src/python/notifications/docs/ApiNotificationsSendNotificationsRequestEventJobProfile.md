# ApiNotificationsSendNotificationsRequestEventJobProfile

The description of a profile.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Profile Id  | [optional] 
**name** | **str** | The name of the profile  | [optional] 
**owner_id** | **int** | The id of the owner  | [optional] 
**active** | **bool** | Whether the profile is in use The orthanc_host of active profiles will be used to poll for studies  | [optional] 
**logo** | **str** | The filename of the logo for this profile  | [optional] 
**orthanc_enabled** | **bool** | Shows whether polling / exporting to orthanc is enabled for this profile  | [optional] 
**orthanc_host** | **str** | The orthanc server to poll for new studies  | [optional] 
**orthanc_http_port** | **int** | The HTTP port of the orthanc server that is polled for new studies  | [optional] 
**orthanc_dicom_port** | **int** | The dicom port of the orthanc server that is polled for new studies  | [optional] 
**orthanc_aet** | **str** | The AET of the profile on the orthanc server that is polled for new studies  | [optional] 
**orthanc_store_addr** | **str** | The IP of the orthanc server where the results will be uploaded to (destination orthanc server)  | [optional] 
**orthanc_store_port** | **int** | The port of the destination orthanc server  | [optional] 
**orthanc_store_aet** | **str** | The AET at the destination orthanc server  | [optional] 
**orthanc_query_modality_name** | **str** | The modality name for the PACS server to query  | [optional] 
**algorithm_id** | **int** | The id of the algorithm that should be used to process the incoming studies  | [optional] 
**custom_algorithm_invocation_template** | **str** | A custom invocation template; if not filled the default invocation template of the algorithm will be used  | [optional] 
**custom_validator_orthanc** | **str** | Template used to verify whether incoming studies can be accepted by the algorithms  | [optional] 
**seg_editor_enabled** | **bool** | Shows whether the segmentation editor is enabled for this profile  | [optional] 
**editor_target** | **str** | The segmentation editor in use for this job | [optional] 
**output_filter_rule** | **str** | A filter rule that filters results that should be stored on the destination orthanc server  | [optional] 

## Example

```python
from core_notifications.models.api_notifications_send_notifications_request_event_job_profile import ApiNotificationsSendNotificationsRequestEventJobProfile

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsSendNotificationsRequestEventJobProfile from a JSON string
api_notifications_send_notifications_request_event_job_profile_instance = ApiNotificationsSendNotificationsRequestEventJobProfile.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsSendNotificationsRequestEventJobProfile.to_json()

# convert the object into a dict
api_notifications_send_notifications_request_event_job_profile_dict = api_notifications_send_notifications_request_event_job_profile_instance.to_dict()
# create an instance of ApiNotificationsSendNotificationsRequestEventJobProfile from a dict
api_notifications_send_notifications_request_event_job_profile_form_dict = api_notifications_send_notifications_request_event_job_profile.from_dict(api_notifications_send_notifications_request_event_job_profile_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


