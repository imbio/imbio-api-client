# ApiNotificationsSendNotificationsRequestEventJobResultsInner

description of a result response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the result | [optional] 
**created_at** | **str** | timestamp when the result was created | [optional] 
**modified_at** | **str** | timestamp when the result was modified | [optional] 
**name** | **str** | name of the result | [optional] 
**value** | **str** | content of the result | [optional] 
**type** | **str** | type of the result value | [optional] 
**owner_id** | **int** | The id of the owner  | [optional] 
**modified_by** | [**ApiNotificationsSendNotificationsRequestEventJobOwner**](ApiNotificationsSendNotificationsRequestEventJobOwner.md) |  | [optional] 

## Example

```python
from core_notifications.models.api_notifications_send_notifications_request_event_job_results_inner import ApiNotificationsSendNotificationsRequestEventJobResultsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsSendNotificationsRequestEventJobResultsInner from a JSON string
api_notifications_send_notifications_request_event_job_results_inner_instance = ApiNotificationsSendNotificationsRequestEventJobResultsInner.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsSendNotificationsRequestEventJobResultsInner.to_json()

# convert the object into a dict
api_notifications_send_notifications_request_event_job_results_inner_dict = api_notifications_send_notifications_request_event_job_results_inner_instance.to_dict()
# create an instance of ApiNotificationsSendNotificationsRequestEventJobResultsInner from a dict
api_notifications_send_notifications_request_event_job_results_inner_form_dict = api_notifications_send_notifications_request_event_job_results_inner.from_dict(api_notifications_send_notifications_request_event_job_results_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


