# ApiNotificationsSendNotificationsRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dest** | [**List[ApiNotificationsSendNotificationsRequestDestInner]**](ApiNotificationsSendNotificationsRequestDestInner.md) |  | 
**event** | [**ApiNotificationsSendNotificationsRequestEvent**](ApiNotificationsSendNotificationsRequestEvent.md) |  | 

## Example

```python
from core_notifications.models.api_notifications_send_notifications_request import ApiNotificationsSendNotificationsRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsSendNotificationsRequest from a JSON string
api_notifications_send_notifications_request_instance = ApiNotificationsSendNotificationsRequest.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsSendNotificationsRequest.to_json()

# convert the object into a dict
api_notifications_send_notifications_request_dict = api_notifications_send_notifications_request_instance.to_dict()
# create an instance of ApiNotificationsSendNotificationsRequest from a dict
api_notifications_send_notifications_request_form_dict = api_notifications_send_notifications_request.from_dict(api_notifications_send_notifications_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


