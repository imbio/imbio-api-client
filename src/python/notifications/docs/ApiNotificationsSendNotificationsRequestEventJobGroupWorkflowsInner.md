# ApiNotificationsSendNotificationsRequestEventJobGroupWorkflowsInner

Workflow object description

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from core_notifications.models.api_notifications_send_notifications_request_event_job_group_workflows_inner import ApiNotificationsSendNotificationsRequestEventJobGroupWorkflowsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsSendNotificationsRequestEventJobGroupWorkflowsInner from a JSON string
api_notifications_send_notifications_request_event_job_group_workflows_inner_instance = ApiNotificationsSendNotificationsRequestEventJobGroupWorkflowsInner.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsSendNotificationsRequestEventJobGroupWorkflowsInner.to_json()

# convert the object into a dict
api_notifications_send_notifications_request_event_job_group_workflows_inner_dict = api_notifications_send_notifications_request_event_job_group_workflows_inner_instance.to_dict()
# create an instance of ApiNotificationsSendNotificationsRequestEventJobGroupWorkflowsInner from a dict
api_notifications_send_notifications_request_event_job_group_workflows_inner_form_dict = api_notifications_send_notifications_request_event_job_group_workflows_inner.from_dict(api_notifications_send_notifications_request_event_job_group_workflows_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


