# core_notifications.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/notifications*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_notifications_alert**](DefaultApi.md#api_notifications_alert) | **POST** /alert | 
[**api_notifications_get_notification_types**](DefaultApi.md#api_notifications_get_notification_types) | **GET** /notification-types | 
[**api_notifications_get_template**](DefaultApi.md#api_notifications_get_template) | **GET** /templates/{language}/{filename} | 
[**api_notifications_get_templates**](DefaultApi.md#api_notifications_get_templates) | **GET** /templates | 
[**api_notifications_put_template**](DefaultApi.md#api_notifications_put_template) | **PUT** /templates | 
[**api_notifications_send_notifications**](DefaultApi.md#api_notifications_send_notifications) | **POST** / | 


# **api_notifications_alert**
> api_notifications_alert(api_notifications_alert_request)



Send notifications

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_notifications
from core_notifications.models.api_notifications_alert_request import ApiNotificationsAlertRequest
from core_notifications.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/notifications
# See configuration.py for a list of all supported configuration parameters.
configuration = core_notifications.Configuration(
    host = "https://launchpad.imbio.com/api/v1/notifications"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_notifications.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_notifications.DefaultApi(api_client)
    api_notifications_alert_request = core_notifications.ApiNotificationsAlertRequest() # ApiNotificationsAlertRequest | why the alert is triggered

    try:
        api_instance.api_notifications_alert(api_notifications_alert_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_notifications_alert: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_notifications_alert_request** | [**ApiNotificationsAlertRequest**](ApiNotificationsAlertRequest.md)| why the alert is triggered | 

### Return type

void (empty response body)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_notifications_get_notification_types**
> List[ApiNotificationsGetNotificationTypes200ResponseInner] api_notifications_get_notification_types(template=template)



Returns the notification types

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_notifications
from core_notifications.models.api_notifications_get_notification_types200_response_inner import ApiNotificationsGetNotificationTypes200ResponseInner
from core_notifications.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/notifications
# See configuration.py for a list of all supported configuration parameters.
configuration = core_notifications.Configuration(
    host = "https://launchpad.imbio.com/api/v1/notifications"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_notifications.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_notifications.DefaultApi(api_client)
    template = 'template_example' # str |  (optional)

    try:
        api_response = api_instance.api_notifications_get_notification_types(template=template)
        print("The response of DefaultApi->api_notifications_get_notification_types:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_notifications_get_notification_types: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **template** | **str**|  | [optional] 

### Return type

[**List[ApiNotificationsGetNotificationTypes200ResponseInner]**](ApiNotificationsGetNotificationTypes200ResponseInner.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the notification types. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_notifications_get_template**
> str api_notifications_get_template(language, filename)



Returns a template's content

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_notifications
from core_notifications.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/notifications
# See configuration.py for a list of all supported configuration parameters.
configuration = core_notifications.Configuration(
    host = "https://launchpad.imbio.com/api/v1/notifications"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_notifications.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_notifications.DefaultApi(api_client)
    language = 'language_example' # str | What is the language of the template (en|de|etc.)
    filename = 'filename_example' # str | What is the name of the template

    try:
        api_response = api_instance.api_notifications_get_template(language, filename)
        print("The response of DefaultApi->api_notifications_get_template:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_notifications_get_template: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language** | **str**| What is the language of the template (en|de|etc.) | 
 **filename** | **str**| What is the name of the template | 

### Return type

**str**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the content of the template |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_notifications_get_templates**
> List[str] api_notifications_get_templates()



Returns all template file names and which notification is using that template

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_notifications
from core_notifications.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/notifications
# See configuration.py for a list of all supported configuration parameters.
configuration = core_notifications.Configuration(
    host = "https://launchpad.imbio.com/api/v1/notifications"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_notifications.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_notifications.DefaultApi(api_client)

    try:
        api_response = api_instance.api_notifications_get_templates()
        print("The response of DefaultApi->api_notifications_get_templates:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_notifications_get_templates: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

**List[str]**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns all template file names and which notification is using that template |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_notifications_put_template**
> api_notifications_put_template(name, language, notifications, template)



Save notification email template

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_notifications
from core_notifications.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/notifications
# See configuration.py for a list of all supported configuration parameters.
configuration = core_notifications.Configuration(
    host = "https://launchpad.imbio.com/api/v1/notifications"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_notifications.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_notifications.DefaultApi(api_client)
    name = 'name_example' # str | Name to save the template as
    language = 'language_example' # str | Language of the template (en|de|etc.)
    notifications = [56] # List[int] | Notification ids to use the template with
    template = None # bytearray | Template and meta to save

    try:
        api_instance.api_notifications_put_template(name, language, notifications, template)
    except Exception as e:
        print("Exception when calling DefaultApi->api_notifications_put_template: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| Name to save the template as | 
 **language** | **str**| Language of the template (en|de|etc.) | 
 **notifications** | [**List[int]**](int.md)| Notification ids to use the template with | 
 **template** | **bytearray**| Template and meta to save | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_notifications_send_notifications**
> api_notifications_send_notifications(api_notifications_send_notifications_request)



Send notifications

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_notifications
from core_notifications.models.api_notifications_send_notifications_request import ApiNotificationsSendNotificationsRequest
from core_notifications.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/notifications
# See configuration.py for a list of all supported configuration parameters.
configuration = core_notifications.Configuration(
    host = "https://launchpad.imbio.com/api/v1/notifications"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_notifications.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_notifications.DefaultApi(api_client)
    api_notifications_send_notifications_request = core_notifications.ApiNotificationsSendNotificationsRequest() # ApiNotificationsSendNotificationsRequest | Notifications to send

    try:
        api_instance.api_notifications_send_notifications(api_notifications_send_notifications_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_notifications_send_notifications: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_notifications_send_notifications_request** | [**ApiNotificationsSendNotificationsRequest**](ApiNotificationsSendNotificationsRequest.md)| Notifications to send | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

