# ApiNotificationsSendNotificationsRequestDestInner

Email destination information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **str** | destination email | 
**user_name** | **str** | name of a addressed user | [optional] 
**language** | **str** | language of the email | [optional] 

## Example

```python
from core_notifications.models.api_notifications_send_notifications_request_dest_inner import ApiNotificationsSendNotificationsRequestDestInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsSendNotificationsRequestDestInner from a JSON string
api_notifications_send_notifications_request_dest_inner_instance = ApiNotificationsSendNotificationsRequestDestInner.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsSendNotificationsRequestDestInner.to_json()

# convert the object into a dict
api_notifications_send_notifications_request_dest_inner_dict = api_notifications_send_notifications_request_dest_inner_instance.to_dict()
# create an instance of ApiNotificationsSendNotificationsRequestDestInner from a dict
api_notifications_send_notifications_request_dest_inner_form_dict = api_notifications_send_notifications_request_dest_inner.from_dict(api_notifications_send_notifications_request_dest_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


