# ApiNotificationsSendNotificationsRequestEventJobCommentsInnerAllowedActions


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**edit** | **bool** | shows if user can edit the commment | [optional] 
**delete** | **bool** | shows if user can delete the commment | [optional] 

## Example

```python
from core_notifications.models.api_notifications_send_notifications_request_event_job_comments_inner_allowed_actions import ApiNotificationsSendNotificationsRequestEventJobCommentsInnerAllowedActions

# TODO update the JSON string below
json = "{}"
# create an instance of ApiNotificationsSendNotificationsRequestEventJobCommentsInnerAllowedActions from a JSON string
api_notifications_send_notifications_request_event_job_comments_inner_allowed_actions_instance = ApiNotificationsSendNotificationsRequestEventJobCommentsInnerAllowedActions.from_json(json)
# print the JSON string representation of the object
print ApiNotificationsSendNotificationsRequestEventJobCommentsInnerAllowedActions.to_json()

# convert the object into a dict
api_notifications_send_notifications_request_event_job_comments_inner_allowed_actions_dict = api_notifications_send_notifications_request_event_job_comments_inner_allowed_actions_instance.to_dict()
# create an instance of ApiNotificationsSendNotificationsRequestEventJobCommentsInnerAllowedActions from a dict
api_notifications_send_notifications_request_event_job_comments_inner_allowed_actions_form_dict = api_notifications_send_notifications_request_event_job_comments_inner_allowed_actions.from_dict(api_notifications_send_notifications_request_event_job_comments_inner_allowed_actions_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


