# ApiSkinsPutFavicon200Response

Object containing an url

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **str** |  | [optional] 

## Example

```python
from core_skins.models.api_skins_put_favicon200_response import ApiSkinsPutFavicon200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiSkinsPutFavicon200Response from a JSON string
api_skins_put_favicon200_response_instance = ApiSkinsPutFavicon200Response.from_json(json)
# print the JSON string representation of the object
print ApiSkinsPutFavicon200Response.to_json()

# convert the object into a dict
api_skins_put_favicon200_response_dict = api_skins_put_favicon200_response_instance.to_dict()
# create an instance of ApiSkinsPutFavicon200Response from a dict
api_skins_put_favicon200_response_form_dict = api_skins_put_favicon200_response.from_dict(api_skins_put_favicon200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


