# core_skins.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/skins*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_skins_delete_default_logo**](DefaultApi.md#api_skins_delete_default_logo) | **DELETE** /logo/{logo_type} | 
[**api_skins_delete_default_theme**](DefaultApi.md#api_skins_delete_default_theme) | **DELETE** /theme | 
[**api_skins_delete_favicon**](DefaultApi.md#api_skins_delete_favicon) | **DELETE** /favicon/ | 
[**api_skins_delete_group_logo**](DefaultApi.md#api_skins_delete_group_logo) | **DELETE** /{group_id}/logo/{logo_type} | 
[**api_skins_delete_group_theme**](DefaultApi.md#api_skins_delete_group_theme) | **DELETE** /{group_id}/theme | 
[**api_skins_get_default_logo**](DefaultApi.md#api_skins_get_default_logo) | **GET** /logo/{logo_type} | 
[**api_skins_get_default_theme**](DefaultApi.md#api_skins_get_default_theme) | **GET** /theme | 
[**api_skins_get_favicon**](DefaultApi.md#api_skins_get_favicon) | **GET** /favicon/ | 
[**api_skins_get_group_logo**](DefaultApi.md#api_skins_get_group_logo) | **GET** /{group_id}/logo/{logo_type} | 
[**api_skins_get_group_theme**](DefaultApi.md#api_skins_get_group_theme) | **GET** /{group_id}/theme | 
[**api_skins_put_default_logo**](DefaultApi.md#api_skins_put_default_logo) | **PUT** /logo/{logo_type} | 
[**api_skins_put_default_theme**](DefaultApi.md#api_skins_put_default_theme) | **PUT** /theme | 
[**api_skins_put_favicon**](DefaultApi.md#api_skins_put_favicon) | **PUT** /favicon/ | 
[**api_skins_put_group_logo**](DefaultApi.md#api_skins_put_group_logo) | **PUT** /{group_id}/logo/{logo_type} | 
[**api_skins_put_group_theme**](DefaultApi.md#api_skins_put_group_theme) | **PUT** /{group_id}/theme | 


# **api_skins_delete_default_logo**
> api_skins_delete_default_logo(logo_type)



Deletes the default logo

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_skins
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)
    logo_type = 'logo_type_example' # str | Where to use the logo (page|report)

    try:
        api_instance.api_skins_delete_default_logo(logo_type)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_delete_default_logo: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logo_type** | **str**| Where to use the logo (page|report) | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_delete_default_theme**
> api_skins_delete_default_theme()



Deletes the default theme

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_skins
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)

    try:
        api_instance.api_skins_delete_default_theme()
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_delete_default_theme: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_delete_favicon**
> api_skins_delete_favicon()



Deletes the default favicon

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_skins
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)

    try:
        api_instance.api_skins_delete_favicon()
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_delete_favicon: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_delete_group_logo**
> api_skins_delete_group_logo(group_id, logo_type)



deletes the logo of a group

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_skins
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group
    logo_type = 'logo_type_example' # str | Where to use the logo (page|report)

    try:
        api_instance.api_skins_delete_group_logo(group_id, logo_type)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_delete_group_logo: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 
 **logo_type** | **str**| Where to use the logo (page|report) | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_delete_group_theme**
> api_skins_delete_group_theme(group_id)



Deletes the theme of a group

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_skins
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group

    try:
        api_instance.api_skins_delete_group_theme(group_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_delete_group_theme: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_get_default_logo**
> bytearray api_skins_get_default_logo(logo_type, to_save=to_save)



Returns default Imbio logo

### Example

```python
import time
import os
import core_skins
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)


# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)
    logo_type = 'logo_type_example' # str | Where to use the logo (page|report)
    to_save = True # bool | Set true if you want to save the file instead of getting to content of it  (optional)

    try:
        api_response = api_instance.api_skins_get_default_logo(logo_type, to_save=to_save)
        print("The response of DefaultApi->api_skins_get_default_logo:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_get_default_logo: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logo_type** | **str**| Where to use the logo (page|report) | 
 **to_save** | **bool**| Set true if you want to save the file instead of getting to content of it  | [optional] 

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/png

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns logo |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_get_default_theme**
> str api_skins_get_default_theme()



Returns default Imbio theme

### Example

```python
import time
import os
import core_skins
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)


# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)

    try:
        api_response = api_instance.api_skins_get_default_theme()
        print("The response of DefaultApi->api_skins_get_default_theme:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_get_default_theme: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns theme object |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_get_favicon**
> bytearray api_skins_get_favicon(to_save=to_save)



Returns default favicon

### Example

```python
import time
import os
import core_skins
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)


# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)
    to_save = True # bool | Set true if you want to save the file instead of getting to content of it  (optional)

    try:
        api_response = api_instance.api_skins_get_favicon(to_save=to_save)
        print("The response of DefaultApi->api_skins_get_favicon:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_get_favicon: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **to_save** | **bool**| Set true if you want to save the file instead of getting to content of it  | [optional] 

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/x-icon

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns Imbio favicon |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_get_group_logo**
> bytearray api_skins_get_group_logo(group_id, logo_type, to_save=to_save)



Returns the logo of a group

### Example

```python
import time
import os
import core_skins
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)


# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group
    logo_type = 'logo_type_example' # str | Where to use the logo (page|report)
    to_save = True # bool | Set true if you want to save the file instead of getting to content of it  (optional)

    try:
        api_response = api_instance.api_skins_get_group_logo(group_id, logo_type, to_save=to_save)
        print("The response of DefaultApi->api_skins_get_group_logo:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_get_group_logo: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 
 **logo_type** | **str**| Where to use the logo (page|report) | 
 **to_save** | **bool**| Set true if you want to save the file instead of getting to content of it  | [optional] 

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/png

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns logo |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_get_group_theme**
> str api_skins_get_group_theme(group_id)



Returns the theme of a group

### Example

```python
import time
import os
import core_skins
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)


# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group

    try:
        api_response = api_instance.api_skins_get_group_theme(group_id)
        print("The response of DefaultApi->api_skins_get_group_theme:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_get_group_theme: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns theme object |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_put_default_logo**
> ApiSkinsPutFavicon200Response api_skins_put_default_logo(logo_type, logo)



Uploads a new default logo

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_skins
from core_skins.models.api_skins_put_favicon200_response import ApiSkinsPutFavicon200Response
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)
    logo_type = 'logo_type_example' # str | Where to use the logo (page|report)
    logo = None # bytearray | logo file

    try:
        api_response = api_instance.api_skins_put_default_logo(logo_type, logo)
        print("The response of DefaultApi->api_skins_put_default_logo:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_put_default_logo: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logo_type** | **str**| Where to use the logo (page|report) | 
 **logo** | **bytearray**| logo file | 

### Return type

[**ApiSkinsPutFavicon200Response**](ApiSkinsPutFavicon200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns url object |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_put_default_theme**
> api_skins_put_default_theme(body)



Uploads a new default theme

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_skins
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)
    body = None # object | theme

    try:
        api_instance.api_skins_put_default_theme(body)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_put_default_theme: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**| theme | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_put_favicon**
> ApiSkinsPutFavicon200Response api_skins_put_favicon(favicon)



Uploads a new default favicon

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_skins
from core_skins.models.api_skins_put_favicon200_response import ApiSkinsPutFavicon200Response
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)
    favicon = None # bytearray | favicon file

    try:
        api_response = api_instance.api_skins_put_favicon(favicon)
        print("The response of DefaultApi->api_skins_put_favicon:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_put_favicon: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **favicon** | **bytearray**| favicon file | 

### Return type

[**ApiSkinsPutFavicon200Response**](ApiSkinsPutFavicon200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns url object |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_put_group_logo**
> ApiSkinsPutFavicon200Response api_skins_put_group_logo(group_id, logo_type, logo)



Uploads a logo of a group

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_skins
from core_skins.models.api_skins_put_favicon200_response import ApiSkinsPutFavicon200Response
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group
    logo_type = 'logo_type_example' # str | Where to use the logo (page|report)
    logo = None # bytearray | logo file

    try:
        api_response = api_instance.api_skins_put_group_logo(group_id, logo_type, logo)
        print("The response of DefaultApi->api_skins_put_group_logo:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_put_group_logo: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 
 **logo_type** | **str**| Where to use the logo (page|report) | 
 **logo** | **bytearray**| logo file | 

### Return type

[**ApiSkinsPutFavicon200Response**](ApiSkinsPutFavicon200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns url object |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_skins_put_group_theme**
> api_skins_put_group_theme(group_id, body)



Uploads a theme of a group

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_skins
from core_skins.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/skins
# See configuration.py for a list of all supported configuration parameters.
configuration = core_skins.Configuration(
    host = "https://launchpad.imbio.com/api/v1/skins"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_skins.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_skins.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group
    body = None # object | theme

    try:
        api_instance.api_skins_put_group_theme(group_id, body)
    except Exception as e:
        print("Exception when calling DefaultApi->api_skins_put_group_theme: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 
 **body** | **object**| theme | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

