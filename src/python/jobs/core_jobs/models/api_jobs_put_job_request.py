# coding: utf-8

"""
    IMBIO core api for jobs

    Methods for managing jobs 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


from __future__ import annotations
import pprint
import re  # noqa: F401
import json


from typing import Any, ClassVar, Dict, List, Optional, Union
from pydantic import BaseModel, StrictBool, StrictInt, StrictStr, field_validator
from pydantic import Field
from typing_extensions import Annotated
try:
    from typing import Self
except ImportError:
    from typing_extensions import Self

class ApiJobsPutJobRequest(BaseModel):
    """
    The description of a job update.
    """ # noqa: E501
    name: Optional[Annotated[str, Field(strict=True)]] = Field(default=None, description="The name of the job ")
    status: Optional[StrictStr] = Field(default=None, description="Job statuses ")
    output_asset_ids: Optional[List[StrictInt]] = Field(default=None, description="The asset ids of the processed assets ")
    report_ids: Optional[List[StrictInt]] = Field(default=None, description="The asset ids of the processed assets ")
    alg_return_code: Optional[StrictInt] = Field(default=None, description="The return code of the algorithm container ")
    alg_std_out: Optional[StrictStr] = Field(default=None, description="The std out of the algorithm container ")
    alg_std_err: Optional[StrictStr] = Field(default=None, description="The std out of the algorithm container ")
    alg_results: Optional[Union[str, Any]] = Field(default=None, description="Raw results section from reports.json ")
    error_message: Optional[StrictStr] = Field(default=None, description="All error messages except for the std err of the algorithm ")
    exported_to_seg_editor: Optional[StrictBool] = Field(default=None, description="Was this job exported to seg editor ")
    secret_signing_key: Optional[StrictStr] = Field(default=None, description="Secret signing key for the seg editor ")
    edited: Optional[StrictBool] = Field(default=None, description="Whether this job was edited by SET ")
    set_expired: Optional[StrictBool] = Field(default=None, description="Signals that the SET input has expired ")
    show_seg_editor: Optional[StrictBool] = Field(default=None, description="Shows whether the seg editor icon should be displayed for this job ")
    qualified_candidate: Optional[StrictBool] = Field(default=None, description="Shows whether the job is Qualified Candidate or not ")
    passed_screening: Optional[StrictBool] = Field(default=None, description="Shows whether the job has passed screening ")
    followup_recommended: Optional[StrictBool] = Field(default=None, description="Shows whether the job is recommended for followup ")
    study_id: Optional[StrictInt] = Field(default=None, description="Study's id in ICCP ")
    __properties: ClassVar[List[str]] = ["name", "status", "output_asset_ids", "report_ids", "alg_return_code", "alg_std_out", "alg_std_err", "alg_results", "error_message", "exported_to_seg_editor", "secret_signing_key", "edited", "set_expired", "show_seg_editor", "qualified_candidate", "passed_screening", "followup_recommended", "study_id"]

    @field_validator('name')
    def name_validate_regular_expression(cls, value):
        """Validates the regular expression"""
        if value is None:
            return value

        if not re.match(r"^[a-zA-Z0-9-_]{1,40}$", value):
            raise ValueError(r"must validate the regular expression /^[a-zA-Z0-9-_]{1,40}$/")
        return value

    @field_validator('status')
    def status_validate_enum(cls, value):
        """Validates the enum"""
        if value is None:
            return value

        if value not in ('created', 'preparing', 'running', 'cleanup', 'input_failure', 'qa_failure', 'alg_failure', 'export_failure', 'set_export_failure', 'orthanc_export_failure', 'failure', 'uploading', 'uploaded', 'ready_for_set', 'ready_for_orthanc', 'exporting', 'exporting_to_set', 'exporting_to_orthanc', 'waiting_for_overread', 'overread_complete', 'waiting_for_qa', 'qa_overdue', 'qa_complete', 'complete', 'input_review', 'screened_out', 'followup_recommended', 'followup_in_progress', 'followup_complete', 'on_hold'):
            raise ValueError("must be one of enum values ('created', 'preparing', 'running', 'cleanup', 'input_failure', 'qa_failure', 'alg_failure', 'export_failure', 'set_export_failure', 'orthanc_export_failure', 'failure', 'uploading', 'uploaded', 'ready_for_set', 'ready_for_orthanc', 'exporting', 'exporting_to_set', 'exporting_to_orthanc', 'waiting_for_overread', 'overread_complete', 'waiting_for_qa', 'qa_overdue', 'qa_complete', 'complete', 'input_review', 'screened_out', 'followup_recommended', 'followup_in_progress', 'followup_complete', 'on_hold')")
        return value

    model_config = {
        "populate_by_name": True,
        "validate_assignment": True
    }


    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.model_dump(by_alias=True))

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        # TODO: pydantic v2: use .model_dump_json(by_alias=True, exclude_unset=True) instead
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> Self:
        """Create an instance of ApiJobsPutJobRequest from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self) -> Dict[str, Any]:
        """Return the dictionary representation of the model using alias.

        This has the following differences from calling pydantic's
        `self.model_dump(by_alias=True)`:

        * `None` is only added to the output dict for nullable fields that
          were set at model initialization. Other fields with value `None`
          are ignored.
        """
        _dict = self.model_dump(
            by_alias=True,
            exclude={
            },
            exclude_none=True,
        )
        return _dict

    @classmethod
    def from_dict(cls, obj: Dict) -> Self:
        """Create an instance of ApiJobsPutJobRequest from a dict"""
        if obj is None:
            return None

        if not isinstance(obj, dict):
            return cls.model_validate(obj)

        _obj = cls.model_validate({
            "name": obj.get("name"),
            "status": obj.get("status"),
            "output_asset_ids": obj.get("output_asset_ids"),
            "report_ids": obj.get("report_ids"),
            "alg_return_code": obj.get("alg_return_code"),
            "alg_std_out": obj.get("alg_std_out"),
            "alg_std_err": obj.get("alg_std_err"),
            "alg_results": obj.get("alg_results"),
            "error_message": obj.get("error_message"),
            "exported_to_seg_editor": obj.get("exported_to_seg_editor"),
            "secret_signing_key": obj.get("secret_signing_key"),
            "edited": obj.get("edited"),
            "set_expired": obj.get("set_expired"),
            "show_seg_editor": obj.get("show_seg_editor"),
            "qualified_candidate": obj.get("qualified_candidate"),
            "passed_screening": obj.get("passed_screening"),
            "followup_recommended": obj.get("followup_recommended"),
            "study_id": obj.get("study_id")
        })
        return _obj


