# ApiJobsSetExportDoneRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**secret_signing_key** | **str** |  | [optional] 
**error_message** | **str** |  | [optional] 

## Example

```python
from core_jobs.models.api_jobs_set_export_done_request import ApiJobsSetExportDoneRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsSetExportDoneRequest from a JSON string
api_jobs_set_export_done_request_instance = ApiJobsSetExportDoneRequest.from_json(json)
# print the JSON string representation of the object
print ApiJobsSetExportDoneRequest.to_json()

# convert the object into a dict
api_jobs_set_export_done_request_dict = api_jobs_set_export_done_request_instance.to_dict()
# create an instance of ApiJobsSetExportDoneRequest from a dict
api_jobs_set_export_done_request_form_dict = api_jobs_set_export_done_request.from_dict(api_jobs_set_export_done_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


