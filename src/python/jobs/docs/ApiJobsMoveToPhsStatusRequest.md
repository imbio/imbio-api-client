# ApiJobsMoveToPhsStatusRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_status** | **str** |  | [optional] 

## Example

```python
from core_jobs.models.api_jobs_move_to_phs_status_request import ApiJobsMoveToPhsStatusRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsMoveToPhsStatusRequest from a JSON string
api_jobs_move_to_phs_status_request_instance = ApiJobsMoveToPhsStatusRequest.from_json(json)
# print the JSON string representation of the object
print ApiJobsMoveToPhsStatusRequest.to_json()

# convert the object into a dict
api_jobs_move_to_phs_status_request_dict = api_jobs_move_to_phs_status_request_instance.to_dict()
# create an instance of ApiJobsMoveToPhsStatusRequest from a dict
api_jobs_move_to_phs_status_request_form_dict = api_jobs_move_to_phs_status_request.from_dict(api_jobs_move_to_phs_status_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


