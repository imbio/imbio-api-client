# ApiJobsGetAllJobs200ResponseItemsInnerOwner

Some info of a user

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of a user | [optional] 
**name** | **str** | name of a user or part of email | [optional] 
**email** | **str** | email of a user | [optional] 

## Example

```python
from core_jobs.models.api_jobs_get_all_jobs200_response_items_inner_owner import ApiJobsGetAllJobs200ResponseItemsInnerOwner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsGetAllJobs200ResponseItemsInnerOwner from a JSON string
api_jobs_get_all_jobs200_response_items_inner_owner_instance = ApiJobsGetAllJobs200ResponseItemsInnerOwner.from_json(json)
# print the JSON string representation of the object
print ApiJobsGetAllJobs200ResponseItemsInnerOwner.to_json()

# convert the object into a dict
api_jobs_get_all_jobs200_response_items_inner_owner_dict = api_jobs_get_all_jobs200_response_items_inner_owner_instance.to_dict()
# create an instance of ApiJobsGetAllJobs200ResponseItemsInnerOwner from a dict
api_jobs_get_all_jobs200_response_items_inner_owner_form_dict = api_jobs_get_all_jobs200_response_items_inner_owner.from_dict(api_jobs_get_all_jobs200_response_items_inner_owner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


