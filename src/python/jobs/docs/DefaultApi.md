# core_jobs.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/jobs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_comments_add_comment**](DefaultApi.md#api_comments_add_comment) | **POST** /actions/{job_id}/comments | 
[**api_comments_delete_comment**](DefaultApi.md#api_comments_delete_comment) | **DELETE** /actions/{job_id}/comments/{comment_id} | 
[**api_comments_put_comment**](DefaultApi.md#api_comments_put_comment) | **PUT** /actions/{job_id}/comments/{comment_id} | 
[**api_jobs_approve_job**](DefaultApi.md#api_jobs_approve_job) | **POST** /actions/approve/{job_id} | 
[**api_jobs_delete_job**](DefaultApi.md#api_jobs_delete_job) | **DELETE** /{job_id} | 
[**api_jobs_delete_patient_info**](DefaultApi.md#api_jobs_delete_patient_info) | **POST** /actions/delete_patient_info/{job_id} | 
[**api_jobs_get_all_jobs**](DefaultApi.md#api_jobs_get_all_jobs) | **GET** / | 
[**api_jobs_get_editor_url**](DefaultApi.md#api_jobs_get_editor_url) | **POST** /actions/get_seg_editor_url/{job_id} | 
[**api_jobs_get_job**](DefaultApi.md#api_jobs_get_job) | **GET** /{job_id} | 
[**api_jobs_get_summary**](DefaultApi.md#api_jobs_get_summary) | **GET** /summary | 
[**api_jobs_move_to_phs_status**](DefaultApi.md#api_jobs_move_to_phs_status) | **POST** /actions/move_to_phs_status/{job_id} | 
[**api_jobs_orthanc_export_done**](DefaultApi.md#api_jobs_orthanc_export_done) | **POST** /actions/orthanc_export_done/{job_id} | 
[**api_jobs_post_job**](DefaultApi.md#api_jobs_post_job) | **POST** / | 
[**api_jobs_push_back_to_qa**](DefaultApi.md#api_jobs_push_back_to_qa) | **POST** /actions/push_back_to_qa/{job_id} | 
[**api_jobs_put_job**](DefaultApi.md#api_jobs_put_job) | **PUT** /{job_id} | 
[**api_jobs_reject_job**](DefaultApi.md#api_jobs_reject_job) | **POST** /actions/reject/{job_id} | 
[**api_jobs_rerun**](DefaultApi.md#api_jobs_rerun) | **POST** /actions/rerun/{job_id} | 
[**api_jobs_rerun_with_new_input**](DefaultApi.md#api_jobs_rerun_with_new_input) | **POST** /actions/rerun_with_new_input/{job_id} | 
[**api_jobs_select_job**](DefaultApi.md#api_jobs_select_job) | **POST** /actions/select_job | 
[**api_jobs_select_job_for_orthanc**](DefaultApi.md#api_jobs_select_job_for_orthanc) | **POST** /actions/select_job_for_orthanc | 
[**api_jobs_select_job_for_set**](DefaultApi.md#api_jobs_select_job_for_set) | **POST** /actions/select_job_for_set | 
[**api_jobs_set_export_done**](DefaultApi.md#api_jobs_set_export_done) | **POST** /actions/set_export_done/{job_id} | 
[**api_jobs_start_next**](DefaultApi.md#api_jobs_start_next) | **POST** /actions/start_next/{job_id} | 
[**api_jobs_terminate_job**](DefaultApi.md#api_jobs_terminate_job) | **POST** /actions/terminate/{job_id} | 
[**api_jobs_update_job_and_move_to_next_status**](DefaultApi.md#api_jobs_update_job_and_move_to_next_status) | **POST** /actions/move_to_next_status/{job_id} | 
[**api_results_add_result**](DefaultApi.md#api_results_add_result) | **POST** /actions/{job_id}/results | 
[**api_results_delete_result**](DefaultApi.md#api_results_delete_result) | **DELETE** /actions/{job_id}/results/{result_id} | 
[**api_results_put_result**](DefaultApi.md#api_results_put_result) | **PUT** /actions/{job_id}/results/{result_id} | 


# **api_comments_add_comment**
> ApiJobsPostJob200Response api_comments_add_comment(job_id, api_comments_add_comment_request)



Add free text comment to a job

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_comments_add_comment_request import ApiCommentsAddCommentRequest
from core_jobs.models.api_jobs_post_job200_response import ApiJobsPostJob200Response
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    api_comments_add_comment_request = core_jobs.ApiCommentsAddCommentRequest() # ApiCommentsAddCommentRequest | Comment body description 

    try:
        api_response = api_instance.api_comments_add_comment(job_id, api_comments_add_comment_request)
        print("The response of DefaultApi->api_comments_add_comment:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_comments_add_comment: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **api_comments_add_comment_request** | [**ApiCommentsAddCommentRequest**](ApiCommentsAddCommentRequest.md)| Comment body description  | 

### Return type

[**ApiJobsPostJob200Response**](ApiJobsPostJob200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_comments_delete_comment**
> api_comments_delete_comment(job_id, comment_id)



Deletes a job's comment

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    comment_id = 'comment_id_example' # str | The unique identifier of a comment

    try:
        api_instance.api_comments_delete_comment(job_id, comment_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_comments_delete_comment: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **comment_id** | **str**| The unique identifier of a comment | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_comments_put_comment**
> api_comments_put_comment(job_id, comment_id, api_comments_add_comment_request)



Modifies a job's comment and updates owner

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_comments_add_comment_request import ApiCommentsAddCommentRequest
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    comment_id = 'comment_id_example' # str | The unique identifier of a comment
    api_comments_add_comment_request = core_jobs.ApiCommentsAddCommentRequest() # ApiCommentsAddCommentRequest | Edit comment request body

    try:
        api_instance.api_comments_put_comment(job_id, comment_id, api_comments_add_comment_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_comments_put_comment: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **comment_id** | **str**| The unique identifier of a comment | 
 **api_comments_add_comment_request** | [**ApiCommentsAddCommentRequest**](ApiCommentsAddCommentRequest.md)| Edit comment request body | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_approve_job**
> api_jobs_approve_job(job_id, api_jobs_approve_job_request)



Approves a job; the outputs of the job will be made available

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_approve_job_request import ApiJobsApproveJobRequest
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    api_jobs_approve_job_request = core_jobs.ApiJobsApproveJobRequest() # ApiJobsApproveJobRequest | Approve job's body description 

    try:
        api_instance.api_jobs_approve_job(job_id, api_jobs_approve_job_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_approve_job: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **api_jobs_approve_job_request** | [**ApiJobsApproveJobRequest**](ApiJobsApproveJobRequest.md)| Approve job&#39;s body description  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**403** | 403: Permission denied to perform operation  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_delete_job**
> api_jobs_delete_job(job_id)



Deletes one job. 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job

    try:
        api_instance.api_jobs_delete_job(job_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_delete_job: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_delete_patient_info**
> api_jobs_delete_patient_info(job_id)



deletes all patient infos

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job

    try:
        api_instance.api_jobs_delete_patient_info(job_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_delete_patient_info: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_get_all_jobs**
> ApiJobsGetAllJobs200Response api_jobs_get_all_jobs(limit=limit, offset=offset, order=order, search=search, algorithm_id=algorithm_id, group_id=group_id, item_id=item_id, response_format=response_format, order_by=order_by, accession_number=accession_number, show_deleted=show_deleted, job_after=job_after, job_before=job_before, job_changed_after=job_changed_after, job_changed_before=job_changed_before, status=status, study_before=study_before, study_after=study_after, minage=minage, maxage=maxage, patient_sex=patient_sex)



Returns all jobs

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_get_all_jobs200_response import ApiJobsGetAllJobs200Response
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    limit = 50 # int | Limit the number of returned roles to indicated value (default 50).  (optional) (default to 50)
    offset = 0 # int | When set the query returns 'limit' number of roles but skip 'offset' number of roles at the beginning.  (optional) (default to 0)
    order = 'order_example' # str | Specify the order of the listing: desc, asc  (optional)
    search = 'search_example' # str | Specify a search string  (optional)
    algorithm_id = [56] # List[int] | Filter by algorithm(s)  (optional)
    group_id = [56] # List[int] | Filter by group(s)  (optional)
    item_id = [56] # List[int] | Filter resource by their primary id  (optional)
    response_format = 'response_format_example' # str | How detialed the response should be  (optional)
    order_by = 'order_by_example' # str | Order by the provided argument name.  (optional)
    accession_number = ['accession_number_example'] # List[str] | Restrict the list to this accession number (optional)
    show_deleted = True # bool | Restrict the list of jobs to include deleted as well or not (default false)  (optional)
    job_after = '2013-10-20' # date | Restrict the list of jobs created after this date (optional)
    job_before = '2013-10-20' # date | Restrict the list of jobs created before this date (optional)
    job_changed_after = 'job_changed_after_example' # str | Restrict the list of jobs created after this date (optional)
    job_changed_before = 'job_changed_before_example' # str | Restrict the list of jobs created before this date (optional)
    status = ['status_example'] # List[str] | Restrict the list of jobs to only those with provided statuses.  (optional)
    study_before = '2013-10-20' # date | Restrict the list of assets with study before this date (optional)
    study_after = '2013-10-20' # date | Restrict the list of assets with study after this date (optional)
    minage = 56 # int | Filter by age  (optional)
    maxage = 56 # int | Filter by age  (optional)
    patient_sex = ['patient_sex_example'] # List[str] | Filter by patient's gender  (optional)

    try:
        api_response = api_instance.api_jobs_get_all_jobs(limit=limit, offset=offset, order=order, search=search, algorithm_id=algorithm_id, group_id=group_id, item_id=item_id, response_format=response_format, order_by=order_by, accession_number=accession_number, show_deleted=show_deleted, job_after=job_after, job_before=job_before, job_changed_after=job_changed_after, job_changed_before=job_changed_before, status=status, study_before=study_before, study_after=study_after, minage=minage, maxage=maxage, patient_sex=patient_sex)
        print("The response of DefaultApi->api_jobs_get_all_jobs:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_get_all_jobs: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Limit the number of returned roles to indicated value (default 50).  | [optional] [default to 50]
 **offset** | **int**| When set the query returns &#39;limit&#39; number of roles but skip &#39;offset&#39; number of roles at the beginning.  | [optional] [default to 0]
 **order** | **str**| Specify the order of the listing: desc, asc  | [optional] 
 **search** | **str**| Specify a search string  | [optional] 
 **algorithm_id** | [**List[int]**](int.md)| Filter by algorithm(s)  | [optional] 
 **group_id** | [**List[int]**](int.md)| Filter by group(s)  | [optional] 
 **item_id** | [**List[int]**](int.md)| Filter resource by their primary id  | [optional] 
 **response_format** | **str**| How detialed the response should be  | [optional] 
 **order_by** | **str**| Order by the provided argument name.  | [optional] 
 **accession_number** | [**List[str]**](str.md)| Restrict the list to this accession number | [optional] 
 **show_deleted** | **bool**| Restrict the list of jobs to include deleted as well or not (default false)  | [optional] 
 **job_after** | **date**| Restrict the list of jobs created after this date | [optional] 
 **job_before** | **date**| Restrict the list of jobs created before this date | [optional] 
 **job_changed_after** | **str**| Restrict the list of jobs created after this date | [optional] 
 **job_changed_before** | **str**| Restrict the list of jobs created before this date | [optional] 
 **status** | [**List[str]**](str.md)| Restrict the list of jobs to only those with provided statuses.  | [optional] 
 **study_before** | **date**| Restrict the list of assets with study before this date | [optional] 
 **study_after** | **date**| Restrict the list of assets with study after this date | [optional] 
 **minage** | **int**| Filter by age  | [optional] 
 **maxage** | **int**| Filter by age  | [optional] 
 **patient_sex** | [**List[str]**](str.md)| Filter by patient&#39;s gender  | [optional] 

### Return type

[**ApiJobsGetAllJobs200Response**](ApiJobsGetAllJobs200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get all resource |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_get_editor_url**
> ApiJobsGetEditorUrl200Response api_jobs_get_editor_url(job_id)



Returns the seg editor url

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_get_editor_url200_response import ApiJobsGetEditorUrl200Response
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job

    try:
        api_response = api_instance.api_jobs_get_editor_url(job_id)
        print("The response of DefaultApi->api_jobs_get_editor_url:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_get_editor_url: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 

### Return type

[**ApiJobsGetEditorUrl200Response**](ApiJobsGetEditorUrl200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns url object |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_get_job**
> ApiJobsGetAllJobs200ResponseItemsInner api_jobs_get_job(job_id)



Returns the current settings for the job. 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_get_all_jobs200_response_items_inner import ApiJobsGetAllJobs200ResponseItemsInner
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job

    try:
        api_response = api_instance.api_jobs_get_job(job_id)
        print("The response of DefaultApi->api_jobs_get_job:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_get_job: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 

### Return type

[**ApiJobsGetAllJobs200ResponseItemsInner**](ApiJobsGetAllJobs200ResponseItemsInner.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The settings for the job. |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_get_summary**
> object api_jobs_get_summary()



Returns the number of jobs per instance type

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)

    try:
        api_response = api_instance.api_jobs_get_summary()
        print("The response of DefaultApi->api_jobs_get_summary:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_get_summary: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the number of jobs per instance type |  -  |
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_move_to_phs_status**
> api_jobs_move_to_phs_status(job_id, api_jobs_move_to_phs_status_request)



Push job to next PHS status

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_move_to_phs_status_request import ApiJobsMoveToPhsStatusRequest
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    api_jobs_move_to_phs_status_request = core_jobs.ApiJobsMoveToPhsStatusRequest() # ApiJobsMoveToPhsStatusRequest | Next status 

    try:
        api_instance.api_jobs_move_to_phs_status(job_id, api_jobs_move_to_phs_status_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_move_to_phs_status: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **api_jobs_move_to_phs_status_request** | [**ApiJobsMoveToPhsStatusRequest**](ApiJobsMoveToPhsStatusRequest.md)| Next status  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**403** | 403: Permission denied to perform operation  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_orthanc_export_done**
> api_jobs_orthanc_export_done(job_id, api_jobs_set_export_done_request)



Orthanc export is done, puts job to the next status

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_set_export_done_request import ApiJobsSetExportDoneRequest
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    api_jobs_set_export_done_request = core_jobs.ApiJobsSetExportDoneRequest() # ApiJobsSetExportDoneRequest | Initial properties of the job. 

    try:
        api_instance.api_jobs_orthanc_export_done(job_id, api_jobs_set_export_done_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_orthanc_export_done: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **api_jobs_set_export_done_request** | [**ApiJobsSetExportDoneRequest**](ApiJobsSetExportDoneRequest.md)| Initial properties of the job.  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_post_job**
> ApiJobsPostJob200Response api_jobs_post_job(api_jobs_post_job_request)



Create a new job

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_post_job200_response import ApiJobsPostJob200Response
from core_jobs.models.api_jobs_post_job_request import ApiJobsPostJobRequest
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    api_jobs_post_job_request = core_jobs.ApiJobsPostJobRequest() # ApiJobsPostJobRequest | Initial properties of the job. 

    try:
        api_response = api_instance.api_jobs_post_job(api_jobs_post_job_request)
        print("The response of DefaultApi->api_jobs_post_job:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_post_job: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_jobs_post_job_request** | [**ApiJobsPostJobRequest**](ApiJobsPostJobRequest.md)| Initial properties of the job.  | 

### Return type

[**ApiJobsPostJob200Response**](ApiJobsPostJob200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_push_back_to_qa**
> api_jobs_push_back_to_qa(job_id)



Push back a job to QA review

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job

    try:
        api_instance.api_jobs_push_back_to_qa(job_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_push_back_to_qa: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**403** | 403: Permission denied to perform operation  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_put_job**
> api_jobs_put_job(job_id, api_jobs_put_job_request)



Updates the job specified with job_id 

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_put_job_request import ApiJobsPutJobRequest
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    api_jobs_put_job_request = core_jobs.ApiJobsPutJobRequest() # ApiJobsPutJobRequest | Initial properties of the job. 

    try:
        api_instance.api_jobs_put_job(job_id, api_jobs_put_job_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_put_job: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **api_jobs_put_job_request** | [**ApiJobsPutJobRequest**](ApiJobsPutJobRequest.md)| Initial properties of the job.  | 

### Return type

void (empty response body)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_reject_job**
> api_jobs_reject_job(job_id)



Rejects a job; the outputs of the job will be deleted

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job

    try:
        api_instance.api_jobs_reject_job(job_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_reject_job: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**403** | 403: Permission denied to perform operation  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_rerun**
> ApiJobsPostJob200Response api_jobs_rerun(job_id)



rerun a job

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_post_job200_response import ApiJobsPostJob200Response
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job

    try:
        api_response = api_instance.api_jobs_rerun(job_id)
        print("The response of DefaultApi->api_jobs_rerun:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_rerun: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 

### Return type

[**ApiJobsPostJob200Response**](ApiJobsPostJob200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_rerun_with_new_input**
> ApiJobsPostJob200Response api_jobs_rerun_with_new_input(job_id, api_jobs_rerun_with_new_input_request)



rerun a job

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_post_job200_response import ApiJobsPostJob200Response
from core_jobs.models.api_jobs_rerun_with_new_input_request import ApiJobsRerunWithNewInputRequest
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    api_jobs_rerun_with_new_input_request = core_jobs.ApiJobsRerunWithNewInputRequest() # ApiJobsRerunWithNewInputRequest | New assets for an input reviewed job 

    try:
        api_response = api_instance.api_jobs_rerun_with_new_input(job_id, api_jobs_rerun_with_new_input_request)
        print("The response of DefaultApi->api_jobs_rerun_with_new_input:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_rerun_with_new_input: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **api_jobs_rerun_with_new_input_request** | [**ApiJobsRerunWithNewInputRequest**](ApiJobsRerunWithNewInputRequest.md)| New assets for an input reviewed job  | 

### Return type

[**ApiJobsPostJob200Response**](ApiJobsPostJob200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |
**400** | 400: Bad request, operation not processed  |  -  |
**403** | 403: Permission denied to perform operation  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_select_job**
> ApiJobsGetAllJobs200ResponseItemsInner api_jobs_select_job(instance_type)



Returns a job_id from the created set

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_get_all_jobs200_response_items_inner import ApiJobsGetAllJobs200ResponseItemsInner
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    instance_type = 'instance_type_example' # str | The instance type of the job which should be selected if there is any If all is specified then the instance type won't be taken into account 

    try:
        api_response = api_instance.api_jobs_select_job(instance_type)
        print("The response of DefaultApi->api_jobs_select_job:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_select_job: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **instance_type** | **str**| The instance type of the job which should be selected if there is any If all is specified then the instance type won&#39;t be taken into account  | 

### Return type

[**ApiJobsGetAllJobs200ResponseItemsInner**](ApiJobsGetAllJobs200ResponseItemsInner.md)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The settings for the job. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_select_job_for_orthanc**
> ApiJobsGetAllJobs200ResponseItemsInner api_jobs_select_job_for_orthanc()



Returns a job_id from the created set

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_get_all_jobs200_response_items_inner import ApiJobsGetAllJobs200ResponseItemsInner
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)

    try:
        api_response = api_instance.api_jobs_select_job_for_orthanc()
        print("The response of DefaultApi->api_jobs_select_job_for_orthanc:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_select_job_for_orthanc: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiJobsGetAllJobs200ResponseItemsInner**](ApiJobsGetAllJobs200ResponseItemsInner.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The settings for the job. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_select_job_for_set**
> ApiJobsGetAllJobs200ResponseItemsInner api_jobs_select_job_for_set()



Returns a job_id from the created set

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_get_all_jobs200_response_items_inner import ApiJobsGetAllJobs200ResponseItemsInner
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)

    try:
        api_response = api_instance.api_jobs_select_job_for_set()
        print("The response of DefaultApi->api_jobs_select_job_for_set:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_select_job_for_set: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiJobsGetAllJobs200ResponseItemsInner**](ApiJobsGetAllJobs200ResponseItemsInner.md)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The settings for the job. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_set_export_done**
> api_jobs_set_export_done(job_id, api_jobs_set_export_done_request)



Set export is done, puts job to the next status

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_set_export_done_request import ApiJobsSetExportDoneRequest
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    api_jobs_set_export_done_request = core_jobs.ApiJobsSetExportDoneRequest() # ApiJobsSetExportDoneRequest | Initial properties of the job. 

    try:
        api_instance.api_jobs_set_export_done(job_id, api_jobs_set_export_done_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_set_export_done: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **api_jobs_set_export_done_request** | [**ApiJobsSetExportDoneRequest**](ApiJobsSetExportDoneRequest.md)| Initial properties of the job.  | 

### Return type

void (empty response body)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_start_next**
> ApiJobsPostJob200Response api_jobs_start_next(job_id, api_jobs_start_next_request)



Job's edit is done and starts a new job with some new params

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_post_job200_response import ApiJobsPostJob200Response
from core_jobs.models.api_jobs_start_next_request import ApiJobsStartNextRequest
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    api_jobs_start_next_request = core_jobs.ApiJobsStartNextRequest() # ApiJobsStartNextRequest | Initial properties of the job. 

    try:
        api_response = api_instance.api_jobs_start_next(job_id, api_jobs_start_next_request)
        print("The response of DefaultApi->api_jobs_start_next:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_start_next: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **api_jobs_start_next_request** | [**ApiJobsStartNextRequest**](ApiJobsStartNextRequest.md)| Initial properties of the job.  | 

### Return type

[**ApiJobsPostJob200Response**](ApiJobsPostJob200Response.md)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_terminate_job**
> api_jobs_terminate_job(job_id)



Terminate the job with the given id if it's in a not finished state

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job

    try:
        api_instance.api_jobs_terminate_job(job_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_terminate_job: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |
**403** | 403: Permission denied to perform operation  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_jobs_update_job_and_move_to_next_status**
> api_jobs_update_job_and_move_to_next_status(job_id, api_jobs_put_job_request)



Updates the job specified with job_id 

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_put_job_request import ApiJobsPutJobRequest
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    api_jobs_put_job_request = core_jobs.ApiJobsPutJobRequest() # ApiJobsPutJobRequest | Initial properties of the job. 

    try:
        api_instance.api_jobs_update_job_and_move_to_next_status(job_id, api_jobs_put_job_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_jobs_update_job_and_move_to_next_status: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **api_jobs_put_job_request** | [**ApiJobsPutJobRequest**](ApiJobsPutJobRequest.md)| Initial properties of the job.  | 

### Return type

void (empty response body)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_results_add_result**
> ApiJobsPostJob200Response api_results_add_result(job_id, api_results_add_result_request)



Add key-value result to a job

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_jobs_post_job200_response import ApiJobsPostJob200Response
from core_jobs.models.api_results_add_result_request import ApiResultsAddResultRequest
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    api_results_add_result_request = core_jobs.ApiResultsAddResultRequest() # ApiResultsAddResultRequest | Result body description 

    try:
        api_response = api_instance.api_results_add_result(job_id, api_results_add_result_request)
        print("The response of DefaultApi->api_results_add_result:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_results_add_result: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **api_results_add_result_request** | [**ApiResultsAddResultRequest**](ApiResultsAddResultRequest.md)| Result body description  | 

### Return type

[**ApiJobsPostJob200Response**](ApiJobsPostJob200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_results_delete_result**
> api_results_delete_result(job_id, result_id)



Deletes a job's result

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    result_id = 'result_id_example' # str | The unique identifier of a result

    try:
        api_instance.api_results_delete_result(job_id, result_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_results_delete_result: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **result_id** | **str**| The unique identifier of a result | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_results_put_result**
> api_results_put_result(job_id, result_id, api_results_put_result_request)



Modifies a job's result and updates owner

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_jobs
from core_jobs.models.api_results_put_result_request import ApiResultsPutResultRequest
from core_jobs.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/jobs
# See configuration.py for a list of all supported configuration parameters.
configuration = core_jobs.Configuration(
    host = "https://launchpad.imbio.com/api/v1/jobs"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_jobs.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_jobs.DefaultApi(api_client)
    job_id = 'job_id_example' # str | The unique identifier of a job
    result_id = 'result_id_example' # str | The unique identifier of a result
    api_results_put_result_request = core_jobs.ApiResultsPutResultRequest() # ApiResultsPutResultRequest | Edit result request body

    try:
        api_instance.api_results_put_result(job_id, result_id, api_results_put_result_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_results_put_result: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **job_id** | **str**| The unique identifier of a job | 
 **result_id** | **str**| The unique identifier of a result | 
 **api_results_put_result_request** | [**ApiResultsPutResultRequest**](ApiResultsPutResultRequest.md)| Edit result request body | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

