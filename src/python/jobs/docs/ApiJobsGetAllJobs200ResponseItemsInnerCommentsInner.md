# ApiJobsGetAllJobs200ResponseItemsInnerCommentsInner

description of a comment in response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of the comment | [optional] 
**created_at** | **str** | timestamp when the comment was created | [optional] 
**modified_at** | **str** | timestamp when the comment was modified | [optional] 
**value** | **str** | content of the comment | [optional] 
**owner_id** | **int** | The id of the owner  | [optional] 
**modified_by** | [**ApiJobsGetAllJobs200ResponseItemsInnerOwner**](ApiJobsGetAllJobs200ResponseItemsInnerOwner.md) |  | [optional] 
**allowed_actions** | [**ApiJobsGetAllJobs200ResponseItemsInnerCommentsInnerAllowedActions**](ApiJobsGetAllJobs200ResponseItemsInnerCommentsInnerAllowedActions.md) |  | [optional] 

## Example

```python
from core_jobs.models.api_jobs_get_all_jobs200_response_items_inner_comments_inner import ApiJobsGetAllJobs200ResponseItemsInnerCommentsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsGetAllJobs200ResponseItemsInnerCommentsInner from a JSON string
api_jobs_get_all_jobs200_response_items_inner_comments_inner_instance = ApiJobsGetAllJobs200ResponseItemsInnerCommentsInner.from_json(json)
# print the JSON string representation of the object
print ApiJobsGetAllJobs200ResponseItemsInnerCommentsInner.to_json()

# convert the object into a dict
api_jobs_get_all_jobs200_response_items_inner_comments_inner_dict = api_jobs_get_all_jobs200_response_items_inner_comments_inner_instance.to_dict()
# create an instance of ApiJobsGetAllJobs200ResponseItemsInnerCommentsInner from a dict
api_jobs_get_all_jobs200_response_items_inner_comments_inner_form_dict = api_jobs_get_all_jobs200_response_items_inner_comments_inner.from_dict(api_jobs_get_all_jobs200_response_items_inner_comments_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


