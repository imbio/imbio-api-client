# ApiJobsGetAllJobs200ResponseItemsInnerCommentsInnerAllowedActions


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**edit** | **bool** | shows if user can edit the commment | [optional] 
**delete** | **bool** | shows if user can delete the commment | [optional] 

## Example

```python
from core_jobs.models.api_jobs_get_all_jobs200_response_items_inner_comments_inner_allowed_actions import ApiJobsGetAllJobs200ResponseItemsInnerCommentsInnerAllowedActions

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsGetAllJobs200ResponseItemsInnerCommentsInnerAllowedActions from a JSON string
api_jobs_get_all_jobs200_response_items_inner_comments_inner_allowed_actions_instance = ApiJobsGetAllJobs200ResponseItemsInnerCommentsInnerAllowedActions.from_json(json)
# print the JSON string representation of the object
print ApiJobsGetAllJobs200ResponseItemsInnerCommentsInnerAllowedActions.to_json()

# convert the object into a dict
api_jobs_get_all_jobs200_response_items_inner_comments_inner_allowed_actions_dict = api_jobs_get_all_jobs200_response_items_inner_comments_inner_allowed_actions_instance.to_dict()
# create an instance of ApiJobsGetAllJobs200ResponseItemsInnerCommentsInnerAllowedActions from a dict
api_jobs_get_all_jobs200_response_items_inner_comments_inner_allowed_actions_form_dict = api_jobs_get_all_jobs200_response_items_inner_comments_inner_allowed_actions.from_dict(api_jobs_get_all_jobs200_response_items_inner_comments_inner_allowed_actions_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


