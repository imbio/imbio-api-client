# ApiJobsGetAllJobs200ResponseItemsInner

The description of a job.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Job Id  | [optional] 
**name** | **str** | The name of the job  | [optional] 
**group** | [**ApiJobsGetAllJobs200ResponseItemsInnerGroup**](ApiJobsGetAllJobs200ResponseItemsInnerGroup.md) |  | [optional] 
**group_id** | **int** | The id of the group that the job belongs to  | [optional] 
**owner_id** | **int** | The id of the owner  | [optional] 
**owner** | [**ApiJobsGetAllJobs200ResponseItemsInnerOwner**](ApiJobsGetAllJobs200ResponseItemsInnerOwner.md) |  | [optional] 
**status** | **str** | Job statuses  | [optional] 
**algorithm_id** | **int** | The id of the algorithm that is used to run the job  | [optional] 
**profile_id** | **int** | The id of the profile  | [optional] 
**arguments** | **str** | The arguments passed to the algorithm  | [optional] 
**algorithm** | [**ApiJobsGetAllJobs200ResponseItemsInnerAlgorithm**](ApiJobsGetAllJobs200ResponseItemsInnerAlgorithm.md) |  | [optional] 
**assets** | [**List[ApiJobsGetAllJobs200ResponseItemsInnerAssetsInner]**](ApiJobsGetAllJobs200ResponseItemsInnerAssetsInner.md) | A list of assets associated with this job  | [optional] 
**output_asset_id** | **int** | Id of the output asset  | [optional] 
**profile** | [**ApiJobsGetAllJobs200ResponseItemsInnerProfile**](ApiJobsGetAllJobs200ResponseItemsInnerProfile.md) |  | [optional] 
**compute_instance_type** | **str** | The instance type required to run the algorithm  | [optional] 
**exported_to_seg_editor** | **bool** | Was this job exported to seg editor  | [optional] 
**alg_return_code** | **int** | The return code of the algorithm container  | [optional] 
**alg_std_out** | **str** | The std out of the algorithm container  | [optional] 
**alg_std_err** | **str** | The std out of the algorithm container  | [optional] 
**error_message** | **str** | All error messages except for the std err of the algorithm  | [optional] 
**timezone** | **str** | shows the timezone of the job | [optional] 
**created_at** | **str** | The time when the job was created  | [optional] 
**initial_upload_date** | **str** | The time when the original (first) job was created  | [optional] 
**modified_at** | **str** | The time when the job was last modified  | [optional] 
**set_opened** | **str** | The time when SET was opened for job  | [optional] 
**set_closed** | **str** | The time when SET was closed for job  | [optional] 
**completed_at** | **str** | The time when the job entered completed or failure state  | [optional] 
**last_status_change** | **str** | The time when the job&#39;s status changed last time  | [optional] 
**edited** | **bool** | Whether this job was edited by SET  | [optional] 
**qualified_candidate** | **bool** | Shows whether the job is Qualified Candidate or not  | [optional] 
**show_seg_editor** | **bool** | Shows whether the seg editor icon should be displayed for this job  | [optional] 
**original_job_id** | **int** | The id of the job whose image data was used through several edits to create this job  | [optional] 
**edited_job_id** | **int** | The id of the job whose image data was edited to create this job  | [optional] 
**priority** | **int** | priority level for this job  | [optional] 
**review_required** | **bool** | Shows whether review is required on this job  | [optional] 
**custom_validator_orthanc** | **str** | Template used to verify whether incoming studies can be accepted by the algorithms  | [optional] 
**orthanc_host** | **str** | The orthanc server to poll for new studies  | [optional] 
**orthanc_http_port** | **int** | The HTTP port of the orthanc server that is polled for new studies  | [optional] 
**orthanc_dicom_port** | **int** | The dicom port of the orthanc server that is polled for new studies  | [optional] 
**orthanc_aet** | **str** | The AET of the profile on the orthanc server that is polled for new studies  | [optional] 
**orthanc_store_addr** | **str** | The IP of the orthanc server where the results will be uploaded to (destination orthanc server)  | [optional] 
**orthanc_store_port** | **int** | The port of the destination orthanc server  | [optional] 
**orthanc_store_aet** | **str** | The AET at the destination orthanc server  | [optional] 
**output_filter_rule** | **str** | A filter rule that filters results that should be stored on the destination orthanc server  | [optional] 
**qa_workflow_enabled** | **bool** | Shows if the qa review workflow is enabled | [optional] 
**overread_workflow_enabled** | **bool** | Shows if the overread workflow is enabled | [optional] 
**overread_request_enabled** | **bool** | Shows if overread request functionality is enabled | [optional] 
**seg_editor_enabled** | **bool** | Shows if the segmentation editor is enabled | [optional] 
**editor_target** | **str** | The segmentation editor in use for this job | [optional] 
**workflows** | [**List[ApiJobsGetAllJobs200ResponseItemsInnerGroupWorkflowsInner]**](ApiJobsGetAllJobs200ResponseItemsInnerGroupWorkflowsInner.md) |  | [optional] 
**upload_source** | **str** | Source where the job was created from (eg. api|ui|bridge|etc.) | [optional] 
**reviewed** | **bool** | shows if the job was reviewed | [optional] 
**overread** | **bool** | shows if the job was overread | [optional] 
**deleted** | **bool** | shows if the job was deleted | [optional] 
**history** | [**List[ApiJobsGetAllJobs200ResponseItemsInnerHistoryInner]**](ApiJobsGetAllJobs200ResponseItemsInnerHistoryInner.md) | List of job, which are related to each other  | [optional] 
**study** | [**ApiJobsGetAllJobs200ResponseItemsInnerStudy**](ApiJobsGetAllJobs200ResponseItemsInnerStudy.md) |  | [optional] 
**allowed_actions** | [**ApiJobsGetAllJobs200ResponseItemsInnerAllowedActions**](ApiJobsGetAllJobs200ResponseItemsInnerAllowedActions.md) |  | [optional] 
**allowed_statuses** | **List[str]** | list of statuses for manual status change | [optional] 
**comments** | [**List[ApiJobsGetAllJobs200ResponseItemsInnerCommentsInner]**](ApiJobsGetAllJobs200ResponseItemsInnerCommentsInner.md) | list of comments | [optional] 
**results** | [**List[ApiJobsGetAllJobs200ResponseItemsInnerResultsInner]**](ApiJobsGetAllJobs200ResponseItemsInnerResultsInner.md) | list of results | [optional] 

## Example

```python
from core_jobs.models.api_jobs_get_all_jobs200_response_items_inner import ApiJobsGetAllJobs200ResponseItemsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsGetAllJobs200ResponseItemsInner from a JSON string
api_jobs_get_all_jobs200_response_items_inner_instance = ApiJobsGetAllJobs200ResponseItemsInner.from_json(json)
# print the JSON string representation of the object
print ApiJobsGetAllJobs200ResponseItemsInner.to_json()

# convert the object into a dict
api_jobs_get_all_jobs200_response_items_inner_dict = api_jobs_get_all_jobs200_response_items_inner_instance.to_dict()
# create an instance of ApiJobsGetAllJobs200ResponseItemsInner from a dict
api_jobs_get_all_jobs200_response_items_inner_form_dict = api_jobs_get_all_jobs200_response_items_inner.from_dict(api_jobs_get_all_jobs200_response_items_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


