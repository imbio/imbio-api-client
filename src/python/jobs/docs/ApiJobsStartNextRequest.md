# ApiJobsStartNextRequest

Some additional properties to start a new job with

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**input_asset_ids** | **List[int]** | The mask ids that will be processed by the algorithms with the original assets  | [optional] 
**arguments** | **str** | The arguments passed to the algorithm  | [optional] 
**set_expired** | **bool** | Signals that the SET input has expired  | [optional] 
**overread_required** | **bool** | If set to true, than overread workflow will be turned on for the job  | [optional] 
**qa_overdue** | **bool** | Signals that the job hasn&#39;t been edited for a specific amount of time  | [optional] 

## Example

```python
from core_jobs.models.api_jobs_start_next_request import ApiJobsStartNextRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsStartNextRequest from a JSON string
api_jobs_start_next_request_instance = ApiJobsStartNextRequest.from_json(json)
# print the JSON string representation of the object
print ApiJobsStartNextRequest.to_json()

# convert the object into a dict
api_jobs_start_next_request_dict = api_jobs_start_next_request_instance.to_dict()
# create an instance of ApiJobsStartNextRequest from a dict
api_jobs_start_next_request_form_dict = api_jobs_start_next_request.from_dict(api_jobs_start_next_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


