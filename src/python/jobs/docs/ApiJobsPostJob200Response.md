# ApiJobsPostJob200Response


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 

## Example

```python
from core_jobs.models.api_jobs_post_job200_response import ApiJobsPostJob200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsPostJob200Response from a JSON string
api_jobs_post_job200_response_instance = ApiJobsPostJob200Response.from_json(json)
# print the JSON string representation of the object
print ApiJobsPostJob200Response.to_json()

# convert the object into a dict
api_jobs_post_job200_response_dict = api_jobs_post_job200_response_instance.to_dict()
# create an instance of ApiJobsPostJob200Response from a dict
api_jobs_post_job200_response_form_dict = api_jobs_post_job200_response.from_dict(api_jobs_post_job200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


