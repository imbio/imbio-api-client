# ApiJobsGetAllJobs200ResponseItemsInnerAllowedActions

An object containing the allowed actions Based on these flags the UI can determine which controls it has to display 

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**edit** | **bool** | Shows whether this job was edited with SET  | [optional] 
**set_expired** | **bool** | Shows whether the SET input has expired since the job was finished If the data has been expired then it cannot be edited  | [optional] 
**show_csv_icon** | **bool** | Shows whether this job has CSV data  | [optional] 
**show_pdf_icon** | **bool** | Shows whether this job has PDF data  | [optional] 
**show_seg_editor** | **bool** | Shows whether this job has or had SET data  | [optional] 
**show_reject** | **bool** | Shows whether whether the job can be rejected  | [optional] 
**show_output_download_icon** | **bool** | Shows whether this job has an output asset with files to download  | [optional] 
**show_input_download_icon** | **bool** | Shows whether this job has an input asset with files to download  | [optional] 
**show_trash_icon** | **bool** | Shows whether patient data can be deleted from this job  | [optional] 
**rerun** | **bool** | Shows if a job is rerunable  | [optional] 
**show_post_treatment_icon** | **bool** | Shows if a job can be post treated  | [optional] 
**show_create_result_icon** | **bool** | true if user can upload result | [optional] 
**show_edit_result_icon** | **bool** | true if user can modify result | [optional] 
**show_create_comment_icon** | **bool** | true if user can comment to the job | [optional] 

## Example

```python
from core_jobs.models.api_jobs_get_all_jobs200_response_items_inner_allowed_actions import ApiJobsGetAllJobs200ResponseItemsInnerAllowedActions

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsGetAllJobs200ResponseItemsInnerAllowedActions from a JSON string
api_jobs_get_all_jobs200_response_items_inner_allowed_actions_instance = ApiJobsGetAllJobs200ResponseItemsInnerAllowedActions.from_json(json)
# print the JSON string representation of the object
print ApiJobsGetAllJobs200ResponseItemsInnerAllowedActions.to_json()

# convert the object into a dict
api_jobs_get_all_jobs200_response_items_inner_allowed_actions_dict = api_jobs_get_all_jobs200_response_items_inner_allowed_actions_instance.to_dict()
# create an instance of ApiJobsGetAllJobs200ResponseItemsInnerAllowedActions from a dict
api_jobs_get_all_jobs200_response_items_inner_allowed_actions_form_dict = api_jobs_get_all_jobs200_response_items_inner_allowed_actions.from_dict(api_jobs_get_all_jobs200_response_items_inner_allowed_actions_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


