# ApiJobsApproveJobRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**overread_required** | **bool** |  | [optional] [default to False]

## Example

```python
from core_jobs.models.api_jobs_approve_job_request import ApiJobsApproveJobRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsApproveJobRequest from a JSON string
api_jobs_approve_job_request_instance = ApiJobsApproveJobRequest.from_json(json)
# print the JSON string representation of the object
print ApiJobsApproveJobRequest.to_json()

# convert the object into a dict
api_jobs_approve_job_request_dict = api_jobs_approve_job_request_instance.to_dict()
# create an instance of ApiJobsApproveJobRequest from a dict
api_jobs_approve_job_request_form_dict = api_jobs_approve_job_request.from_dict(api_jobs_approve_job_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


