# ApiJobsRerunWithNewInputRequest

The job description for starting an input reviewed job

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**input_asset_ids** | **List[int]** | The new asset ids for this job  | 

## Example

```python
from core_jobs.models.api_jobs_rerun_with_new_input_request import ApiJobsRerunWithNewInputRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsRerunWithNewInputRequest from a JSON string
api_jobs_rerun_with_new_input_request_instance = ApiJobsRerunWithNewInputRequest.from_json(json)
# print the JSON string representation of the object
print ApiJobsRerunWithNewInputRequest.to_json()

# convert the object into a dict
api_jobs_rerun_with_new_input_request_dict = api_jobs_rerun_with_new_input_request_instance.to_dict()
# create an instance of ApiJobsRerunWithNewInputRequest from a dict
api_jobs_rerun_with_new_input_request_form_dict = api_jobs_rerun_with_new_input_request.from_dict(api_jobs_rerun_with_new_input_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


