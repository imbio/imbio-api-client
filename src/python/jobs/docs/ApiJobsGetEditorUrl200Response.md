# ApiJobsGetEditorUrl200Response

Object containing an url

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **str** |  | [optional] 

## Example

```python
from core_jobs.models.api_jobs_get_editor_url200_response import ApiJobsGetEditorUrl200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsGetEditorUrl200Response from a JSON string
api_jobs_get_editor_url200_response_instance = ApiJobsGetEditorUrl200Response.from_json(json)
# print the JSON string representation of the object
print ApiJobsGetEditorUrl200Response.to_json()

# convert the object into a dict
api_jobs_get_editor_url200_response_dict = api_jobs_get_editor_url200_response_instance.to_dict()
# create an instance of ApiJobsGetEditorUrl200Response from a dict
api_jobs_get_editor_url200_response_form_dict = api_jobs_get_editor_url200_response.from_dict(api_jobs_get_editor_url200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


