# ApiCommentsAddCommentRequest

description of a post/put comment request

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **str** | The content of the comment | 
**type** | **str** | type of the comment | [optional] 

## Example

```python
from core_jobs.models.api_comments_add_comment_request import ApiCommentsAddCommentRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiCommentsAddCommentRequest from a JSON string
api_comments_add_comment_request_instance = ApiCommentsAddCommentRequest.from_json(json)
# print the JSON string representation of the object
print ApiCommentsAddCommentRequest.to_json()

# convert the object into a dict
api_comments_add_comment_request_dict = api_comments_add_comment_request_instance.to_dict()
# create an instance of ApiCommentsAddCommentRequest from a dict
api_comments_add_comment_request_form_dict = api_comments_add_comment_request.from_dict(api_comments_add_comment_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


