# ApiJobsPutJobRequest

The description of a job update.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the job  | [optional] 
**status** | **str** | Job statuses  | [optional] 
**output_asset_ids** | **List[int]** | The asset ids of the processed assets  | [optional] 
**report_ids** | **List[int]** | The asset ids of the processed assets  | [optional] 
**alg_return_code** | **int** | The return code of the algorithm container  | [optional] 
**alg_std_out** | **str** | The std out of the algorithm container  | [optional] 
**alg_std_err** | **str** | The std out of the algorithm container  | [optional] 
**alg_results** | **object** | Raw results section from reports.json  | [optional] 
**error_message** | **str** | All error messages except for the std err of the algorithm  | [optional] 
**exported_to_seg_editor** | **bool** | Was this job exported to seg editor  | [optional] 
**secret_signing_key** | **str** | Secret signing key for the seg editor  | [optional] 
**edited** | **bool** | Whether this job was edited by SET  | [optional] 
**set_expired** | **bool** | Signals that the SET input has expired  | [optional] 
**show_seg_editor** | **bool** | Shows whether the seg editor icon should be displayed for this job  | [optional] 
**qualified_candidate** | **bool** | Shows whether the job is Qualified Candidate or not  | [optional] 
**passed_screening** | **bool** | Shows whether the job has passed screening  | [optional] 
**followup_recommended** | **bool** | Shows whether the job is recommended for followup  | [optional] 
**study_id** | **int** | Study&#39;s id in ICCP  | [optional] 

## Example

```python
from core_jobs.models.api_jobs_put_job_request import ApiJobsPutJobRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsPutJobRequest from a JSON string
api_jobs_put_job_request_instance = ApiJobsPutJobRequest.from_json(json)
# print the JSON string representation of the object
print ApiJobsPutJobRequest.to_json()

# convert the object into a dict
api_jobs_put_job_request_dict = api_jobs_put_job_request_instance.to_dict()
# create an instance of ApiJobsPutJobRequest from a dict
api_jobs_put_job_request_form_dict = api_jobs_put_job_request.from_dict(api_jobs_put_job_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


