# ApiResultsPutResultRequest

Description of an update object for result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of the result, such as name of the measurement | [optional] 
**value** | **str** | Some value of the result, what was measured | [optional] 
**type** | **str** | Type of the result | [optional] 

## Example

```python
from core_jobs.models.api_results_put_result_request import ApiResultsPutResultRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiResultsPutResultRequest from a JSON string
api_results_put_result_request_instance = ApiResultsPutResultRequest.from_json(json)
# print the JSON string representation of the object
print ApiResultsPutResultRequest.to_json()

# convert the object into a dict
api_results_put_result_request_dict = api_results_put_result_request_instance.to_dict()
# create an instance of ApiResultsPutResultRequest from a dict
api_results_put_result_request_form_dict = api_results_put_result_request.from_dict(api_results_put_result_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


