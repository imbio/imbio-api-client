# ApiJobsPostJobRequest

The description of a job.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name of the job  | 
**algorithm_id** | **int** | The id of the algorithm that is used to run the job  | 
**arguments** | **str** | The arguments passed to the algorithm  | 
**input_asset_ids** | **List[int]** | The asset ids that will be processed by the algorithms  | 
**profile_id** | **int** | The id of the profile which stores the orthanc server info to which the results have to be uploaded  | [optional] 
**study** | [**ApiJobsGetAllJobs200ResponseItemsInnerStudy**](ApiJobsGetAllJobs200ResponseItemsInnerStudy.md) |  | [optional] 
**group_id** | **int** | The id of the group with which the job is created  | 
**original_job_id** | **int** | The id of the job whose image data was used through several edits to create this job  | [optional] 
**edited_job_id** | **int** | The id of the job whose image data was edited to create this job  | [optional] 
**error_message** | **str** | If specified a job will be created in input_failure state  | [optional] 
**workflows** | **List[int]** |  | [optional] 
**upload_source** | **str** | Source where the job was created from (eg. api|ui|bridge|etc.) | [optional] 
**callbacks** | [**List[ApiJobsPostJobRequestCallbacksInner]**](ApiJobsPostJobRequestCallbacksInner.md) |  | [optional] 

## Example

```python
from core_jobs.models.api_jobs_post_job_request import ApiJobsPostJobRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsPostJobRequest from a JSON string
api_jobs_post_job_request_instance = ApiJobsPostJobRequest.from_json(json)
# print the JSON string representation of the object
print ApiJobsPostJobRequest.to_json()

# convert the object into a dict
api_jobs_post_job_request_dict = api_jobs_post_job_request_instance.to_dict()
# create an instance of ApiJobsPostJobRequest from a dict
api_jobs_post_job_request_form_dict = api_jobs_post_job_request.from_dict(api_jobs_post_job_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


