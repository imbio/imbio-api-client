# ApiJobsPostJobRequestCallbacksInner

This object contains callback information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **str** | The url where the request will be sent | 
**method** | **str** | The request method | [optional] 
**data** | **object** | json meta to send | [optional] 
**headers** | **object** | Header to used with the callback | [optional] 

## Example

```python
from core_jobs.models.api_jobs_post_job_request_callbacks_inner import ApiJobsPostJobRequestCallbacksInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsPostJobRequestCallbacksInner from a JSON string
api_jobs_post_job_request_callbacks_inner_instance = ApiJobsPostJobRequestCallbacksInner.from_json(json)
# print the JSON string representation of the object
print ApiJobsPostJobRequestCallbacksInner.to_json()

# convert the object into a dict
api_jobs_post_job_request_callbacks_inner_dict = api_jobs_post_job_request_callbacks_inner_instance.to_dict()
# create an instance of ApiJobsPostJobRequestCallbacksInner from a dict
api_jobs_post_job_request_callbacks_inner_form_dict = api_jobs_post_job_request_callbacks_inner.from_dict(api_jobs_post_job_request_callbacks_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


