# ApiJobsGetAllJobs200ResponseItemsInnerHistoryInner


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of a job | [optional] 
**name** | **str** | name of a job | [optional] 
**workflows** | [**List[ApiJobsGetAllJobs200ResponseItemsInnerGroupWorkflowsInner]**](ApiJobsGetAllJobs200ResponseItemsInnerGroupWorkflowsInner.md) |  | [optional] 
**upload_source** | **str** | Source where the job was created from (eg. api|ui|bridge|etc.) | [optional] 
**algorithm** | [**ApiJobsGetAllJobs200ResponseItemsInnerHistoryInnerAlgorithm**](ApiJobsGetAllJobs200ResponseItemsInnerHistoryInnerAlgorithm.md) |  | [optional] 
**status** | **str** | status of a job | [optional] 
**created_at** | **str** | The time when the job was created | [optional] 

## Example

```python
from core_jobs.models.api_jobs_get_all_jobs200_response_items_inner_history_inner import ApiJobsGetAllJobs200ResponseItemsInnerHistoryInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiJobsGetAllJobs200ResponseItemsInnerHistoryInner from a JSON string
api_jobs_get_all_jobs200_response_items_inner_history_inner_instance = ApiJobsGetAllJobs200ResponseItemsInnerHistoryInner.from_json(json)
# print the JSON string representation of the object
print ApiJobsGetAllJobs200ResponseItemsInnerHistoryInner.to_json()

# convert the object into a dict
api_jobs_get_all_jobs200_response_items_inner_history_inner_dict = api_jobs_get_all_jobs200_response_items_inner_history_inner_instance.to_dict()
# create an instance of ApiJobsGetAllJobs200ResponseItemsInnerHistoryInner from a dict
api_jobs_get_all_jobs200_response_items_inner_history_inner_form_dict = api_jobs_get_all_jobs200_response_items_inner_history_inner.from_dict(api_jobs_get_all_jobs200_response_items_inner_history_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


