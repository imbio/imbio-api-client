# coding: utf-8

"""
    IMBIO core api for jobs

    Methods for managing jobs 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_jobs.models.api_jobs_get_all_jobs200_response_items_inner_algorithm import ApiJobsGetAllJobs200ResponseItemsInnerAlgorithm

class TestApiJobsGetAllJobs200ResponseItemsInnerAlgorithm(unittest.TestCase):
    """ApiJobsGetAllJobs200ResponseItemsInnerAlgorithm unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiJobsGetAllJobs200ResponseItemsInnerAlgorithm:
        """Test ApiJobsGetAllJobs200ResponseItemsInnerAlgorithm
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiJobsGetAllJobs200ResponseItemsInnerAlgorithm`
        """
        model = ApiJobsGetAllJobs200ResponseItemsInnerAlgorithm()
        if include_optional:
            return ApiJobsGetAllJobs200ResponseItemsInnerAlgorithm(
                id = 56,
                name = '',
                description = '',
                version = '',
                active = True,
                post_treatment_algorithm = True,
                archive_path = '',
                archive_name = '',
                compute_instance_type = '',
                shell_command = '',
                owner_id = 56,
                validator_orthanc = '',
                required_number_of_assets = 56,
                logo_url = '',
                show_arguments = [
                    None
                    ],
                series_description = [
                    None
                    ],
                workflows = [
                    core_jobs.models.api_jobs_get_all_jobs_200_response_items_inner_group_workflows_inner.api_jobs_get_all_jobs_200_response_items_inner_group_workflows_inner(
                        id = 56, 
                        name = '', )
                    ]
            )
        else:
            return ApiJobsGetAllJobs200ResponseItemsInnerAlgorithm(
        )
        """

    def testApiJobsGetAllJobs200ResponseItemsInnerAlgorithm(self):
        """Test ApiJobsGetAllJobs200ResponseItemsInnerAlgorithm"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
