# coding: utf-8

"""
    IMBIO core api for jobs

    Methods for managing jobs 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_jobs.models.api_jobs_get_all_jobs200_response_items_inner_owner import ApiJobsGetAllJobs200ResponseItemsInnerOwner

class TestApiJobsGetAllJobs200ResponseItemsInnerOwner(unittest.TestCase):
    """ApiJobsGetAllJobs200ResponseItemsInnerOwner unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiJobsGetAllJobs200ResponseItemsInnerOwner:
        """Test ApiJobsGetAllJobs200ResponseItemsInnerOwner
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiJobsGetAllJobs200ResponseItemsInnerOwner`
        """
        model = ApiJobsGetAllJobs200ResponseItemsInnerOwner()
        if include_optional:
            return ApiJobsGetAllJobs200ResponseItemsInnerOwner(
                id = 56,
                name = '',
                email = ''
            )
        else:
            return ApiJobsGetAllJobs200ResponseItemsInnerOwner(
        )
        """

    def testApiJobsGetAllJobs200ResponseItemsInnerOwner(self):
        """Test ApiJobsGetAllJobs200ResponseItemsInnerOwner"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
