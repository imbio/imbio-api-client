# coding: utf-8

"""
    IMBIO core api for jobs

    Methods for managing jobs 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_jobs.models.api_jobs_post_job_request_callbacks_inner import ApiJobsPostJobRequestCallbacksInner

class TestApiJobsPostJobRequestCallbacksInner(unittest.TestCase):
    """ApiJobsPostJobRequestCallbacksInner unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiJobsPostJobRequestCallbacksInner:
        """Test ApiJobsPostJobRequestCallbacksInner
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiJobsPostJobRequestCallbacksInner`
        """
        model = ApiJobsPostJobRequestCallbacksInner()
        if include_optional:
            return ApiJobsPostJobRequestCallbacksInner(
                url = '',
                method = 'POST',
                data = core_jobs.models.data.data(),
                headers = core_jobs.models.headers.headers()
            )
        else:
            return ApiJobsPostJobRequestCallbacksInner(
                url = '',
        )
        """

    def testApiJobsPostJobRequestCallbacksInner(self):
        """Test ApiJobsPostJobRequestCallbacksInner"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
