# ApiAssetsGetAsset404Response

The error object

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **int** | The error code for this error | [optional] 
**message** | **str** | The error message for this error | [optional] 

## Example

```python
from core_assets.models.api_assets_get_asset404_response import ApiAssetsGetAsset404Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiAssetsGetAsset404Response from a JSON string
api_assets_get_asset404_response_instance = ApiAssetsGetAsset404Response.from_json(json)
# print the JSON string representation of the object
print ApiAssetsGetAsset404Response.to_json()

# convert the object into a dict
api_assets_get_asset404_response_dict = api_assets_get_asset404_response_instance.to_dict()
# create an instance of ApiAssetsGetAsset404Response from a dict
api_assets_get_asset404_response_form_dict = api_assets_get_asset404_response.from_dict(api_assets_get_asset404_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


