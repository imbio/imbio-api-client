# ApiAssetsPostAssetRequest

Study properties parsed from the DICOM study that will be used by the UI/bridge 

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | An optional name to the asset  | [optional] 
**asset_type** | **str** | The type of the asset  | 
**group_id** | **int** | The id of the group with which the asset is created  | 
**series_options_log** | **object** | Option that the series was selected from | [optional] 
**deid_values** | **object** | Object containing deidentified dicom attributes and their values | [optional] 
**deidentified** | **bool** | The asset was deidentified  | [optional] 

## Example

```python
from core_assets.models.api_assets_post_asset_request import ApiAssetsPostAssetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiAssetsPostAssetRequest from a JSON string
api_assets_post_asset_request_instance = ApiAssetsPostAssetRequest.from_json(json)
# print the JSON string representation of the object
print ApiAssetsPostAssetRequest.to_json()

# convert the object into a dict
api_assets_post_asset_request_dict = api_assets_post_asset_request_instance.to_dict()
# create an instance of ApiAssetsPostAssetRequest from a dict
api_assets_post_asset_request_form_dict = api_assets_post_asset_request.from_dict(api_assets_post_asset_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


