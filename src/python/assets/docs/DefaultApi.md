# core_assets.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/assets*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_assets_cleanup_assets**](DefaultApi.md#api_assets_cleanup_assets) | **POST** /actions/cleanup | 
[**api_assets_cleanup_assets_not_tracked**](DefaultApi.md#api_assets_cleanup_assets_not_tracked) | **POST** /actions/cleanup/not_tracked | 
[**api_assets_delete_asset**](DefaultApi.md#api_assets_delete_asset) | **DELETE** /{asset_id} | 
[**api_assets_download_by_token**](DefaultApi.md#api_assets_download_by_token) | **GET** /{asset_id}/archive/{asset_token} | 
[**api_assets_get_all_assets**](DefaultApi.md#api_assets_get_all_assets) | **GET** / | 
[**api_assets_get_asset**](DefaultApi.md#api_assets_get_asset) | **GET** /{asset_id} | 
[**api_assets_get_asset_archive**](DefaultApi.md#api_assets_get_asset_archive) | **GET** /{asset_id}/archive | 
[**api_assets_get_asset_report_aggregated_csv**](DefaultApi.md#api_assets_get_asset_report_aggregated_csv) | **GET** /reports/aggregated_csv | 
[**api_assets_get_asset_report_aggregated_pdf**](DefaultApi.md#api_assets_get_asset_report_aggregated_pdf) | **GET** /reports/aggregated_pdf | 
[**api_assets_get_asset_report_csv**](DefaultApi.md#api_assets_get_asset_report_csv) | **GET** /{asset_id}/reports/csv | 
[**api_assets_get_asset_report_pdf**](DefaultApi.md#api_assets_get_asset_report_pdf) | **GET** /{asset_id}/reports/pdf | 
[**api_assets_get_download_url**](DefaultApi.md#api_assets_get_download_url) | **GET** /{asset_id}/download_url | 
[**api_assets_get_upload_url**](DefaultApi.md#api_assets_get_upload_url) | **GET** /{asset_id}/upload_url | 
[**api_assets_post_asset**](DefaultApi.md#api_assets_post_asset) | **POST** / | 
[**api_assets_put_asset_archive**](DefaultApi.md#api_assets_put_asset_archive) | **PUT** /{asset_id}/archive | 
[**api_assets_put_asset_dicom**](DefaultApi.md#api_assets_put_asset_dicom) | **PUT** /{asset_id}/dicom | 
[**api_assets_put_asset_report_csv**](DefaultApi.md#api_assets_put_asset_report_csv) | **PUT** /{asset_id}/reports/csv | 
[**api_assets_put_asset_report_pdf**](DefaultApi.md#api_assets_put_asset_report_pdf) | **PUT** /{asset_id}/reports/pdf | 
[**api_assets_upload_by_token**](DefaultApi.md#api_assets_upload_by_token) | **PUT** /{asset_id}/archive/{asset_token} | 
[**api_assets_upload_done**](DefaultApi.md#api_assets_upload_done) | **POST** /actions/{asset_id}/upload_done | 


# **api_assets_cleanup_assets**
> api_assets_cleanup_assets()



Deletes all assets which are older than the group storage time 

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)

    try:
        api_instance.api_assets_cleanup_assets()
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_cleanup_assets: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**206** | 206: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_cleanup_assets_not_tracked**
> api_assets_cleanup_assets_not_tracked()



Deletes all files, not recorded in the database 

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)

    try:
        api_instance.api_assets_cleanup_assets_not_tracked()
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_cleanup_assets_not_tracked: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**206** | 206: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_delete_asset**
> api_assets_delete_asset(asset_id)



Deletes one asset. 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset

    try:
        api_instance.api_assets_delete_asset(asset_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_delete_asset: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_download_by_token**
> bytearray api_assets_download_by_token(asset_id, asset_token, to_save=to_save)



Downloads a file with a token An url in this form can be get with the {asset_id}/download_url endpoint 

### Example

```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)


# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset
    asset_token = 'asset_token_example' # str | The download token which contains the description of the downloadable object in encrypted format 
    to_save = True # bool | Set true if you want to save the file instead of getting to content of it  (optional)

    try:
        api_response = api_instance.api_assets_download_by_token(asset_id, asset_token, to_save=to_save)
        print("The response of DefaultApi->api_assets_download_by_token:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_download_by_token: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 
 **asset_token** | **str**| The download token which contains the description of the downloadable object in encrypted format  | 
 **to_save** | **bool**| Set true if you want to save the file instead of getting to content of it  | [optional] 

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the asset archive |  * Content-Disposition -  <br>  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_get_all_assets**
> ApiAssetsGetAllAssets200Response api_assets_get_all_assets(limit=limit, offset=offset, order=order, search=search, item_id=item_id, group_id=group_id, response_format=response_format, asset_type=asset_type, order_by=order_by, show_all=show_all, study_after=study_after, study_before=study_before)



Returns all assets descriptors (but not the archive or reports)

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.models.api_assets_get_all_assets200_response import ApiAssetsGetAllAssets200Response
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    limit = 50 # int | Limit the number of returned roles to indicated value (default 50).  (optional) (default to 50)
    offset = 0 # int | When set the query returns 'limit' number of roles but skip 'offset' number of roles at the beginning.  (optional) (default to 0)
    order = 'order_example' # str | Specify the order of the listing: desc, asc  (optional)
    search = 'search_example' # str | Specify a search string  (optional)
    item_id = [56] # List[int] | Filter resource by their primary id  (optional)
    group_id = [56] # List[int] | Filter by group(s)  (optional)
    response_format = 'response_format_example' # str | How detialed the response should be  (optional)
    asset_type = 'asset_type_example' # str | Restrict the list of assets to only those with provided asset_types.  (optional)
    order_by = 'order_by_example' # str | Order by the provided argument name.  (optional)
    show_all = True # bool | Restrict the list of assets to only those with archive files if set to False  (optional)
    study_after = '2013-10-20' # date | Restrict the list of assets with study after this date (optional)
    study_before = '2013-10-20' # date | Restrict the list of assets with study before this date (optional)

    try:
        api_response = api_instance.api_assets_get_all_assets(limit=limit, offset=offset, order=order, search=search, item_id=item_id, group_id=group_id, response_format=response_format, asset_type=asset_type, order_by=order_by, show_all=show_all, study_after=study_after, study_before=study_before)
        print("The response of DefaultApi->api_assets_get_all_assets:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_get_all_assets: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Limit the number of returned roles to indicated value (default 50).  | [optional] [default to 50]
 **offset** | **int**| When set the query returns &#39;limit&#39; number of roles but skip &#39;offset&#39; number of roles at the beginning.  | [optional] [default to 0]
 **order** | **str**| Specify the order of the listing: desc, asc  | [optional] 
 **search** | **str**| Specify a search string  | [optional] 
 **item_id** | [**List[int]**](int.md)| Filter resource by their primary id  | [optional] 
 **group_id** | [**List[int]**](int.md)| Filter by group(s)  | [optional] 
 **response_format** | **str**| How detialed the response should be  | [optional] 
 **asset_type** | **str**| Restrict the list of assets to only those with provided asset_types.  | [optional] 
 **order_by** | **str**| Order by the provided argument name.  | [optional] 
 **show_all** | **bool**| Restrict the list of assets to only those with archive files if set to False  | [optional] 
 **study_after** | **date**| Restrict the list of assets with study after this date | [optional] 
 **study_before** | **date**| Restrict the list of assets with study before this date | [optional] 

### Return type

[**ApiAssetsGetAllAssets200Response**](ApiAssetsGetAllAssets200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get all resource |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_get_asset**
> ApiAssetsGetAllAssets200ResponseItemsInner api_assets_get_asset(asset_id)



Returns the current settings for the asset. 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.models.api_assets_get_all_assets200_response_items_inner import ApiAssetsGetAllAssets200ResponseItemsInner
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset

    try:
        api_response = api_instance.api_assets_get_asset(asset_id)
        print("The response of DefaultApi->api_assets_get_asset:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_get_asset: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 

### Return type

[**ApiAssetsGetAllAssets200ResponseItemsInner**](ApiAssetsGetAllAssets200ResponseItemsInner.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | details of a role |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_get_asset_archive**
> bytearray api_assets_get_asset_archive(asset_id, to_save=to_save)



Get asset archive

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset
    to_save = True # bool | Set true if you want to save the file instead of getting to content of it  (optional)

    try:
        api_response = api_instance.api_assets_get_asset_archive(asset_id, to_save=to_save)
        print("The response of DefaultApi->api_assets_get_asset_archive:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_get_asset_archive: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 
 **to_save** | **bool**| Set true if you want to save the file instead of getting to content of it  | [optional] 

### Return type

**bytearray**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the asset archive |  * Content-Disposition -  <br>  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_get_asset_report_aggregated_csv**
> bytearray api_assets_get_asset_report_aggregated_csv(algorithm_id=algorithm_id, job_id=job_id, group_id=group_id, patient_name=patient_name, patient_sex=patient_sex, minage=minage, maxage=maxage, job_after=job_after, job_before=job_before, status=status)



Get aggregated csv reports

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    algorithm_id = [56] # List[int] | Filter by algorithm(s)  (optional)
    job_id = [56] # List[int] | Restrict the aggregated csv to only jobs selected  (optional)
    group_id = [56] # List[int] | Filter by group(s)  (optional)
    patient_name = ['patient_name_example'] # List[str] | Filter by patient's name  (optional)
    patient_sex = ['patient_sex_example'] # List[str] | Filter by patient's gender  (optional)
    minage = 56 # int | Filter by age  (optional)
    maxage = 56 # int | Filter by age  (optional)
    job_after = '2013-10-20' # date | Restrict the list of jobs created after this date (optional)
    job_before = '2013-10-20' # date | Restrict the list of jobs created before this date (optional)
    status = ['status_example'] # List[str] | Restrict the list of jobs to only those with provided statuses.  (optional)

    try:
        api_response = api_instance.api_assets_get_asset_report_aggregated_csv(algorithm_id=algorithm_id, job_id=job_id, group_id=group_id, patient_name=patient_name, patient_sex=patient_sex, minage=minage, maxage=maxage, job_after=job_after, job_before=job_before, status=status)
        print("The response of DefaultApi->api_assets_get_asset_report_aggregated_csv:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_get_asset_report_aggregated_csv: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **algorithm_id** | [**List[int]**](int.md)| Filter by algorithm(s)  | [optional] 
 **job_id** | [**List[int]**](int.md)| Restrict the aggregated csv to only jobs selected  | [optional] 
 **group_id** | [**List[int]**](int.md)| Filter by group(s)  | [optional] 
 **patient_name** | [**List[str]**](str.md)| Filter by patient&#39;s name  | [optional] 
 **patient_sex** | [**List[str]**](str.md)| Filter by patient&#39;s gender  | [optional] 
 **minage** | **int**| Filter by age  | [optional] 
 **maxage** | **int**| Filter by age  | [optional] 
 **job_after** | **date**| Restrict the list of jobs created after this date | [optional] 
 **job_before** | **date**| Restrict the list of jobs created before this date | [optional] 
 **status** | [**List[str]**](str.md)| Restrict the list of jobs to only those with provided statuses.  | [optional] 

### Return type

**bytearray**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/csv

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the csv report |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_get_asset_report_aggregated_pdf**
> str api_assets_get_asset_report_aggregated_pdf(algorithm_id=algorithm_id, job_id=job_id, group_id=group_id, patient_name=patient_name, patient_sex=patient_sex, minage=minage, maxage=maxage, job_after=job_after, job_before=job_before, status=status)



Get aggregated pdf reports

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    algorithm_id = [56] # List[int] | Filter by algorithm(s)  (optional)
    job_id = [56] # List[int] | Restrict the aggregated csv to only jobs selected  (optional)
    group_id = [56] # List[int] | Filter by group(s)  (optional)
    patient_name = ['patient_name_example'] # List[str] | Filter by patient's name  (optional)
    patient_sex = ['patient_sex_example'] # List[str] | Filter by patient's gender  (optional)
    minage = 56 # int | Filter by age  (optional)
    maxage = 56 # int | Filter by age  (optional)
    job_after = '2013-10-20' # date | Restrict the list of jobs created after this date (optional)
    job_before = '2013-10-20' # date | Restrict the list of jobs created before this date (optional)
    status = ['status_example'] # List[str] | Restrict the list of jobs to only those with provided statuses.  (optional)

    try:
        api_response = api_instance.api_assets_get_asset_report_aggregated_pdf(algorithm_id=algorithm_id, job_id=job_id, group_id=group_id, patient_name=patient_name, patient_sex=patient_sex, minage=minage, maxage=maxage, job_after=job_after, job_before=job_before, status=status)
        print("The response of DefaultApi->api_assets_get_asset_report_aggregated_pdf:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_get_asset_report_aggregated_pdf: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **algorithm_id** | [**List[int]**](int.md)| Filter by algorithm(s)  | [optional] 
 **job_id** | [**List[int]**](int.md)| Restrict the aggregated csv to only jobs selected  | [optional] 
 **group_id** | [**List[int]**](int.md)| Filter by group(s)  | [optional] 
 **patient_name** | [**List[str]**](str.md)| Filter by patient&#39;s name  | [optional] 
 **patient_sex** | [**List[str]**](str.md)| Filter by patient&#39;s gender  | [optional] 
 **minage** | **int**| Filter by age  | [optional] 
 **maxage** | **int**| Filter by age  | [optional] 
 **job_after** | **date**| Restrict the list of jobs created after this date | [optional] 
 **job_before** | **date**| Restrict the list of jobs created before this date | [optional] 
 **status** | [**List[str]**](str.md)| Restrict the list of jobs to only those with provided statuses.  | [optional] 

### Return type

**str**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-zip

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A zip file containing the pdf reports for the selected jobs  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_get_asset_report_csv**
> bytearray api_assets_get_asset_report_csv(asset_id, to_save=to_save)



Get csv report

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset
    to_save = True # bool | Set true if you want to save the file instead of getting to content of it  (optional)

    try:
        api_response = api_instance.api_assets_get_asset_report_csv(asset_id, to_save=to_save)
        print("The response of DefaultApi->api_assets_get_asset_report_csv:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_get_asset_report_csv: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 
 **to_save** | **bool**| Set true if you want to save the file instead of getting to content of it  | [optional] 

### Return type

**bytearray**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/csv, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the csv report |  * Content-Disposition -  <br>  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_get_asset_report_pdf**
> bytearray api_assets_get_asset_report_pdf(asset_id, to_save=to_save)



Get pdf report

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset
    to_save = True # bool | Set true if you want to save the file instead of getting to content of it  (optional)

    try:
        api_response = api_instance.api_assets_get_asset_report_pdf(asset_id, to_save=to_save)
        print("The response of DefaultApi->api_assets_get_asset_report_pdf:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_get_asset_report_pdf: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 
 **to_save** | **bool**| Set true if you want to save the file instead of getting to content of it  | [optional] 

### Return type

**bytearray**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pdf, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the pdf report |  * Content-Disposition -  <br>  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_get_download_url**
> ApiAssetsGetUploadUrl200Response api_assets_get_download_url(asset_id)



Get presigned url for download to S3

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.models.api_assets_get_upload_url200_response import ApiAssetsGetUploadUrl200Response
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset

    try:
        api_response = api_instance.api_assets_get_download_url(asset_id)
        print("The response of DefaultApi->api_assets_get_download_url:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_get_download_url: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 

### Return type

[**ApiAssetsGetUploadUrl200Response**](ApiAssetsGetUploadUrl200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns url object |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_get_upload_url**
> ApiAssetsGetUploadUrl200Response api_assets_get_upload_url(asset_id, filename, group_id)



Get presigned url for upload to S3 or to ICCP. If Content-Md5 is set in header, md5 checksum is checked during upload. Content-Md5 header must match between url generation and upload! MD5 checksum should be Base64 encoded string. In the header both Content-Md5 and X-Amz-Meta-Md5 should be set! 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.models.api_assets_get_upload_url200_response import ApiAssetsGetUploadUrl200Response
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset
    filename = 'filename_example' # str | Assets filename to generate presigned url 
    group_id = 56 # int | Filter by group 

    try:
        api_response = api_instance.api_assets_get_upload_url(asset_id, filename, group_id)
        print("The response of DefaultApi->api_assets_get_upload_url:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_get_upload_url: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 
 **filename** | **str**| Assets filename to generate presigned url  | 
 **group_id** | **int**| Filter by group  | 

### Return type

[**ApiAssetsGetUploadUrl200Response**](ApiAssetsGetUploadUrl200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns url object |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_post_asset**
> ApiAssetsPostAsset200Response api_assets_post_asset(api_assets_post_asset_request)



Create a new asset description; this will not yet contain the archive, the archive will be uploaded after this is created 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.models.api_assets_post_asset200_response import ApiAssetsPostAsset200Response
from core_assets.models.api_assets_post_asset_request import ApiAssetsPostAssetRequest
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    api_assets_post_asset_request = core_assets.ApiAssetsPostAssetRequest() # ApiAssetsPostAssetRequest | Initial properties of the asset. 

    try:
        api_response = api_instance.api_assets_post_asset(api_assets_post_asset_request)
        print("The response of DefaultApi->api_assets_post_asset:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_post_asset: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_assets_post_asset_request** | [**ApiAssetsPostAssetRequest**](ApiAssetsPostAssetRequest.md)| Initial properties of the asset.  | 

### Return type

[**ApiAssetsPostAsset200Response**](ApiAssetsPostAsset200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_put_asset_archive**
> api_assets_put_asset_archive(asset_id, archive)



Add asset archive

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset
    archive = None # bytearray | The asset archive 

    try:
        api_instance.api_assets_put_asset_archive(asset_id, archive)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_put_asset_archive: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 
 **archive** | **bytearray**| The asset archive  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**400** | 400: Bad request, operation not processed  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_put_asset_dicom**
> api_assets_put_asset_dicom(asset_id, api_assets_put_asset_dicom_request)



Uploads a single dicom file

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.models.api_assets_put_asset_dicom_request import ApiAssetsPutAssetDicomRequest
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset
    api_assets_put_asset_dicom_request = core_assets.ApiAssetsPutAssetDicomRequest() # ApiAssetsPutAssetDicomRequest | 

    try:
        api_instance.api_assets_put_asset_dicom(asset_id, api_assets_put_asset_dicom_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_put_asset_dicom: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 
 **api_assets_put_asset_dicom_request** | [**ApiAssetsPutAssetDicomRequest**](ApiAssetsPutAssetDicomRequest.md)|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/dicom
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_put_asset_report_csv**
> api_assets_put_asset_report_csv(asset_id, csv_file)



Upload csv

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset
    csv_file = None # bytearray | csv file

    try:
        api_instance.api_assets_put_asset_report_csv(asset_id, csv_file)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_put_asset_report_csv: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 
 **csv_file** | **bytearray**| csv file | 

### Return type

void (empty response body)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**400** | 400: Bad request, operation not processed  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_put_asset_report_pdf**
> api_assets_put_asset_report_pdf(asset_id, pdf_file)



Upload pdf

### Example

* OAuth Authentication (oauth2_client):
```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset
    pdf_file = None # bytearray | pdf file

    try:
        api_instance.api_assets_put_asset_report_pdf(asset_id, pdf_file)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_put_asset_report_pdf: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 
 **pdf_file** | **bytearray**| pdf file | 

### Return type

void (empty response body)

### Authorization

[oauth2_client](../README.md#oauth2_client)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**400** | 400: Bad request, operation not processed  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_upload_by_token**
> api_assets_upload_by_token(asset_id, asset_token, to_save=to_save)



Uploads a file with a token An url in this form can be get with the {asset_id}/upload_url endpoint If Content-Md5 is set in header, md5 checksum is checked during upload. Content-Md5 header must match between url generation and upload! MD5 checksum should be Base64 encoded string. In the header both Content-Md5 and X-Amz-Meta-Md5 should be set! 

### Example

```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)


# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset
    asset_token = 'asset_token_example' # str | The download token which contains the description of the downloadable object in encrypted format 
    to_save = True # bool | Set true if you want to save the file instead of getting to content of it  (optional)

    try:
        api_instance.api_assets_upload_by_token(asset_id, asset_token, to_save=to_save)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_upload_by_token: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 
 **asset_token** | **str**| The download token which contains the description of the downloadable object in encrypted format  | 
 **to_save** | **bool**| Set true if you want to save the file instead of getting to content of it  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/octet-stream
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_assets_upload_done**
> api_assets_upload_done(asset_id)



Signal that asset upload is finished 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_assets
from core_assets.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/assets
# See configuration.py for a list of all supported configuration parameters.
configuration = core_assets.Configuration(
    host = "https://launchpad.imbio.com/api/v1/assets"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_assets.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_assets.DefaultApi(api_client)
    asset_id = 'asset_id_example' # str | The unique identifier of an asset

    try:
        api_instance.api_assets_upload_done(asset_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_assets_upload_done: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset_id** | **str**| The unique identifier of an asset | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

