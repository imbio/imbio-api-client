# ApiAssetsGetAllAssets200Response


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**num_of_all** | **int** | Number of all resource | [optional] 
**index** | **int** | Items returned from index | [optional] 
**items** | [**List[ApiAssetsGetAllAssets200ResponseItemsInner]**](ApiAssetsGetAllAssets200ResponseItemsInner.md) | The list of returned resource | [optional] 
**more** | **bool** | True if there is more than the requested amount of items  | [optional] 

## Example

```python
from core_assets.models.api_assets_get_all_assets200_response import ApiAssetsGetAllAssets200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiAssetsGetAllAssets200Response from a JSON string
api_assets_get_all_assets200_response_instance = ApiAssetsGetAllAssets200Response.from_json(json)
# print the JSON string representation of the object
print ApiAssetsGetAllAssets200Response.to_json()

# convert the object into a dict
api_assets_get_all_assets200_response_dict = api_assets_get_all_assets200_response_instance.to_dict()
# create an instance of ApiAssetsGetAllAssets200Response from a dict
api_assets_get_all_assets200_response_form_dict = api_assets_get_all_assets200_response.from_dict(api_assets_get_all_assets200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


