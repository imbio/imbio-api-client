# ApiAssetsGetAllAssets200ResponseItemsInner

Study properties parsed from the DICOM study that will be used by the UI/bridge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Asset Id | [optional] 
**name** | **str** | An optional name to the asset  | [optional] 
**asset_type** | **str** | The type of the asset  | [optional] 
**uploaded_at** | **str** | The time when the asset was uploaded  | [optional] 
**owner_id** | **int** | The id of the owner  | [optional] 
**group_id** | **int** | The id of the group whom the asset belongs to  | [optional] 
**study_id** | **int** | The id of the study whom the asset belongs to  | [optional] 
**study_date** | **str** | The date when the study was created  | [optional] 
**patient_name** | **str** | The name of the patient  | [optional] 
**accession_number** | **str** | The accession of the study  | [optional] 
**deidentified** | **bool** | The asset was deidentified  | [optional] 
**has_archive** | **bool** | The asset has an archive file  | [optional] 
**has_csv_report** | **bool** | The asset has csv report  | [optional] 
**has_pdf_report** | **bool** | The asset has pdf report  | [optional] 
**cleaned_up** | **bool** | The asset archives have been removed from the asset  | [optional] 
**series_options_log** | **object** | Option that the series was selected from | [optional] 
**deid_values** | **object** | Object containing deidentified dicom attributes and their values | [optional] 

## Example

```python
from core_assets.models.api_assets_get_all_assets200_response_items_inner import ApiAssetsGetAllAssets200ResponseItemsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiAssetsGetAllAssets200ResponseItemsInner from a JSON string
api_assets_get_all_assets200_response_items_inner_instance = ApiAssetsGetAllAssets200ResponseItemsInner.from_json(json)
# print the JSON string representation of the object
print ApiAssetsGetAllAssets200ResponseItemsInner.to_json()

# convert the object into a dict
api_assets_get_all_assets200_response_items_inner_dict = api_assets_get_all_assets200_response_items_inner_instance.to_dict()
# create an instance of ApiAssetsGetAllAssets200ResponseItemsInner from a dict
api_assets_get_all_assets200_response_items_inner_form_dict = api_assets_get_all_assets200_response_items_inner.from_dict(api_assets_get_all_assets200_response_items_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


