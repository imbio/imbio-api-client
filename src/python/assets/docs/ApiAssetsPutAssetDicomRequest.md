# ApiAssetsPutAssetDicomRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dicom** | **bytearray** | A single dicom file | 

## Example

```python
from core_assets.models.api_assets_put_asset_dicom_request import ApiAssetsPutAssetDicomRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiAssetsPutAssetDicomRequest from a JSON string
api_assets_put_asset_dicom_request_instance = ApiAssetsPutAssetDicomRequest.from_json(json)
# print the JSON string representation of the object
print ApiAssetsPutAssetDicomRequest.to_json()

# convert the object into a dict
api_assets_put_asset_dicom_request_dict = api_assets_put_asset_dicom_request_instance.to_dict()
# create an instance of ApiAssetsPutAssetDicomRequest from a dict
api_assets_put_asset_dicom_request_form_dict = api_assets_put_asset_dicom_request.from_dict(api_assets_put_asset_dicom_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


