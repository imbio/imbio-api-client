# coding: utf-8

"""
    IMBIO core api for profiles

    Methods for managing profiles 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


from __future__ import annotations
import pprint
import re  # noqa: F401
import json


from typing import Any, ClassVar, Dict, List, Optional
from pydantic import BaseModel, StrictBool, StrictInt, StrictStr
from pydantic import Field
from typing_extensions import Annotated
from core_profiles.models.api_profiles_get_all_profiles200_response_items_inner_group_workflows_inner import ApiProfilesGetAllProfiles200ResponseItemsInnerGroupWorkflowsInner
try:
    from typing import Self
except ImportError:
    from typing_extensions import Self

class ApiProfilesGetAllProfiles200ResponseItemsInnerGroup(BaseModel):
    """
    Nested group schema
    """ # noqa: E501
    id: Optional[Annotated[int, Field(le=999999999, strict=True, ge=0)]] = Field(default=None, description="The id of the group ")
    name: Optional[StrictStr] = Field(default=None, description="The name of the group ")
    description: Optional[StrictStr] = Field(default=None, description="The description of the group ")
    page_logo_url: Optional[StrictStr] = Field(default=None, description="The url of the group logo to show on the site ")
    report_logo_url: Optional[StrictStr] = Field(default=None, description="The url of the report logo to use in the reports ")
    review_workflow_enabled: Optional[StrictBool] = Field(default=None, description="Shows if the review workflow is enabled")
    qa_workflow_enabled: Optional[StrictBool] = Field(default=None, description="Shows if the qa review workflow is enabled")
    overread_workflow_enabled: Optional[StrictBool] = Field(default=None, description="Shows if the overread workflow is enabled")
    overread_request_enabled: Optional[StrictBool] = Field(default=None, description="Flag to decide whether overread request functionality is enabled for the group")
    seg_editor_enabled: Optional[StrictBool] = Field(default=None, description="Shows if the segmentation editor is enabled")
    deidentify: Optional[StrictStr] = Field(default=None, description="Flag for deidentify data before uploading")
    storage_time: Optional[StrictInt] = Field(default=None, description="How many days to store assets")
    research: Optional[StrictBool] = Field(default=None, description="shows if this the group runs algorithms for research")
    region: Optional[StrictStr] = Field(default=None, description="shows that the group is in what region")
    allow_notification_suppress: Optional[StrictBool] = Field(default=None, description="Disable notification filtering on user level.")
    notifications: Optional[List[StrictInt]] = Field(default=None, description="List of allowed notifications")
    workflows: Optional[List[ApiProfilesGetAllProfiles200ResponseItemsInnerGroupWorkflowsInner]] = Field(default=None, description="Some info of the algorithms belonging to group")
    en_upload_any_series: Optional[StrictBool] = Field(default=None, description="Lets the group to upload any series including invalide series")
    disable_series_preprocessing: Optional[StrictBool] = Field(default=None, description="Disable series preprocessing")
    __properties: ClassVar[List[str]] = ["id", "name", "description", "page_logo_url", "report_logo_url", "review_workflow_enabled", "qa_workflow_enabled", "overread_workflow_enabled", "overread_request_enabled", "seg_editor_enabled", "deidentify", "storage_time", "research", "region", "allow_notification_suppress", "notifications", "workflows", "en_upload_any_series", "disable_series_preprocessing"]

    model_config = {
        "populate_by_name": True,
        "validate_assignment": True
    }


    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.model_dump(by_alias=True))

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        # TODO: pydantic v2: use .model_dump_json(by_alias=True, exclude_unset=True) instead
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> Self:
        """Create an instance of ApiProfilesGetAllProfiles200ResponseItemsInnerGroup from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self) -> Dict[str, Any]:
        """Return the dictionary representation of the model using alias.

        This has the following differences from calling pydantic's
        `self.model_dump(by_alias=True)`:

        * `None` is only added to the output dict for nullable fields that
          were set at model initialization. Other fields with value `None`
          are ignored.
        """
        _dict = self.model_dump(
            by_alias=True,
            exclude={
            },
            exclude_none=True,
        )
        # override the default output from pydantic by calling `to_dict()` of each item in workflows (list)
        _items = []
        if self.workflows:
            for _item in self.workflows:
                if _item:
                    _items.append(_item.to_dict())
            _dict['workflows'] = _items
        return _dict

    @classmethod
    def from_dict(cls, obj: Dict) -> Self:
        """Create an instance of ApiProfilesGetAllProfiles200ResponseItemsInnerGroup from a dict"""
        if obj is None:
            return None

        if not isinstance(obj, dict):
            return cls.model_validate(obj)

        _obj = cls.model_validate({
            "id": obj.get("id"),
            "name": obj.get("name"),
            "description": obj.get("description"),
            "page_logo_url": obj.get("page_logo_url"),
            "report_logo_url": obj.get("report_logo_url"),
            "review_workflow_enabled": obj.get("review_workflow_enabled"),
            "qa_workflow_enabled": obj.get("qa_workflow_enabled"),
            "overread_workflow_enabled": obj.get("overread_workflow_enabled"),
            "overread_request_enabled": obj.get("overread_request_enabled"),
            "seg_editor_enabled": obj.get("seg_editor_enabled"),
            "deidentify": obj.get("deidentify"),
            "storage_time": obj.get("storage_time"),
            "research": obj.get("research"),
            "region": obj.get("region"),
            "allow_notification_suppress": obj.get("allow_notification_suppress"),
            "notifications": obj.get("notifications"),
            "workflows": [ApiProfilesGetAllProfiles200ResponseItemsInnerGroupWorkflowsInner.from_dict(_item) for _item in obj.get("workflows")] if obj.get("workflows") is not None else None,
            "en_upload_any_series": obj.get("en_upload_any_series"),
            "disable_series_preprocessing": obj.get("disable_series_preprocessing")
        })
        return _obj


