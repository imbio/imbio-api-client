# coding: utf-8

"""
    IMBIO core api for profiles

    Methods for managing profiles 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


from __future__ import annotations
import pprint
import re  # noqa: F401
import json


from typing import Any, ClassVar, Dict, List, Optional
from pydantic import BaseModel, StrictInt, StrictStr
from pydantic import Field
try:
    from typing import Self
except ImportError:
    from typing_extensions import Self

class ApiProfilesGetAllProfiles200ResponseItemsInnerAlgorithm(BaseModel):
    """
    Minimal information about algorithm
    """ # noqa: E501
    id: Optional[StrictInt] = Field(default=None, description="Algorithm Id ")
    name: Optional[StrictStr] = Field(default=None, description="The name of the algorithm ")
    version: Optional[StrictStr] = Field(default=None, description="The version of the algorithm ")
    validator_orthanc: Optional[StrictStr] = Field(default=None, description="Contains the validator template that will be run on the Study ")
    logo_url: Optional[StrictStr] = Field(default=None, description="The default url for an algorithm ")
    __properties: ClassVar[List[str]] = ["id", "name", "version", "validator_orthanc", "logo_url"]

    model_config = {
        "populate_by_name": True,
        "validate_assignment": True
    }


    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.model_dump(by_alias=True))

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        # TODO: pydantic v2: use .model_dump_json(by_alias=True, exclude_unset=True) instead
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> Self:
        """Create an instance of ApiProfilesGetAllProfiles200ResponseItemsInnerAlgorithm from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self) -> Dict[str, Any]:
        """Return the dictionary representation of the model using alias.

        This has the following differences from calling pydantic's
        `self.model_dump(by_alias=True)`:

        * `None` is only added to the output dict for nullable fields that
          were set at model initialization. Other fields with value `None`
          are ignored.
        """
        _dict = self.model_dump(
            by_alias=True,
            exclude={
            },
            exclude_none=True,
        )
        return _dict

    @classmethod
    def from_dict(cls, obj: Dict) -> Self:
        """Create an instance of ApiProfilesGetAllProfiles200ResponseItemsInnerAlgorithm from a dict"""
        if obj is None:
            return None

        if not isinstance(obj, dict):
            return cls.model_validate(obj)

        _obj = cls.model_validate({
            "id": obj.get("id"),
            "name": obj.get("name"),
            "version": obj.get("version"),
            "validator_orthanc": obj.get("validator_orthanc"),
            "logo_url": obj.get("logo_url")
        })
        return _obj


