# coding: utf-8

"""
    IMBIO core api for profiles

    Methods for managing profiles 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_profiles.models.api_profiles_get_all_profiles200_response_items_inner_owner import ApiProfilesGetAllProfiles200ResponseItemsInnerOwner

class TestApiProfilesGetAllProfiles200ResponseItemsInnerOwner(unittest.TestCase):
    """ApiProfilesGetAllProfiles200ResponseItemsInnerOwner unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiProfilesGetAllProfiles200ResponseItemsInnerOwner:
        """Test ApiProfilesGetAllProfiles200ResponseItemsInnerOwner
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiProfilesGetAllProfiles200ResponseItemsInnerOwner`
        """
        model = ApiProfilesGetAllProfiles200ResponseItemsInnerOwner()
        if include_optional:
            return ApiProfilesGetAllProfiles200ResponseItemsInnerOwner(
                id = 56,
                name = '',
                email = ''
            )
        else:
            return ApiProfilesGetAllProfiles200ResponseItemsInnerOwner(
        )
        """

    def testApiProfilesGetAllProfiles200ResponseItemsInnerOwner(self):
        """Test ApiProfilesGetAllProfiles200ResponseItemsInnerOwner"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
