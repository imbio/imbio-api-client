# coding: utf-8

"""
    IMBIO core api for profiles

    Methods for managing profiles 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_profiles.models.api_profiles_post_profile200_response import ApiProfilesPostProfile200Response

class TestApiProfilesPostProfile200Response(unittest.TestCase):
    """ApiProfilesPostProfile200Response unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiProfilesPostProfile200Response:
        """Test ApiProfilesPostProfile200Response
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiProfilesPostProfile200Response`
        """
        model = ApiProfilesPostProfile200Response()
        if include_optional:
            return ApiProfilesPostProfile200Response(
                id = 56
            )
        else:
            return ApiProfilesPostProfile200Response(
        )
        """

    def testApiProfilesPostProfile200Response(self):
        """Test ApiProfilesPostProfile200Response"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
