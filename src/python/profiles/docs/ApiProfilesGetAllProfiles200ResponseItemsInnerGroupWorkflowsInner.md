# ApiProfilesGetAllProfiles200ResponseItemsInnerGroupWorkflowsInner

Workflow object description

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from core_profiles.models.api_profiles_get_all_profiles200_response_items_inner_group_workflows_inner import ApiProfilesGetAllProfiles200ResponseItemsInnerGroupWorkflowsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiProfilesGetAllProfiles200ResponseItemsInnerGroupWorkflowsInner from a JSON string
api_profiles_get_all_profiles200_response_items_inner_group_workflows_inner_instance = ApiProfilesGetAllProfiles200ResponseItemsInnerGroupWorkflowsInner.from_json(json)
# print the JSON string representation of the object
print ApiProfilesGetAllProfiles200ResponseItemsInnerGroupWorkflowsInner.to_json()

# convert the object into a dict
api_profiles_get_all_profiles200_response_items_inner_group_workflows_inner_dict = api_profiles_get_all_profiles200_response_items_inner_group_workflows_inner_instance.to_dict()
# create an instance of ApiProfilesGetAllProfiles200ResponseItemsInnerGroupWorkflowsInner from a dict
api_profiles_get_all_profiles200_response_items_inner_group_workflows_inner_form_dict = api_profiles_get_all_profiles200_response_items_inner_group_workflows_inner.from_dict(api_profiles_get_all_profiles200_response_items_inner_group_workflows_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


