# ApiProfilesGetAllProfiles200ResponseItemsInnerAlgorithm

Minimal information about algorithm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Algorithm Id  | [optional] 
**name** | **str** | The name of the algorithm  | [optional] 
**version** | **str** | The version of the algorithm  | [optional] 
**validator_orthanc** | **str** | Contains the validator template that will be run on the Study  | [optional] 
**logo_url** | **str** | The default url for an algorithm  | [optional] 

## Example

```python
from core_profiles.models.api_profiles_get_all_profiles200_response_items_inner_algorithm import ApiProfilesGetAllProfiles200ResponseItemsInnerAlgorithm

# TODO update the JSON string below
json = "{}"
# create an instance of ApiProfilesGetAllProfiles200ResponseItemsInnerAlgorithm from a JSON string
api_profiles_get_all_profiles200_response_items_inner_algorithm_instance = ApiProfilesGetAllProfiles200ResponseItemsInnerAlgorithm.from_json(json)
# print the JSON string representation of the object
print ApiProfilesGetAllProfiles200ResponseItemsInnerAlgorithm.to_json()

# convert the object into a dict
api_profiles_get_all_profiles200_response_items_inner_algorithm_dict = api_profiles_get_all_profiles200_response_items_inner_algorithm_instance.to_dict()
# create an instance of ApiProfilesGetAllProfiles200ResponseItemsInnerAlgorithm from a dict
api_profiles_get_all_profiles200_response_items_inner_algorithm_form_dict = api_profiles_get_all_profiles200_response_items_inner_algorithm.from_dict(api_profiles_get_all_profiles200_response_items_inner_algorithm_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


