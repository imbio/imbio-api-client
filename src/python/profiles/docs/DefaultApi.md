# core_profiles.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/profiles*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_profiles_delete_profile**](DefaultApi.md#api_profiles_delete_profile) | **DELETE** /{profile_id} | 
[**api_profiles_get_all_profiles**](DefaultApi.md#api_profiles_get_all_profiles) | **GET** / | 
[**api_profiles_get_profile**](DefaultApi.md#api_profiles_get_profile) | **GET** /{profile_id} | 
[**api_profiles_post_profile**](DefaultApi.md#api_profiles_post_profile) | **POST** / | 
[**api_profiles_put_profile**](DefaultApi.md#api_profiles_put_profile) | **PUT** /{profile_id} | 


# **api_profiles_delete_profile**
> api_profiles_delete_profile(profile_id)



Deletes one profile. 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_profiles
from core_profiles.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/profiles
# See configuration.py for a list of all supported configuration parameters.
configuration = core_profiles.Configuration(
    host = "https://launchpad.imbio.com/api/v1/profiles"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_profiles.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_profiles.DefaultApi(api_client)
    profile_id = 'profile_id_example' # str | The unique identifier of a profile

    try:
        api_instance.api_profiles_delete_profile(profile_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_profiles_delete_profile: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_id** | **str**| The unique identifier of a profile | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_profiles_get_all_profiles**
> ApiProfilesGetAllProfiles200Response api_profiles_get_all_profiles(limit=limit, offset=offset, order=order, search=search, group_id=group_id, item_id=item_id, algorithm_id=algorithm_id, response_format=response_format, order_by=order_by, owner_id=owner_id, own=own, active=active)



Returns all profiles

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_profiles
from core_profiles.models.api_profiles_get_all_profiles200_response import ApiProfilesGetAllProfiles200Response
from core_profiles.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/profiles
# See configuration.py for a list of all supported configuration parameters.
configuration = core_profiles.Configuration(
    host = "https://launchpad.imbio.com/api/v1/profiles"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_profiles.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_profiles.DefaultApi(api_client)
    limit = 50 # int | Limit the number of returned roles to indicated value (default 50).  (optional) (default to 50)
    offset = 0 # int | When set the query returns 'limit' number of roles but skip 'offset' number of roles at the beginning.  (optional) (default to 0)
    order = 'order_example' # str | Specify the order of the listing: desc, asc  (optional)
    search = 'search_example' # str | Specify a search string  (optional)
    group_id = [56] # List[int] | Filter by group(s)  (optional)
    item_id = [56] # List[int] | Filter resource by their primary id  (optional)
    algorithm_id = [56] # List[int] | Filter by algorithm(s)  (optional)
    response_format = 'response_format_example' # str | How detialed the response should be  (optional)
    order_by = 'order_by_example' # str | Order by the provided argument name.  (optional)
    owner_id = [56] # List[int] | Restrict the list of profiles belonging to a specific owner  (optional)
    own = True # bool | Restrict the list of profiles belonging to the user making the query  (optional)
    active = True # bool | Filter for active or inactive profiles (optional)

    try:
        api_response = api_instance.api_profiles_get_all_profiles(limit=limit, offset=offset, order=order, search=search, group_id=group_id, item_id=item_id, algorithm_id=algorithm_id, response_format=response_format, order_by=order_by, owner_id=owner_id, own=own, active=active)
        print("The response of DefaultApi->api_profiles_get_all_profiles:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_profiles_get_all_profiles: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Limit the number of returned roles to indicated value (default 50).  | [optional] [default to 50]
 **offset** | **int**| When set the query returns &#39;limit&#39; number of roles but skip &#39;offset&#39; number of roles at the beginning.  | [optional] [default to 0]
 **order** | **str**| Specify the order of the listing: desc, asc  | [optional] 
 **search** | **str**| Specify a search string  | [optional] 
 **group_id** | [**List[int]**](int.md)| Filter by group(s)  | [optional] 
 **item_id** | [**List[int]**](int.md)| Filter resource by their primary id  | [optional] 
 **algorithm_id** | [**List[int]**](int.md)| Filter by algorithm(s)  | [optional] 
 **response_format** | **str**| How detialed the response should be  | [optional] 
 **order_by** | **str**| Order by the provided argument name.  | [optional] 
 **owner_id** | [**List[int]**](int.md)| Restrict the list of profiles belonging to a specific owner  | [optional] 
 **own** | **bool**| Restrict the list of profiles belonging to the user making the query  | [optional] 
 **active** | **bool**| Filter for active or inactive profiles | [optional] 

### Return type

[**ApiProfilesGetAllProfiles200Response**](ApiProfilesGetAllProfiles200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a list of profiles. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_profiles_get_profile**
> ApiProfilesGetAllProfiles200ResponseItemsInner api_profiles_get_profile(profile_id)



Returns the current settings for the profile. 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_profiles
from core_profiles.models.api_profiles_get_all_profiles200_response_items_inner import ApiProfilesGetAllProfiles200ResponseItemsInner
from core_profiles.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/profiles
# See configuration.py for a list of all supported configuration parameters.
configuration = core_profiles.Configuration(
    host = "https://launchpad.imbio.com/api/v1/profiles"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_profiles.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_profiles.DefaultApi(api_client)
    profile_id = 'profile_id_example' # str | The unique identifier of a profile

    try:
        api_response = api_instance.api_profiles_get_profile(profile_id)
        print("The response of DefaultApi->api_profiles_get_profile:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_profiles_get_profile: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_id** | **str**| The unique identifier of a profile | 

### Return type

[**ApiProfilesGetAllProfiles200ResponseItemsInner**](ApiProfilesGetAllProfiles200ResponseItemsInner.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The settings for the profile. |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_profiles_post_profile**
> ApiProfilesPostProfile200Response api_profiles_post_profile(api_profiles_post_profile_request)



Create a new profile

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_profiles
from core_profiles.models.api_profiles_post_profile200_response import ApiProfilesPostProfile200Response
from core_profiles.models.api_profiles_post_profile_request import ApiProfilesPostProfileRequest
from core_profiles.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/profiles
# See configuration.py for a list of all supported configuration parameters.
configuration = core_profiles.Configuration(
    host = "https://launchpad.imbio.com/api/v1/profiles"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_profiles.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_profiles.DefaultApi(api_client)
    api_profiles_post_profile_request = core_profiles.ApiProfilesPostProfileRequest() # ApiProfilesPostProfileRequest | Initial properties of the profile. 

    try:
        api_response = api_instance.api_profiles_post_profile(api_profiles_post_profile_request)
        print("The response of DefaultApi->api_profiles_post_profile:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_profiles_post_profile: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_profiles_post_profile_request** | [**ApiProfilesPostProfileRequest**](ApiProfilesPostProfileRequest.md)| Initial properties of the profile.  | 

### Return type

[**ApiProfilesPostProfile200Response**](ApiProfilesPostProfile200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_profiles_put_profile**
> api_profiles_put_profile(profile_id, api_profiles_put_profile_request)



Updates the current profile settings 

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_profiles
from core_profiles.models.api_profiles_put_profile_request import ApiProfilesPutProfileRequest
from core_profiles.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/profiles
# See configuration.py for a list of all supported configuration parameters.
configuration = core_profiles.Configuration(
    host = "https://launchpad.imbio.com/api/v1/profiles"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_profiles.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_profiles.DefaultApi(api_client)
    profile_id = 'profile_id_example' # str | The unique identifier of a profile
    api_profiles_put_profile_request = core_profiles.ApiProfilesPutProfileRequest() # ApiProfilesPutProfileRequest | Properties to update in the profile. 

    try:
        api_instance.api_profiles_put_profile(profile_id, api_profiles_put_profile_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_profiles_put_profile: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_id** | **str**| The unique identifier of a profile | 
 **api_profiles_put_profile_request** | [**ApiProfilesPutProfileRequest**](ApiProfilesPutProfileRequest.md)| Properties to update in the profile.  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

