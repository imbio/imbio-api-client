# ApiProfilesGetAllProfiles200Response

The paginated response for a get all profiles request

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**num_of_all** | **int** | How many profiles there are currently  | [optional] 
**index** | **int** | Profiles are returned from this index  | [optional] 
**items** | [**List[ApiProfilesGetAllProfiles200ResponseItemsInner]**](ApiProfilesGetAllProfiles200ResponseItemsInner.md) | The list of profiles returned from in this request  | [optional] 

## Example

```python
from core_profiles.models.api_profiles_get_all_profiles200_response import ApiProfilesGetAllProfiles200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiProfilesGetAllProfiles200Response from a JSON string
api_profiles_get_all_profiles200_response_instance = ApiProfilesGetAllProfiles200Response.from_json(json)
# print the JSON string representation of the object
print ApiProfilesGetAllProfiles200Response.to_json()

# convert the object into a dict
api_profiles_get_all_profiles200_response_dict = api_profiles_get_all_profiles200_response_instance.to_dict()
# create an instance of ApiProfilesGetAllProfiles200Response from a dict
api_profiles_get_all_profiles200_response_form_dict = api_profiles_get_all_profiles200_response.from_dict(api_profiles_get_all_profiles200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


