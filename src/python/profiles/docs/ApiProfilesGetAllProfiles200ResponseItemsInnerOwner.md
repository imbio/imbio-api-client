# ApiProfilesGetAllProfiles200ResponseItemsInnerOwner

Some info of a user

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of a user | [optional] 
**name** | **str** | name of a user or part of email | [optional] 
**email** | **str** | email of a user | [optional] 

## Example

```python
from core_profiles.models.api_profiles_get_all_profiles200_response_items_inner_owner import ApiProfilesGetAllProfiles200ResponseItemsInnerOwner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiProfilesGetAllProfiles200ResponseItemsInnerOwner from a JSON string
api_profiles_get_all_profiles200_response_items_inner_owner_instance = ApiProfilesGetAllProfiles200ResponseItemsInnerOwner.from_json(json)
# print the JSON string representation of the object
print ApiProfilesGetAllProfiles200ResponseItemsInnerOwner.to_json()

# convert the object into a dict
api_profiles_get_all_profiles200_response_items_inner_owner_dict = api_profiles_get_all_profiles200_response_items_inner_owner_instance.to_dict()
# create an instance of ApiProfilesGetAllProfiles200ResponseItemsInnerOwner from a dict
api_profiles_get_all_profiles200_response_items_inner_owner_form_dict = api_profiles_get_all_profiles200_response_items_inner_owner.from_dict(api_profiles_get_all_profiles200_response_items_inner_owner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


