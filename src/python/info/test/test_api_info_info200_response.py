# coding: utf-8

"""
    IMBIO core api for info

    Methods for getting info about core api 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_info.models.api_info_info200_response import ApiInfoInfo200Response

class TestApiInfoInfo200Response(unittest.TestCase):
    """ApiInfoInfo200Response unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiInfoInfo200Response:
        """Test ApiInfoInfo200Response
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiInfoInfo200Response`
        """
        model = ApiInfoInfo200Response()
        if include_optional:
            return ApiInfoInfo200Response(
                app_title = '',
                mission_control = True,
                support_url = '',
                user_guide_url = '',
                release_notes_url = '',
                privacy_policy_url = '',
                language_url = '',
                version = core_info.models.api_info_info_200_response_version.api_info_info_200_response_version(
                    app_date = '', 
                    app_version = '', 
                    core_version = '', )
            )
        else:
            return ApiInfoInfo200Response(
        )
        """

    def testApiInfoInfo200Response(self):
        """Test ApiInfoInfo200Response"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
