# ApiInfoInfo200ResponseVersion

All version related information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_date** | **str** | The release date of the app  | [optional] 
**app_version** | **str** | The version of the application  | [optional] 
**core_version** | **str** | The version of the core service  | [optional] 

## Example

```python
from core_info.models.api_info_info200_response_version import ApiInfoInfo200ResponseVersion

# TODO update the JSON string below
json = "{}"
# create an instance of ApiInfoInfo200ResponseVersion from a JSON string
api_info_info200_response_version_instance = ApiInfoInfo200ResponseVersion.from_json(json)
# print the JSON string representation of the object
print ApiInfoInfo200ResponseVersion.to_json()

# convert the object into a dict
api_info_info200_response_version_dict = api_info_info200_response_version_instance.to_dict()
# create an instance of ApiInfoInfo200ResponseVersion from a dict
api_info_info200_response_version_form_dict = api_info_info200_response_version.from_dict(api_info_info200_response_version_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


