# ApiInfoInfo200Response

Some base information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_title** | **str** | The HTML title for the application | [optional] 
**mission_control** | **bool** | Shows if deployment is MC or not | [optional] 
**support_url** | **str** | Link to support page | [optional] 
**user_guide_url** | **str** | Link to user guide page | [optional] 
**release_notes_url** | **str** | Link to release notes | [optional] 
**privacy_policy_url** | **str** | Link to privacy policy | [optional] 
**language_url** | **str** | Link to the language files | [optional] 
**version** | [**ApiInfoInfo200ResponseVersion**](ApiInfoInfo200ResponseVersion.md) |  | [optional] 

## Example

```python
from core_info.models.api_info_info200_response import ApiInfoInfo200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiInfoInfo200Response from a JSON string
api_info_info200_response_instance = ApiInfoInfo200Response.from_json(json)
# print the JSON string representation of the object
print ApiInfoInfo200Response.to_json()

# convert the object into a dict
api_info_info200_response_dict = api_info_info200_response_instance.to_dict()
# create an instance of ApiInfoInfo200Response from a dict
api_info_info200_response_form_dict = api_info_info200_response.from_dict(api_info_info200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


