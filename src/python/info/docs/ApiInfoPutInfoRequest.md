# ApiInfoPutInfoRequest

Some base information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_title** | **str** | The HTML title for the application | [optional] 
**support_url** | **str** | Link to support page | [optional] 
**user_guide_url** | **str** | Link to user guide page | [optional] 
**release_notes_url** | **str** | Link to release notes | [optional] 
**privacy_policy_url** | **str** | Link to privacy policy | [optional] 

## Example

```python
from core_info.models.api_info_put_info_request import ApiInfoPutInfoRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiInfoPutInfoRequest from a JSON string
api_info_put_info_request_instance = ApiInfoPutInfoRequest.from_json(json)
# print the JSON string representation of the object
print ApiInfoPutInfoRequest.to_json()

# convert the object into a dict
api_info_put_info_request_dict = api_info_put_info_request_instance.to_dict()
# create an instance of ApiInfoPutInfoRequest from a dict
api_info_put_info_request_form_dict = api_info_put_info_request.from_dict(api_info_put_info_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


