# core_info.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/info*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_info_info**](DefaultApi.md#api_info_info) | **GET** / | 
[**api_info_put_info**](DefaultApi.md#api_info_put_info) | **PUT** / | 
[**api_info_version**](DefaultApi.md#api_info_version) | **GET** /version | 


# **api_info_info**
> ApiInfoInfo200Response api_info_info()



Returns some base configuration info

### Example

```python
import time
import os
import core_info
from core_info.models.api_info_info200_response import ApiInfoInfo200Response
from core_info.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/info
# See configuration.py for a list of all supported configuration parameters.
configuration = core_info.Configuration(
    host = "https://launchpad.imbio.com/api/v1/info"
)


# Enter a context with an instance of the API client
with core_info.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_info.DefaultApi(api_client)

    try:
        api_response = api_instance.api_info_info()
        print("The response of DefaultApi->api_info_info:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_info_info: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiInfoInfo200Response**](ApiInfoInfo200Response.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns some base configuration and version |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_info_put_info**
> api_info_put_info(api_info_put_info_request)



update some base configuration

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_info
from core_info.models.api_info_put_info_request import ApiInfoPutInfoRequest
from core_info.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/info
# See configuration.py for a list of all supported configuration parameters.
configuration = core_info.Configuration(
    host = "https://launchpad.imbio.com/api/v1/info"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_info.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_info.DefaultApi(api_client)
    api_info_put_info_request = core_info.ApiInfoPutInfoRequest() # ApiInfoPutInfoRequest | Information to update 

    try:
        api_instance.api_info_put_info(api_info_put_info_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_info_put_info: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_info_put_info_request** | [**ApiInfoPutInfoRequest**](ApiInfoPutInfoRequest.md)| Information to update  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_info_version**
> ApiInfoInfo200ResponseVersion api_info_version()



Returns the version of the core api

### Example

```python
import time
import os
import core_info
from core_info.models.api_info_info200_response_version import ApiInfoInfo200ResponseVersion
from core_info.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/info
# See configuration.py for a list of all supported configuration parameters.
configuration = core_info.Configuration(
    host = "https://launchpad.imbio.com/api/v1/info"
)


# Enter a context with an instance of the API client
with core_info.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_info.DefaultApi(api_client)

    try:
        api_response = api_instance.api_info_version()
        print("The response of DefaultApi->api_info_version:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_info_version: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiInfoInfo200ResponseVersion**](ApiInfoInfo200ResponseVersion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the version. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

