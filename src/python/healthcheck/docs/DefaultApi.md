# core_healthcheck.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/healthcheck*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_healthcheck_healthcheck**](DefaultApi.md#api_healthcheck_healthcheck) | **GET** / | 


# **api_healthcheck_healthcheck**
> str api_healthcheck_healthcheck()



Healthcheck endpoint

### Example

```python
import time
import os
import core_healthcheck
from core_healthcheck.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/healthcheck
# See configuration.py for a list of all supported configuration parameters.
configuration = core_healthcheck.Configuration(
    host = "https://launchpad.imbio.com/api/v1/healthcheck"
)


# Enter a context with an instance of the API client
with core_healthcheck.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_healthcheck.DefaultApi(api_client)

    try:
        api_response = api_instance.api_healthcheck_healthcheck()
        print("The response of DefaultApi->api_healthcheck_healthcheck:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_healthcheck_healthcheck: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: plain/text

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns 200 and healthy massage. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

