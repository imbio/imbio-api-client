# core_login.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/login*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_login_email**](DefaultApi.md#api_login_email) | **POST** /email | Get an access token via email, password pair (should be used only by first party apps)
[**api_login_logout**](DefaultApi.md#api_login_logout) | **POST** /logout | logs out user


# **api_login_email**
> ApiLoginEmail200Response api_login_email(api_login_email_request)

Get an access token via email, password pair (should be used only by first party apps)

### Example

```python
import time
import os
import core_login
from core_login.models.api_login_email200_response import ApiLoginEmail200Response
from core_login.models.api_login_email_request import ApiLoginEmailRequest
from core_login.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/login
# See configuration.py for a list of all supported configuration parameters.
configuration = core_login.Configuration(
    host = "https://launchpad.imbio.com/api/v1/login"
)


# Enter a context with an instance of the API client
with core_login.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_login.DefaultApi(api_client)
    api_login_email_request = core_login.ApiLoginEmailRequest() # ApiLoginEmailRequest | Login via email password 

    try:
        # Get an access token via email, password pair (should be used only by first party apps)
        api_response = api_instance.api_login_email(api_login_email_request)
        print("The response of DefaultApi->api_login_email:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_login_email: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_login_email_request** | [**ApiLoginEmailRequest**](ApiLoginEmailRequest.md)| Login via email password  | 

### Return type

[**ApiLoginEmail200Response**](ApiLoginEmail200Response.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Accesstoken that can be used to access restricted resources |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_login_logout**
> api_login_logout()

logs out user

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_login
from core_login.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/login
# See configuration.py for a list of all supported configuration parameters.
configuration = core_login.Configuration(
    host = "https://launchpad.imbio.com/api/v1/login"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_login.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_login.DefaultApi(api_client)

    try:
        # logs out user
        api_instance.api_login_logout()
    except Exception as e:
        print("Exception when calling DefaultApi->api_login_logout: %s\n" % e)
```



### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

