# ApiLoginEmail200Response

Token and permissions info after a successul login

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **str** | User&#39;s email address  | [optional] 
**access_token** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**force_password_change** | **bool** | If this flag is set then the only allowed action will be changing the password  | [optional] 
**main_group** | [**ApiLoginEmail200ResponseMainGroup**](ApiLoginEmail200ResponseMainGroup.md) |  | [optional] 
**theme** | **str** |  | [optional] 
**logo_url** | **str** |  | [optional] 
**tc_url** | **str** | Url for the terms and conditions | [optional] 
**rights** | [**List[ApiLoginEmail200ResponseRightsInner]**](ApiLoginEmail200ResponseRightsInner.md) | Permissions info | [optional] 
**profile** | [**ApiLoginEmail200ResponseProfile**](ApiLoginEmail200ResponseProfile.md) |  | [optional] 
**post_login_todo** | [**List[ApiLoginEmail200ResponsePostLoginTodoInner]**](ApiLoginEmail200ResponsePostLoginTodoInner.md) |  | [optional] 

## Example

```python
from core_login.models.api_login_email200_response import ApiLoginEmail200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiLoginEmail200Response from a JSON string
api_login_email200_response_instance = ApiLoginEmail200Response.from_json(json)
# print the JSON string representation of the object
print ApiLoginEmail200Response.to_json()

# convert the object into a dict
api_login_email200_response_dict = api_login_email200_response_instance.to_dict()
# create an instance of ApiLoginEmail200Response from a dict
api_login_email200_response_form_dict = api_login_email200_response.from_dict(api_login_email200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


