# ApiLoginEmail200ResponseProfile

User profile description

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notifications** | [**List[ApiLoginEmail200ResponseProfileNotificationsInner]**](ApiLoginEmail200ResponseProfileNotificationsInner.md) | The list of notifications | [optional] 
**language** | **str** | Preferred language by the user | [optional] 

## Example

```python
from core_login.models.api_login_email200_response_profile import ApiLoginEmail200ResponseProfile

# TODO update the JSON string below
json = "{}"
# create an instance of ApiLoginEmail200ResponseProfile from a JSON string
api_login_email200_response_profile_instance = ApiLoginEmail200ResponseProfile.from_json(json)
# print the JSON string representation of the object
print ApiLoginEmail200ResponseProfile.to_json()

# convert the object into a dict
api_login_email200_response_profile_dict = api_login_email200_response_profile_instance.to_dict()
# create an instance of ApiLoginEmail200ResponseProfile from a dict
api_login_email200_response_profile_form_dict = api_login_email200_response_profile.from_dict(api_login_email200_response_profile_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


