# ApiLoginEmail200ResponsePostLoginTodoInnerTitle


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **str** | The todo title | [optional] 
**tid** | **str** | The translation id | [optional] 
**tparams** | **object** | An object holding the parameters | [optional] 

## Example

```python
from core_login.models.api_login_email200_response_post_login_todo_inner_title import ApiLoginEmail200ResponsePostLoginTodoInnerTitle

# TODO update the JSON string below
json = "{}"
# create an instance of ApiLoginEmail200ResponsePostLoginTodoInnerTitle from a JSON string
api_login_email200_response_post_login_todo_inner_title_instance = ApiLoginEmail200ResponsePostLoginTodoInnerTitle.from_json(json)
# print the JSON string representation of the object
print ApiLoginEmail200ResponsePostLoginTodoInnerTitle.to_json()

# convert the object into a dict
api_login_email200_response_post_login_todo_inner_title_dict = api_login_email200_response_post_login_todo_inner_title_instance.to_dict()
# create an instance of ApiLoginEmail200ResponsePostLoginTodoInnerTitle from a dict
api_login_email200_response_post_login_todo_inner_title_form_dict = api_login_email200_response_post_login_todo_inner_title.from_dict(api_login_email200_response_post_login_todo_inner_title_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


