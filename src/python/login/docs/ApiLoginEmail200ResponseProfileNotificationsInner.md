# ApiLoginEmail200ResponseProfileNotificationsInner

All notification related information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The notification id  | [optional] 
**type** | **str** | The notification type  | [optional] 
**name** | **str** | The notification name  | [optional] 
**description** | **str** | The notification description  | [optional] 
**notification_group** | **str** | The notification group  | [optional] 
**template** | **str** | The notification template  | [optional] 
**allow_suppress_by** | **List[int]** |  | [optional] 
**forced_by** | **List[int]** |  | [optional] 
**enabled** | **bool** |  | [optional] 

## Example

```python
from core_login.models.api_login_email200_response_profile_notifications_inner import ApiLoginEmail200ResponseProfileNotificationsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiLoginEmail200ResponseProfileNotificationsInner from a JSON string
api_login_email200_response_profile_notifications_inner_instance = ApiLoginEmail200ResponseProfileNotificationsInner.from_json(json)
# print the JSON string representation of the object
print ApiLoginEmail200ResponseProfileNotificationsInner.to_json()

# convert the object into a dict
api_login_email200_response_profile_notifications_inner_dict = api_login_email200_response_profile_notifications_inner_instance.to_dict()
# create an instance of ApiLoginEmail200ResponseProfileNotificationsInner from a dict
api_login_email200_response_profile_notifications_inner_form_dict = api_login_email200_response_profile_notifications_inner.from_dict(api_login_email200_response_profile_notifications_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


