# ApiLoginEmail200ResponsePostLoginTodoInnerContent


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **str** | The todo message | [optional] 
**tid** | **str** | The translation id | [optional] 
**tparams** | **object** | An object holding the parameters | [optional] 
**url** | **str** | Either the url or the text is filled | [optional] 

## Example

```python
from core_login.models.api_login_email200_response_post_login_todo_inner_content import ApiLoginEmail200ResponsePostLoginTodoInnerContent

# TODO update the JSON string below
json = "{}"
# create an instance of ApiLoginEmail200ResponsePostLoginTodoInnerContent from a JSON string
api_login_email200_response_post_login_todo_inner_content_instance = ApiLoginEmail200ResponsePostLoginTodoInnerContent.from_json(json)
# print the JSON string representation of the object
print ApiLoginEmail200ResponsePostLoginTodoInnerContent.to_json()

# convert the object into a dict
api_login_email200_response_post_login_todo_inner_content_dict = api_login_email200_response_post_login_todo_inner_content_instance.to_dict()
# create an instance of ApiLoginEmail200ResponsePostLoginTodoInnerContent from a dict
api_login_email200_response_post_login_todo_inner_content_form_dict = api_login_email200_response_post_login_todo_inner_content.from_dict(api_login_email200_response_post_login_todo_inner_content_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


