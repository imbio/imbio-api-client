# ApiLoginEmail200ResponsePostLoginTodoInner


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**style** | **str** | Type of the TODO /info, warning, error/ | [optional] 
**title** | [**ApiLoginEmail200ResponsePostLoginTodoInnerTitle**](ApiLoginEmail200ResponsePostLoginTodoInnerTitle.md) |  | [optional] 
**content** | [**ApiLoginEmail200ResponsePostLoginTodoInnerContent**](ApiLoginEmail200ResponsePostLoginTodoInnerContent.md) |  | [optional] 
**action** | [**ApiLoginEmail200ResponsePostLoginTodoInnerAction**](ApiLoginEmail200ResponsePostLoginTodoInnerAction.md) |  | [optional] 
**schedule** | **str** | When to show the warning, what is effected /login, logout/ | [optional] [default to 'login']

## Example

```python
from core_login.models.api_login_email200_response_post_login_todo_inner import ApiLoginEmail200ResponsePostLoginTodoInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiLoginEmail200ResponsePostLoginTodoInner from a JSON string
api_login_email200_response_post_login_todo_inner_instance = ApiLoginEmail200ResponsePostLoginTodoInner.from_json(json)
# print the JSON string representation of the object
print ApiLoginEmail200ResponsePostLoginTodoInner.to_json()

# convert the object into a dict
api_login_email200_response_post_login_todo_inner_dict = api_login_email200_response_post_login_todo_inner_instance.to_dict()
# create an instance of ApiLoginEmail200ResponsePostLoginTodoInner from a dict
api_login_email200_response_post_login_todo_inner_form_dict = api_login_email200_response_post_login_todo_inner.from_dict(api_login_email200_response_post_login_todo_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


