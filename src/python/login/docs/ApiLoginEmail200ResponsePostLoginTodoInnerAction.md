# ApiLoginEmail200ResponsePostLoginTodoInnerAction

What action the UI should use take

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The id of the action /tc_agreement, force_password_change/ | [optional] 
**type** | **str** | /optional, required/ | [optional] 

## Example

```python
from core_login.models.api_login_email200_response_post_login_todo_inner_action import ApiLoginEmail200ResponsePostLoginTodoInnerAction

# TODO update the JSON string below
json = "{}"
# create an instance of ApiLoginEmail200ResponsePostLoginTodoInnerAction from a JSON string
api_login_email200_response_post_login_todo_inner_action_instance = ApiLoginEmail200ResponsePostLoginTodoInnerAction.from_json(json)
# print the JSON string representation of the object
print ApiLoginEmail200ResponsePostLoginTodoInnerAction.to_json()

# convert the object into a dict
api_login_email200_response_post_login_todo_inner_action_dict = api_login_email200_response_post_login_todo_inner_action_instance.to_dict()
# create an instance of ApiLoginEmail200ResponsePostLoginTodoInnerAction from a dict
api_login_email200_response_post_login_todo_inner_action_form_dict = api_login_email200_response_post_login_todo_inner_action.from_dict(api_login_email200_response_post_login_todo_inner_action_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


