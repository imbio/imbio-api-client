# ApiLoginEmail200ResponseRightsInner

Permissions info

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Rigth id | 
**name** | **str** | Name of the Right | 
**description** | **str** | Description of the Right | [optional] 

## Example

```python
from core_login.models.api_login_email200_response_rights_inner import ApiLoginEmail200ResponseRightsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiLoginEmail200ResponseRightsInner from a JSON string
api_login_email200_response_rights_inner_instance = ApiLoginEmail200ResponseRightsInner.from_json(json)
# print the JSON string representation of the object
print ApiLoginEmail200ResponseRightsInner.to_json()

# convert the object into a dict
api_login_email200_response_rights_inner_dict = api_login_email200_response_rights_inner_instance.to_dict()
# create an instance of ApiLoginEmail200ResponseRightsInner from a dict
api_login_email200_response_rights_inner_form_dict = api_login_email200_response_rights_inner.from_dict(api_login_email200_response_rights_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


