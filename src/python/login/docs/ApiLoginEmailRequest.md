# ApiLoginEmailRequest

Object to authentication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **str** | User&#39;s email address  | 
**password** | **str** | User&#39;s password  | 

## Example

```python
from core_login.models.api_login_email_request import ApiLoginEmailRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiLoginEmailRequest from a JSON string
api_login_email_request_instance = ApiLoginEmailRequest.from_json(json)
# print the JSON string representation of the object
print ApiLoginEmailRequest.to_json()

# convert the object into a dict
api_login_email_request_dict = api_login_email_request_instance.to_dict()
# create an instance of ApiLoginEmailRequest from a dict
api_login_email_request_form_dict = api_login_email_request.from_dict(api_login_email_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


