# ApiLoginEmail200ResponseMainGroup


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from core_login.models.api_login_email200_response_main_group import ApiLoginEmail200ResponseMainGroup

# TODO update the JSON string below
json = "{}"
# create an instance of ApiLoginEmail200ResponseMainGroup from a JSON string
api_login_email200_response_main_group_instance = ApiLoginEmail200ResponseMainGroup.from_json(json)
# print the JSON string representation of the object
print ApiLoginEmail200ResponseMainGroup.to_json()

# convert the object into a dict
api_login_email200_response_main_group_dict = api_login_email200_response_main_group_instance.to_dict()
# create an instance of ApiLoginEmail200ResponseMainGroup from a dict
api_login_email200_response_main_group_form_dict = api_login_email200_response_main_group.from_dict(api_login_email200_response_main_group_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


