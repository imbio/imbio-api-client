# coding: utf-8

"""
    Imbio login endpoints

    Methods for logging in purposes (currently only email, password combo) 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_login.models.api_login_email200_response_post_login_todo_inner_content import ApiLoginEmail200ResponsePostLoginTodoInnerContent

class TestApiLoginEmail200ResponsePostLoginTodoInnerContent(unittest.TestCase):
    """ApiLoginEmail200ResponsePostLoginTodoInnerContent unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiLoginEmail200ResponsePostLoginTodoInnerContent:
        """Test ApiLoginEmail200ResponsePostLoginTodoInnerContent
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiLoginEmail200ResponsePostLoginTodoInnerContent`
        """
        model = ApiLoginEmail200ResponsePostLoginTodoInnerContent()
        if include_optional:
            return ApiLoginEmail200ResponsePostLoginTodoInnerContent(
                text = '',
                tid = '',
                tparams = core_login.models.tparams.tparams(),
                url = ''
            )
        else:
            return ApiLoginEmail200ResponsePostLoginTodoInnerContent(
        )
        """

    def testApiLoginEmail200ResponsePostLoginTodoInnerContent(self):
        """Test ApiLoginEmail200ResponsePostLoginTodoInnerContent"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
