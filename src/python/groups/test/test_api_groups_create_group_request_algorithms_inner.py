# coding: utf-8

"""
    IMBIO core api for groups

    Methods for managing groups 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_groups.models.api_groups_create_group_request_algorithms_inner import ApiGroupsCreateGroupRequestAlgorithmsInner

class TestApiGroupsCreateGroupRequestAlgorithmsInner(unittest.TestCase):
    """ApiGroupsCreateGroupRequestAlgorithmsInner unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiGroupsCreateGroupRequestAlgorithmsInner:
        """Test ApiGroupsCreateGroupRequestAlgorithmsInner
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiGroupsCreateGroupRequestAlgorithmsInner`
        """
        model = ApiGroupsCreateGroupRequestAlgorithmsInner()
        if include_optional:
            return ApiGroupsCreateGroupRequestAlgorithmsInner(
                id = 56,
                custom_algorithm_invocation_template = 'ge23w8Va BKfooZyIW1gBbZ',
                compute_instance_type = 't3.2xlarge',
                credits = -999999,
                research = True,
                review_workflow_enabled = True,
                qa_workflow_enabled = True,
                overread_workflow_enabled = True,
                suppressed_notifications = [
                    56
                    ]
            )
        else:
            return ApiGroupsCreateGroupRequestAlgorithmsInner(
                id = 56,
        )
        """

    def testApiGroupsCreateGroupRequestAlgorithmsInner(self):
        """Test ApiGroupsCreateGroupRequestAlgorithmsInner"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
