# coding: utf-8

"""
    IMBIO core api for groups

    Methods for managing groups 

    The version of the OpenAPI document: v1.0.0
    Contact: szilveszterbalogh@invenshure.com
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest
import datetime

from core_groups.models.api_groups_increase_credits_request import ApiGroupsIncreaseCreditsRequest

class TestApiGroupsIncreaseCreditsRequest(unittest.TestCase):
    """ApiGroupsIncreaseCreditsRequest unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> ApiGroupsIncreaseCreditsRequest:
        """Test ApiGroupsIncreaseCreditsRequest
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ApiGroupsIncreaseCreditsRequest`
        """
        model = ApiGroupsIncreaseCreditsRequest()
        if include_optional:
            return ApiGroupsIncreaseCreditsRequest(
                algorithm_id = 56,
                credits = -999999
            )
        else:
            return ApiGroupsIncreaseCreditsRequest(
        )
        """

    def testApiGroupsIncreaseCreditsRequest(self):
        """Test ApiGroupsIncreaseCreditsRequest"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
