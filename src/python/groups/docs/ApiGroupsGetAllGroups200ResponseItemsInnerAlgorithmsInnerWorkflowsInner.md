# ApiGroupsGetAllGroups200ResponseItemsInnerAlgorithmsInnerWorkflowsInner

Workflow object description

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

## Example

```python
from core_groups.models.api_groups_get_all_groups200_response_items_inner_algorithms_inner_workflows_inner import ApiGroupsGetAllGroups200ResponseItemsInnerAlgorithmsInnerWorkflowsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiGroupsGetAllGroups200ResponseItemsInnerAlgorithmsInnerWorkflowsInner from a JSON string
api_groups_get_all_groups200_response_items_inner_algorithms_inner_workflows_inner_instance = ApiGroupsGetAllGroups200ResponseItemsInnerAlgorithmsInnerWorkflowsInner.from_json(json)
# print the JSON string representation of the object
print ApiGroupsGetAllGroups200ResponseItemsInnerAlgorithmsInnerWorkflowsInner.to_json()

# convert the object into a dict
api_groups_get_all_groups200_response_items_inner_algorithms_inner_workflows_inner_dict = api_groups_get_all_groups200_response_items_inner_algorithms_inner_workflows_inner_instance.to_dict()
# create an instance of ApiGroupsGetAllGroups200ResponseItemsInnerAlgorithmsInnerWorkflowsInner from a dict
api_groups_get_all_groups200_response_items_inner_algorithms_inner_workflows_inner_form_dict = api_groups_get_all_groups200_response_items_inner_algorithms_inner_workflows_inner.from_dict(api_groups_get_all_groups200_response_items_inner_algorithms_inner_workflows_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


