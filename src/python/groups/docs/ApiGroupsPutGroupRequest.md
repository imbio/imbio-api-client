# ApiGroupsPutGroupRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The name used by a group | [optional] 
**description** | **str** | description of a group | [optional] 
**algorithms** | [**List[ApiGroupsCreateGroupRequestAlgorithmsInner]**](ApiGroupsCreateGroupRequestAlgorithmsInner.md) |  | [optional] 
**review_workflow_enabled** | **bool** | Set review workflow to enabled or disabled | [optional] 
**storage_time** | **int** | How many seconds to store assets | [optional] 
**research** | **bool** | shows if this the group runs algorithms for research | [optional] 
**region** | **str** | shows that the group is in what region | [optional] 
**timezone** | **str** | shows the timezone of the group | [optional] 
**qa_workflow_enabled** | **bool** | Shows if the qa review workflow is enabled | [optional] 
**overread_workflow_enabled** | **bool** | Shows if the overread workflow is enabled | [optional] 
**overread_request_enabled** | **bool** | Flag to decide whether overread request functionality is enabled for the group | [optional] 
**seg_editor_enabled** | **bool** | Shows if the segmentation editor is enabled | [optional] 
**editor_target** | **str** | The segmentation editor in use by a group | [optional] 
**deidentify** | **str** | Flag for deidentify data before uploading | [optional] 
**allow_notification_suppress** | **bool** | Disable notification filtering on user level. | [optional] 
**notifications** | **List[int]** | List of allowed notifications | [optional] 
**workflows** | **List[int]** | List of allowed workflows | [optional] 
**en_upload_any_series** | **bool** | Lets the group to upload any series including invalide series | [optional] 
**disable_series_preprocessing** | **bool** | Disable series preprocessing | [optional] 
**lbs_ttl** | **int** | The maximum time (in seconds) a Longitudinal Base Study will wait for samples | [optional] 

## Example

```python
from core_groups.models.api_groups_put_group_request import ApiGroupsPutGroupRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiGroupsPutGroupRequest from a JSON string
api_groups_put_group_request_instance = ApiGroupsPutGroupRequest.from_json(json)
# print the JSON string representation of the object
print ApiGroupsPutGroupRequest.to_json()

# convert the object into a dict
api_groups_put_group_request_dict = api_groups_put_group_request_instance.to_dict()
# create an instance of ApiGroupsPutGroupRequest from a dict
api_groups_put_group_request_form_dict = api_groups_put_group_request.from_dict(api_groups_put_group_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


