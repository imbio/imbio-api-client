# ApiGroupsPutGroupNotificationRequestAlgorithmsInner

Object containing algorithm id and suppressed notification ids

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Id of the algorithm | [optional] 
**suppressed_notifications** | **List[int]** | Ids of suppressed notifications | [optional] 

## Example

```python
from core_groups.models.api_groups_put_group_notification_request_algorithms_inner import ApiGroupsPutGroupNotificationRequestAlgorithmsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiGroupsPutGroupNotificationRequestAlgorithmsInner from a JSON string
api_groups_put_group_notification_request_algorithms_inner_instance = ApiGroupsPutGroupNotificationRequestAlgorithmsInner.from_json(json)
# print the JSON string representation of the object
print ApiGroupsPutGroupNotificationRequestAlgorithmsInner.to_json()

# convert the object into a dict
api_groups_put_group_notification_request_algorithms_inner_dict = api_groups_put_group_notification_request_algorithms_inner_instance.to_dict()
# create an instance of ApiGroupsPutGroupNotificationRequestAlgorithmsInner from a dict
api_groups_put_group_notification_request_algorithms_inner_form_dict = api_groups_put_group_notification_request_algorithms_inner.from_dict(api_groups_put_group_notification_request_algorithms_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


