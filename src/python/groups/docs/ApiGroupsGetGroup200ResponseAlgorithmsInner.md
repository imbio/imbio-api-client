# ApiGroupsGetGroup200ResponseAlgorithmsInner

Some info an algorithm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id of an algorithm | [optional] 
**name** | **str** | name of an algorithm | [optional] 
**description** | **str** | The description of the algorithm | [optional] 
**version** | **str** | version of an algorithm | [optional] 
**credits** | **int** | number of credits to run the algorithm | [optional] 
**logo_url** | **str** | The logo url of the algorithm | [optional] 
**active** | **bool** | Flag to show if algorithm is active | [optional] 
**required_number_of_assets** | **int** |  | [optional] 
**series_description** | **List[object]** | Description of the required series  | [optional] 
**custom_algorithm_invocation_template** | **str** |  | [optional] 
**compute_instance_type** | **str** |  | [optional] 
**research** | **bool** |  | [optional] 
**review_workflow_enabled** | **bool** |  | [optional] 
**qa_workflow_enabled** | **bool** |  | [optional] 
**overread_workflow_enabled** | **bool** |  | [optional] 
**suppressed_notifications** | **List[int]** | Ids of suppressed notifications | [optional] 
**workflows** | [**List[ApiGroupsGetAllGroups200ResponseItemsInnerAlgorithmsInnerWorkflowsInner]**](ApiGroupsGetAllGroups200ResponseItemsInnerAlgorithmsInnerWorkflowsInner.md) | Which workflow the algorithm is using | [optional] 

## Example

```python
from core_groups.models.api_groups_get_group200_response_algorithms_inner import ApiGroupsGetGroup200ResponseAlgorithmsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiGroupsGetGroup200ResponseAlgorithmsInner from a JSON string
api_groups_get_group200_response_algorithms_inner_instance = ApiGroupsGetGroup200ResponseAlgorithmsInner.from_json(json)
# print the JSON string representation of the object
print ApiGroupsGetGroup200ResponseAlgorithmsInner.to_json()

# convert the object into a dict
api_groups_get_group200_response_algorithms_inner_dict = api_groups_get_group200_response_algorithms_inner_instance.to_dict()
# create an instance of ApiGroupsGetGroup200ResponseAlgorithmsInner from a dict
api_groups_get_group200_response_algorithms_inner_form_dict = api_groups_get_group200_response_algorithms_inner.from_dict(api_groups_get_group200_response_algorithms_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


