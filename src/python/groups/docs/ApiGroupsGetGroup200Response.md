# ApiGroupsGetGroup200Response

group's information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Group Id | [optional] 
**name** | **str** | The name used by a group | 
**description** | **str** | description of a group | [optional] 
**review_workflow_enabled** | **bool** | Shows if the review workflow is enabled | [optional] 
**qa_workflow_enabled** | **bool** | Shows if the qa review workflow is enabled | [optional] 
**overread_workflow_enabled** | **bool** | Shows if the overread workflow is enabled | [optional] 
**overread_request_enabled** | **bool** | Flag to decide whether overread request functionality is enabled for the group | [optional] 
**seg_editor_enabled** | **bool** | Shows if the segmentation editor is enabled | [optional] 
**editor_target** | **str** | The segmentation editor in use by a group | [optional] 
**deidentify** | **str** | Flag for deidentify data before uploading | [optional] 
**storage_time** | **int** | How many days to store assets | [optional] 
**research** | **bool** | shows if this group runs algorithms for research | [optional] 
**region** | **str** | shows the region of the group | [optional] 
**timezone** | **str** | shows the timezone of the group | [optional] 
**algorithms** | [**List[ApiGroupsGetGroup200ResponseAlgorithmsInner]**](ApiGroupsGetGroup200ResponseAlgorithmsInner.md) | Some info of the algorithms belonging to group | [optional] 
**allow_notification_suppress** | **bool** | Disable notification filtering on user level. | [optional] 
**notifications** | [**List[ApiGroupsGetAllGroups200ResponseItemsInnerNotificationsInner]**](ApiGroupsGetAllGroups200ResponseItemsInnerNotificationsInner.md) | Some info of the algorithms belonging to group | [optional] 
**workflows** | [**List[ApiGroupsGetAllGroups200ResponseItemsInnerAlgorithmsInnerWorkflowsInner]**](ApiGroupsGetAllGroups200ResponseItemsInnerAlgorithmsInnerWorkflowsInner.md) | Some info of the algorithms belonging to group | [optional] 
**en_upload_any_series** | **bool** | Lets the group to upload any series including invalide series | [optional] 
**disable_series_preprocessing** | **bool** | Disable series preprocessing | [optional] 
**lbs_ttl** | **int** | The maximum time (in seconds) a Longitudinal Base Study will wait for samples | [optional] 

## Example

```python
from core_groups.models.api_groups_get_group200_response import ApiGroupsGetGroup200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiGroupsGetGroup200Response from a JSON string
api_groups_get_group200_response_instance = ApiGroupsGetGroup200Response.from_json(json)
# print the JSON string representation of the object
print ApiGroupsGetGroup200Response.to_json()

# convert the object into a dict
api_groups_get_group200_response_dict = api_groups_get_group200_response_instance.to_dict()
# create an instance of ApiGroupsGetGroup200Response from a dict
api_groups_get_group200_response_form_dict = api_groups_get_group200_response.from_dict(api_groups_get_group200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


