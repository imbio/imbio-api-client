# ApiGroupsIncreaseCreditsRequest

The body of credit update

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**algorithm_id** | **int** | The id of the algorithm to give credits to | [optional] 
**credits** | **int** | The number of credits to give | [optional] 

## Example

```python
from core_groups.models.api_groups_increase_credits_request import ApiGroupsIncreaseCreditsRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiGroupsIncreaseCreditsRequest from a JSON string
api_groups_increase_credits_request_instance = ApiGroupsIncreaseCreditsRequest.from_json(json)
# print the JSON string representation of the object
print ApiGroupsIncreaseCreditsRequest.to_json()

# convert the object into a dict
api_groups_increase_credits_request_dict = api_groups_increase_credits_request_instance.to_dict()
# create an instance of ApiGroupsIncreaseCreditsRequest from a dict
api_groups_increase_credits_request_form_dict = api_groups_increase_credits_request.from_dict(api_groups_increase_credits_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


