# core_groups.DefaultApi

All URIs are relative to *https://launchpad.imbio.com/api/v1/groups*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_groups_create_group**](DefaultApi.md#api_groups_create_group) | **POST** / | 
[**api_groups_delete_group**](DefaultApi.md#api_groups_delete_group) | **DELETE** /{group_id} | 
[**api_groups_get_algorithm_logo**](DefaultApi.md#api_groups_get_algorithm_logo) | **GET** /{group_id}/algorithmLogo/{algorithm_id} | 
[**api_groups_get_algorithm_report_image**](DefaultApi.md#api_groups_get_algorithm_report_image) | **GET** /{group_id}/algorithms/{algorithm_id}/report_{report_image_type} | 
[**api_groups_get_all_groups**](DefaultApi.md#api_groups_get_all_groups) | **GET** / | 
[**api_groups_get_group**](DefaultApi.md#api_groups_get_group) | **GET** /{group_id} | 
[**api_groups_increase_credits**](DefaultApi.md#api_groups_increase_credits) | **POST** /{group_id}/credits/increase | 
[**api_groups_put_algorithm_logo**](DefaultApi.md#api_groups_put_algorithm_logo) | **PUT** /{group_id}/algorithmLogo/{algorithm_id} | 
[**api_groups_put_algorithm_report_image**](DefaultApi.md#api_groups_put_algorithm_report_image) | **PUT** /{group_id}/algorithms/{algorithm_id}/report_{report_image_type} | 
[**api_groups_put_group**](DefaultApi.md#api_groups_put_group) | **PUT** /{group_id} | 
[**api_groups_put_group_algorithm**](DefaultApi.md#api_groups_put_group_algorithm) | **PUT** /{group_id}/algorithms | 
[**api_groups_put_group_notification**](DefaultApi.md#api_groups_put_group_notification) | **PUT** /{group_id}/notifications | 


# **api_groups_create_group**
> ApiGroupsCreateGroup200Response api_groups_create_group(api_groups_create_group_request)



Create a new group

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_groups
from core_groups.models.api_groups_create_group200_response import ApiGroupsCreateGroup200Response
from core_groups.models.api_groups_create_group_request import ApiGroupsCreateGroupRequest
from core_groups.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/groups
# See configuration.py for a list of all supported configuration parameters.
configuration = core_groups.Configuration(
    host = "https://launchpad.imbio.com/api/v1/groups"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_groups.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_groups.DefaultApi(api_client)
    api_groups_create_group_request = core_groups.ApiGroupsCreateGroupRequest() # ApiGroupsCreateGroupRequest | Initial properties of the group.

    try:
        api_response = api_instance.api_groups_create_group(api_groups_create_group_request)
        print("The response of DefaultApi->api_groups_create_group:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_groups_create_group: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_groups_create_group_request** | [**ApiGroupsCreateGroupRequest**](ApiGroupsCreateGroupRequest.md)| Initial properties of the group. | 

### Return type

[**ApiGroupsCreateGroup200Response**](ApiGroupsCreateGroup200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | 200: Returns the the newly created resource&#39;s id  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_groups_delete_group**
> api_groups_delete_group(group_id)



Deletes one group

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_groups
from core_groups.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/groups
# See configuration.py for a list of all supported configuration parameters.
configuration = core_groups.Configuration(
    host = "https://launchpad.imbio.com/api/v1/groups"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_groups.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_groups.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group

    try:
        api_instance.api_groups_delete_group(group_id)
    except Exception as e:
        print("Exception when calling DefaultApi->api_groups_delete_group: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_groups_get_algorithm_logo**
> api_groups_get_algorithm_logo(group_id, algorithm_id, to_save=to_save)



Returns the logo of an algorithm

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_groups
from core_groups.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/groups
# See configuration.py for a list of all supported configuration parameters.
configuration = core_groups.Configuration(
    host = "https://launchpad.imbio.com/api/v1/groups"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_groups.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_groups.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group
    algorithm_id = 'algorithm_id_example' # str | The unique identifier of an algorithm
    to_save = True # bool | Set true if you want to save the file instead of getting to content of it  (optional)

    try:
        api_instance.api_groups_get_algorithm_logo(group_id, algorithm_id, to_save=to_save)
    except Exception as e:
        print("Exception when calling DefaultApi->api_groups_get_algorithm_logo: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 
 **algorithm_id** | **str**| The unique identifier of an algorithm | 
 **to_save** | **bool**| Set true if you want to save the file instead of getting to content of it  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the logo used by a group |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_groups_get_algorithm_report_image**
> api_groups_get_algorithm_report_image(group_id, algorithm_id, report_image_type, to_save=to_save)



Returns the logo of an algorithm

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_groups
from core_groups.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/groups
# See configuration.py for a list of all supported configuration parameters.
configuration = core_groups.Configuration(
    host = "https://launchpad.imbio.com/api/v1/groups"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_groups.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_groups.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group
    algorithm_id = 'algorithm_id_example' # str | The unique identifier of an algorithm
    report_image_type = 'report_image_type_example' # str | The header or footer on the the alg report
    to_save = True # bool | Set true if you want to save the file instead of getting to content of it  (optional)

    try:
        api_instance.api_groups_get_algorithm_report_image(group_id, algorithm_id, report_image_type, to_save=to_save)
    except Exception as e:
        print("Exception when calling DefaultApi->api_groups_get_algorithm_report_image: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 
 **algorithm_id** | **str**| The unique identifier of an algorithm | 
 **report_image_type** | **str**| The header or footer on the the alg report | 
 **to_save** | **bool**| Set true if you want to save the file instead of getting to content of it  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the logo used by a group |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_groups_get_all_groups**
> ApiGroupsGetAllGroups200Response api_groups_get_all_groups(limit=limit, offset=offset, order=order, search=search, item_id=item_id, response_format=response_format, workflow_id=workflow_id, order_by=order_by)



Returns all groups

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_groups
from core_groups.models.api_groups_get_all_groups200_response import ApiGroupsGetAllGroups200Response
from core_groups.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/groups
# See configuration.py for a list of all supported configuration parameters.
configuration = core_groups.Configuration(
    host = "https://launchpad.imbio.com/api/v1/groups"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_groups.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_groups.DefaultApi(api_client)
    limit = 50 # int | Limit the number of returned roles to indicated value (default 50).  (optional) (default to 50)
    offset = 0 # int | When set the query returns 'limit' number of roles but skip 'offset' number of roles at the beginning.  (optional) (default to 0)
    order = 'order_example' # str | Specify the order of the listing: desc, asc  (optional)
    search = 'search_example' # str | Specify a search string  (optional)
    item_id = [56] # List[int] | Filter resource by their primary id  (optional)
    response_format = 'response_format_example' # str | How detialed the response should be  (optional)
    workflow_id = [56] # List[int] | Filter by workflow(s)  (optional)
    order_by = 'order_by_example' # str | Order by the provided argument name.  (optional)

    try:
        api_response = api_instance.api_groups_get_all_groups(limit=limit, offset=offset, order=order, search=search, item_id=item_id, response_format=response_format, workflow_id=workflow_id, order_by=order_by)
        print("The response of DefaultApi->api_groups_get_all_groups:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_groups_get_all_groups: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Limit the number of returned roles to indicated value (default 50).  | [optional] [default to 50]
 **offset** | **int**| When set the query returns &#39;limit&#39; number of roles but skip &#39;offset&#39; number of roles at the beginning.  | [optional] [default to 0]
 **order** | **str**| Specify the order of the listing: desc, asc  | [optional] 
 **search** | **str**| Specify a search string  | [optional] 
 **item_id** | [**List[int]**](int.md)| Filter resource by their primary id  | [optional] 
 **response_format** | **str**| How detialed the response should be  | [optional] 
 **workflow_id** | [**List[int]**](int.md)| Filter by workflow(s)  | [optional] 
 **order_by** | **str**| Order by the provided argument name.  | [optional] 

### Return type

[**ApiGroupsGetAllGroups200Response**](ApiGroupsGetAllGroups200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get all resource |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_groups_get_group**
> ApiGroupsGetGroup200Response api_groups_get_group(group_id)



Returns details of a group

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_groups
from core_groups.models.api_groups_get_group200_response import ApiGroupsGetGroup200Response
from core_groups.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/groups
# See configuration.py for a list of all supported configuration parameters.
configuration = core_groups.Configuration(
    host = "https://launchpad.imbio.com/api/v1/groups"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_groups.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_groups.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group

    try:
        api_response = api_instance.api_groups_get_group(group_id)
        print("The response of DefaultApi->api_groups_get_group:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_groups_get_group: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 

### Return type

[**ApiGroupsGetGroup200Response**](ApiGroupsGetGroup200Response.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | details of a group |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_groups_increase_credits**
> api_groups_increase_credits(group_id, api_groups_increase_credits_request)



increase credits

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_groups
from core_groups.models.api_groups_increase_credits_request import ApiGroupsIncreaseCreditsRequest
from core_groups.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/groups
# See configuration.py for a list of all supported configuration parameters.
configuration = core_groups.Configuration(
    host = "https://launchpad.imbio.com/api/v1/groups"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_groups.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_groups.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group
    api_groups_increase_credits_request = core_groups.ApiGroupsIncreaseCreditsRequest() # ApiGroupsIncreaseCreditsRequest | 

    try:
        api_instance.api_groups_increase_credits(group_id, api_groups_increase_credits_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_groups_increase_credits: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 
 **api_groups_increase_credits_request** | [**ApiGroupsIncreaseCreditsRequest**](ApiGroupsIncreaseCreditsRequest.md)|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_groups_put_algorithm_logo**
> object api_groups_put_algorithm_logo(group_id, algorithm_id, logo)



Uploads a logo of an algorithm to a group

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_groups
from core_groups.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/groups
# See configuration.py for a list of all supported configuration parameters.
configuration = core_groups.Configuration(
    host = "https://launchpad.imbio.com/api/v1/groups"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_groups.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_groups.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group
    algorithm_id = 'algorithm_id_example' # str | The unique identifier of an algorithm
    logo = None # bytearray | logo file

    try:
        api_response = api_instance.api_groups_put_algorithm_logo(group_id, algorithm_id, logo)
        print("The response of DefaultApi->api_groups_put_algorithm_logo:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->api_groups_put_algorithm_logo: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 
 **algorithm_id** | **str**| The unique identifier of an algorithm | 
 **logo** | **bytearray**| logo file | 

### Return type

**object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The logo used by the group has been uploaded |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_groups_put_algorithm_report_image**
> api_groups_put_algorithm_report_image(group_id, algorithm_id, report_image_type, image)



Uploads a logo of an algorithm to a group

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_groups
from core_groups.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/groups
# See configuration.py for a list of all supported configuration parameters.
configuration = core_groups.Configuration(
    host = "https://launchpad.imbio.com/api/v1/groups"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_groups.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_groups.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group
    algorithm_id = 'algorithm_id_example' # str | The unique identifier of an algorithm
    report_image_type = 'report_image_type_example' # str | The header or footer on the the alg report
    image = None # bytearray | The report header / footer image

    try:
        api_instance.api_groups_put_algorithm_report_image(group_id, algorithm_id, report_image_type, image)
    except Exception as e:
        print("Exception when calling DefaultApi->api_groups_put_algorithm_report_image: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 
 **algorithm_id** | **str**| The unique identifier of an algorithm | 
 **report_image_type** | **str**| The header or footer on the the alg report | 
 **image** | **bytearray**| The report header / footer image | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_groups_put_group**
> api_groups_put_group(group_id, api_groups_put_group_request)



Updates one group

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_groups
from core_groups.models.api_groups_put_group_request import ApiGroupsPutGroupRequest
from core_groups.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/groups
# See configuration.py for a list of all supported configuration parameters.
configuration = core_groups.Configuration(
    host = "https://launchpad.imbio.com/api/v1/groups"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_groups.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_groups.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group
    api_groups_put_group_request = core_groups.ApiGroupsPutGroupRequest() # ApiGroupsPutGroupRequest | The data for update

    try:
        api_instance.api_groups_put_group(group_id, api_groups_put_group_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_groups_put_group: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 
 **api_groups_put_group_request** | [**ApiGroupsPutGroupRequest**](ApiGroupsPutGroupRequest.md)| The data for update | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_groups_put_group_algorithm**
> api_groups_put_group_algorithm(group_id, api_groups_create_group_request_algorithms_inner)



Updates one group

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_groups
from core_groups.models.api_groups_create_group_request_algorithms_inner import ApiGroupsCreateGroupRequestAlgorithmsInner
from core_groups.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/groups
# See configuration.py for a list of all supported configuration parameters.
configuration = core_groups.Configuration(
    host = "https://launchpad.imbio.com/api/v1/groups"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_groups.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_groups.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group
    api_groups_create_group_request_algorithms_inner = core_groups.ApiGroupsCreateGroupRequestAlgorithmsInner() # ApiGroupsCreateGroupRequestAlgorithmsInner | The data for update

    try:
        api_instance.api_groups_put_group_algorithm(group_id, api_groups_create_group_request_algorithms_inner)
    except Exception as e:
        print("Exception when calling DefaultApi->api_groups_put_group_algorithm: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 
 **api_groups_create_group_request_algorithms_inner** | [**ApiGroupsCreateGroupRequestAlgorithmsInner**](ApiGroupsCreateGroupRequestAlgorithmsInner.md)| The data for update | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**404** | 404: Resource was not found  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_groups_put_group_notification**
> api_groups_put_group_notification(group_id, api_groups_put_group_notification_request)



Updates notification settings for the selected group

### Example

* OAuth Authentication (oauth2):
```python
import time
import os
import core_groups
from core_groups.models.api_groups_put_group_notification_request import ApiGroupsPutGroupNotificationRequest
from core_groups.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://launchpad.imbio.com/api/v1/groups
# See configuration.py for a list of all supported configuration parameters.
configuration = core_groups.Configuration(
    host = "https://launchpad.imbio.com/api/v1/groups"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

configuration.access_token = os.environ["ACCESS_TOKEN"]

# Enter a context with an instance of the API client
with core_groups.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = core_groups.DefaultApi(api_client)
    group_id = 56 # int | The unique identifier of a group
    api_groups_put_group_notification_request = core_groups.ApiGroupsPutGroupNotificationRequest() # ApiGroupsPutGroupNotificationRequest | The data for update

    try:
        api_instance.api_groups_put_group_notification(group_id, api_groups_put_group_notification_request)
    except Exception as e:
        print("Exception when calling DefaultApi->api_groups_put_group_notification: %s\n" % e)
```



### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **int**| The unique identifier of a group | 
 **api_groups_put_group_notification_request** | [**ApiGroupsPutGroupNotificationRequest**](ApiGroupsPutGroupNotificationRequest.md)| The data for update | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | 204: Successfully executed, nothing to return  |  -  |
**403** | 403: Permission denied to perform operation  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

