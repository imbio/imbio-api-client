# ApiGroupsGetAllGroups200ResponseItemsInnerNotificationsInner

All notification related information

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The notification id  | [optional] 
**type** | **str** | The notification type  | [optional] 
**name** | **str** | The notification name  | [optional] 
**description** | **str** | The notification description  | [optional] 
**notification_group** | **str** | The notification group  | [optional] 
**template** | **str** | The notification template  | [optional] 

## Example

```python
from core_groups.models.api_groups_get_all_groups200_response_items_inner_notifications_inner import ApiGroupsGetAllGroups200ResponseItemsInnerNotificationsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiGroupsGetAllGroups200ResponseItemsInnerNotificationsInner from a JSON string
api_groups_get_all_groups200_response_items_inner_notifications_inner_instance = ApiGroupsGetAllGroups200ResponseItemsInnerNotificationsInner.from_json(json)
# print the JSON string representation of the object
print ApiGroupsGetAllGroups200ResponseItemsInnerNotificationsInner.to_json()

# convert the object into a dict
api_groups_get_all_groups200_response_items_inner_notifications_inner_dict = api_groups_get_all_groups200_response_items_inner_notifications_inner_instance.to_dict()
# create an instance of ApiGroupsGetAllGroups200ResponseItemsInnerNotificationsInner from a dict
api_groups_get_all_groups200_response_items_inner_notifications_inner_form_dict = api_groups_get_all_groups200_response_items_inner_notifications_inner.from_dict(api_groups_get_all_groups200_response_items_inner_notifications_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


