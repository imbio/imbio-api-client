# ApiGroupsCreateGroupRequestAlgorithmsInner

Algorithm description related to a group

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**custom_algorithm_invocation_template** | **str** |  | [optional] 
**compute_instance_type** | **str** | The instance type required to run the algorithm  | [optional] 
**credits** | **int** |  | [optional] 
**research** | **bool** |  | [optional] 
**review_workflow_enabled** | **bool** | This is obsolete, don&#39;t use it. This is only for response validation | [optional] 
**qa_workflow_enabled** | **bool** |  | [optional] 
**overread_workflow_enabled** | **bool** |  | [optional] 
**suppressed_notifications** | **List[int]** | Ids of suppressed notifications | [optional] 

## Example

```python
from core_groups.models.api_groups_create_group_request_algorithms_inner import ApiGroupsCreateGroupRequestAlgorithmsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ApiGroupsCreateGroupRequestAlgorithmsInner from a JSON string
api_groups_create_group_request_algorithms_inner_instance = ApiGroupsCreateGroupRequestAlgorithmsInner.from_json(json)
# print the JSON string representation of the object
print ApiGroupsCreateGroupRequestAlgorithmsInner.to_json()

# convert the object into a dict
api_groups_create_group_request_algorithms_inner_dict = api_groups_create_group_request_algorithms_inner_instance.to_dict()
# create an instance of ApiGroupsCreateGroupRequestAlgorithmsInner from a dict
api_groups_create_group_request_algorithms_inner_form_dict = api_groups_create_group_request_algorithms_inner.from_dict(api_groups_create_group_request_algorithms_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


