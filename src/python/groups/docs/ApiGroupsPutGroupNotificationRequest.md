# ApiGroupsPutGroupNotificationRequest

Notification object that updates the settings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allow_notification_suppress** | **bool** | Allow/disable notification filtering on user level. | [optional] 
**notifications** | **List[int]** | List of notification ids | 
**algorithms** | [**List[ApiGroupsPutGroupNotificationRequestAlgorithmsInner]**](ApiGroupsPutGroupNotificationRequestAlgorithmsInner.md) | List of algorithm objects | [optional] 

## Example

```python
from core_groups.models.api_groups_put_group_notification_request import ApiGroupsPutGroupNotificationRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ApiGroupsPutGroupNotificationRequest from a JSON string
api_groups_put_group_notification_request_instance = ApiGroupsPutGroupNotificationRequest.from_json(json)
# print the JSON string representation of the object
print ApiGroupsPutGroupNotificationRequest.to_json()

# convert the object into a dict
api_groups_put_group_notification_request_dict = api_groups_put_group_notification_request_instance.to_dict()
# create an instance of ApiGroupsPutGroupNotificationRequest from a dict
api_groups_put_group_notification_request_form_dict = api_groups_put_group_notification_request.from_dict(api_groups_put_group_notification_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


