# ApiGroupsGetAllGroups200Response


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**num_of_all** | **int** | How many groups there are currently | [optional] 
**index** | **int** | groups returned from this index | [optional] 
**items** | [**List[ApiGroupsGetAllGroups200ResponseItemsInner]**](ApiGroupsGetAllGroups200ResponseItemsInner.md) | The list of returned groups | [optional] 

## Example

```python
from core_groups.models.api_groups_get_all_groups200_response import ApiGroupsGetAllGroups200Response

# TODO update the JSON string below
json = "{}"
# create an instance of ApiGroupsGetAllGroups200Response from a JSON string
api_groups_get_all_groups200_response_instance = ApiGroupsGetAllGroups200Response.from_json(json)
# print the JSON string representation of the object
print ApiGroupsGetAllGroups200Response.to_json()

# convert the object into a dict
api_groups_get_all_groups200_response_dict = api_groups_get_all_groups200_response_instance.to_dict()
# create an instance of ApiGroupsGetAllGroups200Response from a dict
api_groups_get_all_groups200_response_form_dict = api_groups_get_all_groups200_response.from_dict(api_groups_get_all_groups200_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


