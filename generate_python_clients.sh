#!/usr/bin/env bash

# Declare an associative array
declare -A clients

# Assign key-value pairs
clients["algorithms"]="core/algorithms.json"
clients["assets"]="core/assets.json"
clients["groups"]="core/groups.json"
clients["healthcheck"]="core/healthcheck.json"
clients["info"]="core/info.json"
clients["jobs"]="core/jobs.json"
clients["login"]="core/login.json"
clients["notifications"]="core/notifications.json"
clients["patients"]="core/patients.json"
clients["profiles"]="core/profiles.json"
clients["skins"]="core/skins.json"
clients["studies"]="core/studies.json"
clients["users"]="core/users.json"


# Iterate over keys and values
for client in "${!clients[@]}"; do
    sudo rm -rf src/python/$client
    echo "{\"packageName\": \"core_${client}\"}" > client_jsons/generator_config.json
    docker run -v ${PWD}/client_jsons:/i -v ${PWD}/src/python:/o openapitools/openapi-generator-cli:v7.1.0 generate -g python -i "/i/${clients[$client]}" -o "/o/${client}" -c /i/generator_config.json
    sudo chown -R $USER:$USER src/python
done


# Iterate through generated clients and apply patches
for config_file in $(find src/python/*/ -type f -name configuration.py); do
    patch $config_file < src/python/configuration_patch
done

for rest_file in $(find src/python/*/ -type f -name rest.py); do
    patch $rest_file < src/python/rest_patch
done
