# Imbio API Client Repository

## App password generation
To pull down the source code you'll need an app password with Repositories read permission. To generate one you should visit:
```
https://bitbucket.org/account/settings/app-passwords/
```

## Python clients

### Requirements
Git needs to be installed where you want to install the client (at least for the installation)

### How to install packages with pip
```
pip install -e "git+https://<user_id>@bitbucket.org/imbio/imbio-api-client.git#egg=python&subdirectory=src/python/<client>"
```

You can install it with a requirements file as well, for that you should place
```
-e "git+https://<user_id>@bitbucket.org/imbio/imbio-api-client.git#egg=python&subdirectory=src/python/<client>"
```
into a requirements.txt file, and then simple use this command:
```
pip install -r requirements.txt
```

You can install it from branch name, commit hash, git ref as well. Example:
```
pip install -e "git+https://<user_id>@bitbucket.org/imbio/imbio-api-client.git@v1.1.0.0#egg=python&subdirectory=src/python/<client>"
```

For further example please visit:
```
https://pip.pypa.io/en/stable/reference/pip_install/#git
```

After the pip install command is given, you should enter your app password to install.

### How to use
After install, client can be imported and used. More detail how to use the generated client, please read the README.md in the given generated directory.
